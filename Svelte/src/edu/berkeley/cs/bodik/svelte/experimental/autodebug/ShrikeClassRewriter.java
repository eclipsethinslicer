//package edu.berkeley.cs.bodik.svelte.experimental.autodebug;

//import java.io.IOException;
//import java.io.PrintWriter;

//import com.ibm.wala.shrikeBT.BinaryOpInstruction;
//import com.ibm.wala.shrikeBT.Constants;
//import com.ibm.wala.shrikeBT.Disassembler;
//import com.ibm.wala.shrikeBT.DupInstruction;
//import com.ibm.wala.shrikeBT.GetInstruction;
//import com.ibm.wala.shrikeBT.InvokeInstruction;
//import com.ibm.wala.shrikeBT.MethodData;
//import com.ibm.wala.shrikeBT.MethodEditor;
//import com.ibm.wala.shrikeBT.SwapInstruction;
//import com.ibm.wala.shrikeBT.BinaryOpInstruction.Operator;
//import com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch;
//import com.ibm.wala.shrikeBT.MethodEditor.Output;
//import com.ibm.wala.shrikeBT.MethodEditor.Patch;
//import com.ibm.wala.shrikeBT.analysis.Verifier;
//import com.ibm.wala.shrikeBT.analysis.Analyzer.FailureException;
//import com.ibm.wala.shrikeBT.shrikeCT.ClassInstrumenter;
//import com.ibm.wala.shrikeBT.shrikeCT.OfflineInstrumenter;
//import com.ibm.wala.shrikeCT.ClassReader;
//import com.ibm.wala.shrikeCT.ClassWriter;
//import com.ibm.wala.shrikeCT.InvalidClassFileException;

//public class ShrikeClassRewriter {

//public static void main(String args[]) {
//try {
//String fakeArgs[] = { "bogus", "/home/evan/t/shrikeinstrumentation/old.jar", "-o", "/home/evan/t/shrikeinstrumentation/new.jar" }; 
//OfflineInstrumenter instrumenter = new OfflineInstrumenter();
//instrumenter.parseStandardArgs(fakeArgs);
//instrumenter.setPassUnmodifiedClasses(true);
//instrumenter.beginTraversal();
//ClassInstrumenter ci;
//while ((ci = instrumenter.nextClass()) != null) {
//if ( ci.getReader().getName().equals("Main") )
//doClass(ci,instrumenter);
//}
//instrumenter.close();
//} catch (InvalidClassFileException e) {
////TODO Auto-generated catch block
//e.printStackTrace();
//} catch (IOException e) {
////TODO Auto-generated catch block
//e.printStackTrace();
//}

//}

//private static void doClass(ClassInstrumenter ci, OfflineInstrumenter instrumenter) throws InvalidClassFileException {
////TODO Auto-generated method stub
//final String className = ci.getReader().getName();
//System.out.println("looking at "+ className);

//for (int m = 0; m < ci.getReader().getMethodCount(); m++) {
//MethodData d = ci.visitMethod(m);
//System.out.println("kaputt method " + d.getName() + d.getSignature());

//try {
//(new Disassembler(d)).disassembleTo(new PrintWriter(System.out));
//System.out.println("ok");
//} catch (Exception e) {e.printStackTrace();}

//Verifier v = new Verifier(d);
//try {
//v.verify();
//} catch (FailureException e1) {
////TODO Auto-generated catch block
//e1.printStackTrace();
//}

//if ( d.getName().equals("main") && d.getSignature().equals("[Ljava/lang/String;)V" ) ) {
//System.out.println("messing with main");
//MethodEditor me = new MethodEditor(d);
//me.beginPass();
//me.insertBefore(13, new Patch() {

//@Override
//public void emitTo(Output w) {
//w.emit(DupInstruction.make(0));
//w.emit(BinaryOpInstruction.make("I",Operator.ADD));
////w.emit(GetInstruction.make("Ljava/io/PrintStream;","LJava/lang/System;","out",true));
////w.emit(SwapInstruction.make());
////w.emit(InvokeInstruction.make("V","Ljava/io/PrintStream;","println",Dispatch.VIRTUAL));
//}

//});
//me.applyPatches();
//me.endPass();

//try {
//(new Disassembler(d)).disassembleTo(new PrintWriter(System.out));
//} catch (Exception e) {e.printStackTrace();}
//}
//}
//if (ci.isChanged()) {
//ClassWriter cw = ci.emitClass();
////cw.addField(ClassReader.ACC_PUBLIC | ClassReader.ACC_STATIC, fieldName, Constants.TYPE_boolean, new ClassWriter.Element[0]);
//try {
//instrumenter.outputModifiedClass(ci, cw);
//} catch (IllegalStateException e) {
////TODO Auto-generated catch block
//e.printStackTrace();
//} catch (IOException e) {
////TODO Auto-generated catch block
//e.printStackTrace();
//}
//}
//}
//}
/*******************************************************************************
 * Copyright (c) 2002,2006 IBM Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package edu.berkeley.cs.bodik.svelte.experimental.autodebug;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.BitSet;

import com.ibm.wala.shrikeBT.DupInstruction;
import com.ibm.wala.shrikeBT.GetInstruction;
import com.ibm.wala.shrikeBT.Instruction;
import com.ibm.wala.shrikeBT.InvokeInstruction;
import com.ibm.wala.shrikeBT.MethodData;
import com.ibm.wala.shrikeBT.MethodEditor;
import com.ibm.wala.shrikeBT.SwapInstruction;
import com.ibm.wala.shrikeBT.Util;
import com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch;
import com.ibm.wala.shrikeBT.MethodEditor.Output;
import com.ibm.wala.shrikeBT.MethodEditor.Patch;
import com.ibm.wala.shrikeBT.analysis.Verifier;
import com.ibm.wala.shrikeBT.shrikeCT.ClassInstrumenter;
import com.ibm.wala.shrikeBT.shrikeCT.OfflineInstrumenter;
import com.ibm.wala.shrikeCT.ClassWriter;

/**
 * This is a demo class.
 * 
 * Class files are taken as input arguments (or if there are none, from standard input). The methods
 * in those files are instrumented: we insert a System.err.println() at ever method call, and a
 * System.err.println() at every method entry.
 * 
 * In Unix, I run it like this: java -cp ~/dev/shrike/shrike
 * com.ibm.wala.shrikeBT.shrikeCT.tools.Bench test.jar -o output.jar
 * 
 * The instrumented classes are placed in the directory "output" under the current directory.
 * Disassembled code is written to the file "report" under the current directory.
 */
public class ShrikeClassRewriter {
	private final static boolean disasm = true;
	private final static boolean verify = true;

	private static OfflineInstrumenter instrumenter;

	final private static boolean doEntry = true;
	private static boolean doExit = false;
	private static boolean doException = false;

	public static void instrument(ArrayList<String> input, String outputFilename) throws Exception {
		ArrayList<String> realargs = new ArrayList<String>();
		realargs.add("ignored"); // bogus first arg (name of program), doesn't matter
		realargs.addAll(input);
		realargs.add("-o");
		realargs.add(outputFilename);
		instrument((String[]) realargs.toArray());
	}

	private static void instrument(String[] args) throws Exception {

		for (int i = 0; i < 1; i++) {
			instrumenter = new OfflineInstrumenter();

			Writer w = new BufferedWriter(new FileWriter("report", false));

			args = instrumenter.parseStandardArgs(args);
			if (args.length > 0) {
				if (args[0].equals("-doexit")) {
					doExit = true;
				} else if (args[0].equals("-doexception")) {
					doExit = true;
					doException = true;
				}
			}
			instrumenter.setPassUnmodifiedClasses(true);
			instrumenter.beginTraversal();
			ClassInstrumenter ci;
			while ((ci = instrumenter.nextClass()) != null) {
				doClass(ci, w);
			}
			instrumenter.close();
		}
	}

	static final String fieldName = "_Bench_enable_trace";

	// Keep these commonly used instructions around
	static final Instruction getSysErr = Util.makeGet(System.class, "err");
	static final Instruction callPrintln = Util.makeInvoke(PrintStream.class, "println",
			new Class[] { String.class });

	private static void doClass(final ClassInstrumenter ci, Writer w) throws Exception {
		final String className = ci.getReader().getName();

		for (int m = 0; m < ci.getReader().getMethodCount(); m++) {
			MethodData d = ci.visitMethod(m);

			// d could be null, e.g., if the method is abstract or native
			if (d != null) {

				MethodEditor me = new MethodEditor(d);
				me.beginPass();
				System.out.println(d.getName() + d.getSignature());
				if (d.getName().equals("main") && d.getSignature().equals("([Ljava/lang/String;)V")) {
					System.out.println("messing with main");

					int putis[] = { 6, 13, 16 };

					Verifier v = new Verifier(d);
					BitSet all = new BitSet(d.getInstructions().length);

					for (int puti : putis)
						all.set(puti, true);
					v.computeTypes(null, all, false);

					for (final int puti : putis) {
						final String type = v.getStackTypes()[puti][0];

						// v.getStackTypes()[13][0] is the type of the data
						// if starts with L: just run object.toString() on it and print it

						me.insertBefore(puti, new Patch() {

							@Override
							public void emitTo(Output w) {
								System.out
										.println("type of top of stack at " + puti + "is " + type);

								if (type.startsWith("L")) {
									w.emit(DupInstruction.make(0));
									if (!type.equals("Ljava/lang/String;"))
										w
												.emit(InvokeInstruction.make(
														"()Ljava/lang/String;",
														"Ljava/lang/Object;", "toString",
														Dispatch.VIRTUAL));
									w.emit(GetInstruction.make("Ljava/io/PrintStream;",
											"Ljava/lang/System;", "err", true));
									w.emit(SwapInstruction.make());
									w.emit(InvokeInstruction.make("(Ljava/lang/String;)V",
											"Ljava/io/PrintStream;", "println", Dispatch.VIRTUAL));
								} else if (type.startsWith("I")) {
									w.emit(DupInstruction.make(0));
									w.emit(GetInstruction.make("Ljava/io/PrintStream;",
											"Ljava/lang/System;", "err", true));
									w.emit(SwapInstruction.make());
									w.emit(InvokeInstruction.make("(I)V", "Ljava/io/PrintStream;",
											"println", Dispatch.VIRTUAL));
								} // TODO: arrays

								// w.emit(BinaryOpInstruction.make("I",BinaryOpInstruction.Operator.ADD));
								// w.emit(GetInstruction.make("Ljava/io/PrintStream;","Ljava/lang/System;","err",true));
								// w.emit(SwapInstruction.make());
								// w.emit(ConstantInstruction.makeString("is there any body .... out
								// there?"));
								// how to get the types?!
								// w.emit(InvokeInstruction.make("(Ljava/lang/String;)V","Ljava/io/PrintStream;","println",Dispatch.VIRTUAL));
							}

						});
					}
				}
				me.applyPatches();

			}
		}

		if (ci.isChanged()) {
			ClassWriter cw = ci.emitClass();
			instrumenter.outputModifiedClass(ci, cw);
		}
	}

}
