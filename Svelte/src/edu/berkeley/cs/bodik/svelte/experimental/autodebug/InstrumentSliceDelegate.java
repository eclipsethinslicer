package edu.berkeley.cs.bodik.svelte.experimental.autodebug;

import java.util.ArrayList;
import java.util.Hashtable;

import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;

import com.ibm.wala.ipa.slicer.Statement;

import edu.berkeley.cs.bodik.svelte.Slice;
import edu.berkeley.cs.bodik.svelte.plugin.SliceEnvironment;
import edu.berkeley.cs.bodik.svelte.slicers.JavaLanguage;

public class InstrumentSliceDelegate implements IEditorActionDelegate {

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		// TODO Auto-generated method stub

	}

	public void run(IAction action) {
		if (!(SliceEnvironment.getSlice() != null && JavaLanguage.useShrike))
			return;

		// STEP 1: go thru slice, record class files we need to update. (make sure no java.*
		// protected stuff)
		Hashtable<String, ArrayList<Statement>> classesToStatements = new Hashtable<String, ArrayList<Statement>>();
		Slice slice = SliceEnvironment.getSlice();
		for (Statement s : slice) {
			String klass;
			klass = s.getNode().getMethod().getDeclaringClass().getName().toString();
			ArrayList<Statement> statements = classesToStatements.get(klass);
			if (statements == null) {
				statements = new ArrayList<Statement>();
				classesToStatements.put(klass, statements);
			}
			statements.add(s);
		}

		// TODO: have to strip L?

		// STEP 2: get actual locations of these
		ArrayList<String> inputFilenames = new ArrayList<String>();
		for (String klass : classesToStatements.keySet())
			; // find binary file
		// inputFilenames.add(EclipseUtils.findBinaryFileForClassStripL(klass,SliceEnvironment.getCurrent().getJavaProject()));

		// STEP 3: make a temporary directory to hold replacements
		// just use /tmp/svelte-autodebug.jar
		String outputFilename = "/tmp/svelte-autodebug.jar";

		// STEP 4: use Shrike to convert these, adding instrumentation
		try {
			// Instrumenter.instrument(inputFilenames, outputFilename);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// STEP 5: find launcher for project, copy it, and change class path

		// STEP 6: run launcher? or tell user.

		// STEP 7: gather data

		// STEP 8: compile data and use it.

	}

	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub

	}

}
