package edu.berkeley.cs.bodik.svelte.experimental.autodebug;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;

import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.ParamCaller;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SSAReturnInstruction;

public class InstrumentationSet {
	HashSet<String> fullyQualifiedClassNames;
	Hashtable<String, ArrayList<InstrumentationPoint>> methodsToIPs;

	public InstrumentationSet() {
		fullyQualifiedClassNames = new HashSet<String>();
		methodsToIPs = new Hashtable<String, ArrayList<InstrumentationPoint>>();
	}

	private void addInstrumentationPoint(String methodSignature, InstrumentationPoint ip) {
		methodSignature = methodSignature.replace("/", ".");
		ArrayList<InstrumentationPoint> methodsIPs = methodsToIPs.get(methodSignature);
		if (methodsIPs == null) {
			methodsIPs = new ArrayList<InstrumentationPoint>();
			methodsToIPs.put(methodSignature, methodsIPs);
		}
		methodsIPs.add(ip);
	}

	public boolean toBeInstrumented(String methodSignature) {
		return methodsToIPs.containsKey(methodSignature.replace("/", "."));
	}

	public ArrayList<InstrumentationPoint> instrumentationPointsForMethod(String methodSignature) {
		return methodsToIPs.get(methodSignature.replace("/", "."));
	}

	public void addInstrumentationPoint(Statement statement) {
		InstrumentationPoint newIP = null;
		if (statement instanceof NormalStatement) {
			SSAInstruction instr = ((NormalStatement) statement).getInstruction();
			if (instr instanceof SSAPutInstruction || instr instanceof SSAReturnInstruction)
				newIP = new InstrumentationPoint.PrintTopOfStackIP(statement);

		} else if (statement instanceof ParamCaller) {
			newIP = new InstrumentationPoint.ParamCallerIP(statement);
		}
		if (newIP != null) {
			addInstrumentationPoint(statement.getNode().getMethod().getSignature(), newIP);
			String classname = statement.getNode().getMethod().getDeclaringClass().getName()
					.toString();
			fullyQualifiedClassNames.add(classname);
		}
	}

	public Collection<String> getFullyQualifiedClassNames() {
		return fullyQualifiedClassNames;
	}
}
