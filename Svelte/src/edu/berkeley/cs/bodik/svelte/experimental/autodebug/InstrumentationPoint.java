package edu.berkeley.cs.bodik.svelte.experimental.autodebug;

import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.ParamCaller;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.StatementWithInstructionIndex;
import com.ibm.wala.shrikeBT.ConstantInstruction;
import com.ibm.wala.shrikeBT.MethodEditor;
import com.ibm.wala.shrikeBT.DupInstruction;
import com.ibm.wala.shrikeBT.GetInstruction;
import com.ibm.wala.shrikeBT.InvokeInstruction;
import com.ibm.wala.shrikeBT.MethodEditor;
import com.ibm.wala.shrikeBT.SwapInstruction;
import com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch;
import com.ibm.wala.shrikeBT.MethodEditor.Output;
import com.ibm.wala.shrikeBT.MethodEditor.Patch;

public abstract class InstrumentationPoint {
	protected Statement statement;
	protected int instructionIndex;

	public InstrumentationPoint(Statement s) {
		super();
		this.statement = s;
		assert (s instanceof NormalStatement || s instanceof StatementWithInstructionIndex);
		if (s instanceof NormalStatement)
			this.instructionIndex = ((NormalStatement) s).getInstructionIndex();
		else if (s instanceof StatementWithInstructionIndex)
			this.instructionIndex = ((StatementWithInstructionIndex) s).getInstructionIndex();
	}

	public int getInstructionIndex() {
		return instructionIndex;
	}

	abstract public void instrument(MethodEditor me, String stackTypes[]);

	static class PrintTopOfStackIP extends InstrumentationPoint {
		public PrintTopOfStackIP(Statement s) {
			super(s);
		}

		@Override
		public void instrument(MethodEditor me, String stackTypes[]) {
			final String type = stackTypes[0];
			me.insertBefore(instructionIndex, new Patch() {

				@Override
				public void emitTo(Output w) {
					System.out
							.println("type of top of stack at " + instructionIndex + "is " + type);

					if (type.startsWith("L")) {
						w.emit(GetInstruction.make("Ljava/io/PrintStream;", "Ljava/lang/System;",
								"err", true));
						w.emit(ConstantInstruction.makeString(statement.getNode().getMethod()
								.getSignature()
								+ ":" + " "));
						w.emit(InvokeInstruction.make("(Ljava/lang/String;)V",
								"Ljava/io/PrintStream;", "print", Dispatch.VIRTUAL));
						w.emit(DupInstruction.make(0));
						if (!type.equals("Ljava/lang/String;"))
							w.emit(InvokeInstruction.make("()Ljava/lang/String;",
									"Ljava/lang/Object;", "toString", Dispatch.VIRTUAL));
						w.emit(GetInstruction.make("Ljava/io/PrintStream;", "Ljava/lang/System;",
								"err", true));
						w.emit(SwapInstruction.make());
						w.emit(InvokeInstruction.make("(Ljava/lang/String;)V",
								"Ljava/io/PrintStream;", "println", Dispatch.VIRTUAL));
					} else if (type.startsWith("I")) {
						w.emit(DupInstruction.make(0));
						w.emit(GetInstruction.make("Ljava/io/PrintStream;", "Ljava/lang/System;",
								"err", true));
						w.emit(SwapInstruction.make());
						w.emit(InvokeInstruction.make("(I)V", "Ljava/io/PrintStream;", "println",
								Dispatch.VIRTUAL));
					} // TODO: arrays
				}
			});
		}
	}

	static class ParamCallerIP extends PrintTopOfStackIP {
		public ParamCallerIP(Statement s) {
			super(s);
		}

		@Override
		public void instrument(MethodEditor me, String stackTypes[]) {
			assert (statement instanceof ParamCaller);
			ParamCaller pc = (ParamCaller) statement;

			// System.out.println ( "VALUE NUMBER!!! " + pc.getValueNumber() );
			// if ( pc.getInstruction().getNumberOfUses() != pc.getValueNumber() )
			// return; // for now, we only handle the case when we care about the last argument.

			super.instrument(me, stackTypes);

			// final String type = stackTypes[0];
			// me.insertBefore(instructionIndex, new Patch() {
			//
			// @Override
			// public void emitTo(Output w) {
			// System.out.println("type of top of stack at "+instructionIndex+"is " + type);
			//					
			//
			//
			//
			// if ( type.startsWith("L") ) {
			// w.emit(DupInstruction.make(0));
			// if ( ! type.equals("Ljava/lang/String;") )
			// w.emit(InvokeInstruction.make("()Ljava/lang/String;","Ljava/lang/Object;","toString",Dispatch.VIRTUAL));
			// w.emit(GetInstruction.make("Ljava/io/PrintStream;","Ljava/lang/System;","err",true));
			// w.emit(SwapInstruction.make());
			// w.emit(InvokeInstruction.make("(Ljava/lang/String;)V","Ljava/io/PrintStream;","println",Dispatch.VIRTUAL));
			// } else if ( type.startsWith("I") ) {
			// w.emit(DupInstruction.make(0));
			// w.emit(GetInstruction.make("Ljava/io/PrintStream;","Ljava/lang/System;","err",true));
			// w.emit(SwapInstruction.make());
			// w.emit(InvokeInstruction.make("(I)V","Ljava/io/PrintStream;","println",Dispatch.VIRTUAL));
			// } // TODO: arrays
			// }
			// });
		}
	}
}