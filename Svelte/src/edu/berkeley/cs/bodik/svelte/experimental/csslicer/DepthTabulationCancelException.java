/*******************************************************************************
 * Copyright (c) 2007 IBM Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package edu.berkeley.cs.bodik.svelte.experimental.csslicer;

import com.ibm.wala.eclipse.util.CancelException;

import edu.berkeley.cs.bodik.svelte.experimental.csslicer.DepthTabulationSolver.Result;

/**
 * A {@link CancelException} thrown during tabulation; holds a pointer to a partial {@link Result}.
 * Use with care, this can hold on to a lot of memory.
 * 
 * @author sjfink
 * 
 * This is a copied version for Svelte so I can play around with depth/graph structure in the
 * Tabulation Algorithm.
 */
public class DepthTabulationCancelException extends CancelException {

	private final Result result;

	protected DepthTabulationCancelException(CancelException cause, Result r) {
		super(cause);
		this.result = r;
	}

	public Result getResult() {
		return result;
	}

}
