package edu.berkeley.cs.bodik.svelte.experimental.autodebug;

import java.io.IOException;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.ShrikeCTMethod;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.shrikeBT.DupInstruction;
import com.ibm.wala.shrikeBT.GetInstruction;
import com.ibm.wala.shrikeBT.Instruction;
import com.ibm.wala.shrikeBT.InvokeInstruction;
import com.ibm.wala.shrikeBT.MethodEditor;
import com.ibm.wala.shrikeBT.SwapInstruction;
import com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch;
import com.ibm.wala.shrikeBT.MethodEditor.Output;
import com.ibm.wala.shrikeBT.MethodEditor.Patch;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.SSAInstruction;

import edu.berkeley.cs.bodik.svelte.CallGraphUtils;
import edu.berkeley.cs.bodik.svelte.SvelteAnalysisEngine;

public class ShrikeAutoDebugging {
	public static void main(String args[]) {
		SvelteAnalysisEngine se = new SvelteAnalysisEngine();
		try {
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/resources.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/rt.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/jsse.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/jce.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/charsets.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/sunjce_provider.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/dnsns.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/localedata.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/sunpkcs11.jar");
			se.addClassDir("/home/evan/t/runwork/palante/autodebug/bin");
			se.findEntrypointFromClassnameOfMain("Main");

			CallGraph cg = se.getCallGraph();
			PointerAnalysis pa = se.getPointerAnalysis();
			CGNode main = CallGraphUtils.findMainMethod(cg);

			ShrikeCTMethod sctm = (ShrikeCTMethod) main.getMethod();
			IClass klass = sctm.getDeclaringClass();
			// klass.getClassLoader().

			// MethodEditor me = new
			// MethodEditor(sctm.getInstructions(),sctm.getHandlers(),sctm.getBytecodeMap());
			// testing, DONT USE ME
			MethodEditor me = new MethodEditor(sctm.getInstructions(), sctm.getHandlers(), null);

			me.beginPass();
			me.insertBefore(13, new Patch() {

				@Override
				public void emitTo(Output w) {
					// w.emit(DupInstruction.make(0));
					// w.emit(GetInstruction.make("Ljava/io/PrintStream;","LJava/lang/System;","out",true));
					// w.emit(SwapInstruction.make());
					// w.emit(InvokeInstruction.make("(I)V","Ljava/io/PrintStream;","println",Dispatch.VIRTUAL));
					// w.emit(GetInstruction.make("Ljava/io/PrintStream;","Ljava/lang/System;","out",true));
					// w.emit(ConstantInstruction.makeString("is there any body .... out there?"));
					// w.emit(InvokeInstruction.make("(Ljava/lang/String;)V","Ljava/io/PrintStream;","println",Dispatch.VIRTUAL));
					w.emit(DupInstruction.make(0));
					w.emit(InvokeInstruction.make("()Ljava/lang/String;", "LMain;", "toString",
							Dispatch.VIRTUAL));
					w.emit(GetInstruction.make("Ljava/io/PrintStream;", "Ljava/lang/System;",
							"err", true));
					w.emit(SwapInstruction.make());
					w.emit(InvokeInstruction.make("(Ljava/lang/String;)V", "Ljava/io/PrintStream;",
							"println", Dispatch.VIRTUAL));
				}
			});
			me.insertBefore(16, new Patch() {

				@Override
				public void emitTo(Output w) {
					// w.emit(DupInstruction.make(0));
					// w.emit(GetInstruction.make("Ljava/io/PrintStream;","LJava/lang/System;","out",true));
					// w.emit(SwapInstruction.make());
					// w.emit(InvokeInstruction.make("(I)V","Ljava/io/PrintStream;","println",Dispatch.VIRTUAL));
					// w.emit(GetInstruction.make("Ljava/io/PrintStream;","Ljava/lang/System;","out",true));
					// w.emit(ConstantInstruction.makeString("is there any body .... out there?"));
					// w.emit(InvokeInstruction.make("(Ljava/lang/String;)V","Ljava/io/PrintStream;","println",Dispatch.VIRTUAL));
					w.emit(DupInstruction.make(0));
					w.emit(InvokeInstruction.make("()Ljava/lang/String;", "LMain;", "toString",
							Dispatch.VIRTUAL));
					w.emit(GetInstruction.make("Ljava/io/PrintStream;", "Ljava/lang/System;",
							"err", true));
					w.emit(SwapInstruction.make());
					w.emit(InvokeInstruction.make("(Ljava/lang/String;)V", "Ljava/io/PrintStream;",
							"println", Dispatch.VIRTUAL));
				}
			});
			me.applyPatches();
			me.endPass();

			int j = 0;
			for (SSAInstruction i : main.getIR().getInstructions()) {
				try {
					System.out.println(j++ + ": " + i + "; bcindex=" + sctm.getBytecodeIndex(j));
				} catch (Exception e) {
					System.out.println("NOT THIS TIME!!!");
				}
			}

			j = 0;
			// for (Instruction i: sctm.getInstructions() ) {
			for (Instruction i : me.getInstructions()) {
				System.out.println(j++ + ": " + i);
				if (i instanceof GetInstruction)
					System.out.println(((GetInstruction) i).getFieldType() + "!!!"
							+ ((GetInstruction) i).getClassType() + "!!!"
							+ ((GetInstruction) i).getFieldName());
				if (i instanceof InvokeInstruction)
					System.out.println(((InvokeInstruction) i).getClassType() + "@@@"
							+ ((InvokeInstruction) i).getMethodName());

			}

			// System.out.println(m.getClass().getName());
			// System.out.println(main.getIR());

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidClassFileException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
