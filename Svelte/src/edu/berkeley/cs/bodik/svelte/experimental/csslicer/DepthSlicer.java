/*******************************************************************************
 * Copyright (c) 2006 IBM Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package edu.berkeley.cs.bodik.svelte.experimental.csslicer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.Stack;

import com.ibm.wala.dataflow.IFDS.BackwardsSupergraph;
import com.ibm.wala.dataflow.IFDS.IFlowFunctionMap;
import com.ibm.wala.dataflow.IFDS.IMergeFunction;
import com.ibm.wala.dataflow.IFDS.ISupergraph;
import com.ibm.wala.dataflow.IFDS.TabulationDomain;
import com.ibm.wala.dataflow.IFDS.TabulationProblem;
import com.ibm.wala.dataflow.IFDS.TabulationResult;
import com.ibm.wala.eclipse.util.CancelException;
import com.ibm.wala.ipa.slicer.ISDG;
import com.ibm.wala.ipa.slicer.PDG;
import com.ibm.wala.ipa.slicer.SDG;
import com.ibm.wala.ipa.slicer.SDGView;
import com.ibm.wala.ipa.slicer.SliceFunctions;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.util.collections.HashSetFactory;
import com.ibm.wala.util.collections.Iterator2Collection;
import com.ibm.wala.util.collections.Pair;
import com.ibm.wala.util.debug.Assertions;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.util.intset.SparseIntSet;

/**
 * A demand-driven context-sensitive slicer.
 * 
 * This computes a context-sensitive slice, building an SDG and finding realizable paths to a
 * statement using tabulation.
 * 
 * This implementation uses a preliminary pointer analysis to compute data dependence between heap
 * locations in the SDG.
 * 
 * @author sjfink
 * 
 * This is a copied version for Svelte so I can play around with depth/graph structure in the
 * Tabulation Algorithm.
 * 
 * Currently NOT WORKING
 */
public class DepthSlicer {

	public final static boolean DEBUG = false;

	public final static boolean VERBOSE = false;

	/*
	 * Experimental option: If BAIL_OUT > 0, then the slicer will stop tabulating when the slice
	 * gets bigger than this.
	 */
	public final static int BAIL_OUT = -1;

	public static Pair<Collection<Statement>, Graph<Statement>> computeBackwardsSliceAndDepGraph(
			SDG sdg, Collection<Statement> ss, int depth) throws IllegalArgumentException,
			CancelException {
		return new DepthSlicer().slice(sdg, ss, true, depth);
	}

	private Graph<Statement> getDepGraph() {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Main driver logic.
	 * 
	 * @param depth
	 */
	public Pair<Collection<Statement>, Graph<Statement>> slice(SDG sdg, Collection<Statement> ss,
			boolean backward, int depth) throws CancelException {

		if (sdg == null) {
			throw new IllegalArgumentException("sdg cannot be null");
		}

		Collection<Statement> rootsConsidered = HashSetFactory.make();
		Stack<Statement> workList = new Stack<Statement>();
		Collection<Statement> result = HashSetFactory.make();

		Graph<Statement> superDepGraphResult = null;
		final ArrayList<Graph<Statement>> depGraphs = new ArrayList<Graph<Statement>>();

		for (Statement s : ss) {
			workList.push(s);
			System.out.println("***1seed*** " + s);
		}
		while (!workList.isEmpty()) {
			Statement root = workList.pop();
			System.out.println("***1worklist*** " + root);
			rootsConsidered.add(root);
			Collection<Statement> empty = Collections.emptySet();
			ISDG sdgView = new SDGView(sdg, empty);
			SliceProblem p = makeSliceProblem(root, sdgView, backward);

			if (VERBOSE) {
				System.err.println("worklist now: " + workList.size());
				System.err.println("slice size: " + result.size());
				System.err.println("Tabulate for " + root);
			}

			DepthTabulationSolver<Statement, PDG> solver = DepthTabulationSolver.make(p, depth);
			TabulationResult<Statement, PDG> tr = solver.solve();

			if (DEBUG) {
				System.err.println("RESULT");
				System.err.println(tr);
			}
			if (VERBOSE) {
				System.err.println("Tabulated.");
			}
			Collection<Statement> slice = tr.getSupergraphNodesReached();
			result.addAll(slice);

			if (VERBOSE) {
				System.err.println("Compute new roots...");
			}

			Collection<Statement> newRoots = computeNewRoots(slice, root, rootsConsidered, sdg,
					backward);
			for (Statement st : newRoots) {
				workList.push(st);
			}

			if (BAIL_OUT > 0 && result.size() > BAIL_OUT) {
				workList.clear();
				System.err.println("Bailed out at " + result.size());
			}

			depGraphs.add(solver.getDepGraph());
		}

		if (VERBOSE) {
			System.err.println("Slicer done.");
		}

		superDepGraphResult = new Graph<Statement>() {
			public void removeNodeAndEdges(Statement N) throws UnsupportedOperationException {
			}

			public void addNode(Statement n) {
			}

			public boolean containsNode(Statement N) {
				return false;
			}

			public int getNumberOfNodes() {
				return 0;
			}

			public Iterator<Statement> iterator() {
				return null;
			}

			public void removeNode(Statement n) {
			}

			public void addEdge(Statement src, Statement dst) {
			}

			public int getPredNodeCount(Statement N) {
				return 0;
			}

			public Iterator<? extends Statement> getPredNodes(Statement N) {
				return null;
			}

			public int getSuccNodeCount(Statement N) {
				return 0;
			}

			public boolean hasEdge(Statement src, Statement dst) {
				return false;
			}

			public void removeAllIncidentEdges(Statement node) throws UnsupportedOperationException {
			}

			public void removeEdge(Statement src, Statement dst)
					throws UnsupportedOperationException {
			}

			public void removeIncomingEdges(Statement node) throws UnsupportedOperationException {
			}

			public void removeOutgoingEdges(Statement node) throws UnsupportedOperationException {
			}

			public Iterator<? extends Statement> getSuccNodes(final Statement N) {
				return new Iterator<Statement>() {
					Statement nextStatement = null;
					int depGraphIndex = -1;
					Iterator<? extends Statement> currentIter = null;

					public boolean hasNext() {
						if (depGraphIndex == -1)
							return generateNext();
						return ((currentIter != null && currentIter.hasNext()) || depGraphIndex + 1 < depGraphs
								.size());
					}

					private boolean generateNext() {
						if (currentIter != null && currentIter.hasNext()) {
							nextStatement = currentIter.next();
							return true;
						} else {
							currentIter = null;
							while (depGraphIndex + 1 < depGraphs.size()) {
								depGraphIndex++;
								currentIter = depGraphs.get(depGraphIndex).getSuccNodes(N);
								if (currentIter != null) {
									nextStatement = currentIter.next();
									return true;
								}
							}
							return false;
						}
					}

					public Statement next() {
						if (depGraphIndex == -1)
							generateNext();
						Statement result = nextStatement;
						generateNext();
						return result;
					}

					public void remove() {
					}
				};
			}

		};
		return Pair.make(result, superDepGraphResult);
	}

	/**
	 * Return an object which encapsulates the tabulation logic for the slice problem. Subclasses
	 * can override this method to implement special semantics.
	 */
	protected SliceProblem makeSliceProblem(Statement root, ISDG sdgView, boolean backward) {
		return new SliceProblem(root, sdgView, backward);
	}

	public static Collection<Statement> computeNewRoots(Collection<Statement> slice,
			Statement root, Collection<Statement> rootsConsidered, ISDG sdg, boolean backward) {
		if (backward) {
			return computeNewBackwardRoots(slice, root, rootsConsidered, sdg);
		} else {
			return computeNewForwardRoots(slice, root, rootsConsidered, sdg);
		}
	}

	/**
	 * TODO: generalize this for any unbalanced parentheses problems
	 */
	private static Collection<Statement> computeNewForwardRoots(Collection<Statement> slice,
			Statement root, Collection<Statement> rootsConsidered, ISDG sdg) {
		Collection<Statement> result = HashSetFactory.make();

		for (Statement st : slice) {
			if (st.getNode().equals(root.getNode())) {
				switch (st.getKind()) {
				case HEAP_RET_CALLEE:
				case NORMAL_RET_CALLEE:
				case EXC_RET_CALLEE:
					Collection<Statement> succs = Iterator2Collection.toCollection(sdg
							.getSuccNodes(st));
					succs.removeAll(slice);
					for (Statement s : succs) {
						// s is a statement that is a successor of a return statement to the
						// root
						// method of the slice.
						// normally we expect s to be in the slice ... since it wasn't, it
						// must have been ruled out by balanced parens since we "magically"
						// entered the root method. So, consider s a new "magic root"
						if (!rootsConsidered.contains(s)) {
							if (VERBOSE) {
								System.err.println("Adding root " + s);
							}
							result.add(s);
						}
					}
					break;
				default:
					// do nothing
					break;
				}
			}
		}
		return result;
	}

	/**
	 * TODO: generalize this for any unbalanced parentheses problems
	 */
	private static Collection<Statement> computeNewBackwardRoots(Collection<Statement> slice,
			Statement root, Collection<Statement> rootsConsidered, ISDG sdg) {
		Collection<Statement> result = HashSetFactory.make();

		for (Statement st : slice) {
			if (st.getNode().equals(root.getNode())) {
				switch (st.getKind()) {
				case HEAP_PARAM_CALLEE:
				case PARAM_CALLEE:
				case METHOD_ENTRY:
					Collection<Statement> preds = Iterator2Collection.toCollection(sdg
							.getPredNodes(st));
					preds.removeAll(slice);
					for (Statement p : preds) {
						// p is a statement that is a predecessor of an incoming parameter
						// to the root
						// method of the slice.
						// normally we expect p to be in the slice ... since it wasn't, it
						// must have been ruled out by balanced parens since we "magically"
						// entered the root method. So, consider p a new "magic root"
						if (!rootsConsidered.contains(p)) {
							if (VERBOSE) {
								System.err.println("Adding root " + p);
							}
							result.add(p);
						}
					}
					break;
				default:
					// do nothing
					break;
				}
			}
		}
		return result;
	}

	/**
	 * Tabulation problem representing slicing
	 * 
	 */
	public static class SliceProblem implements TabulationProblem<Statement, PDG> {

		private final Statement src;

		private final ISupergraph<Statement, PDG> supergraph;

		private final IFlowFunctionMap<Statement> f;

		public SliceProblem(Statement s, ISDG sdg, boolean backward) {
			this.src = s;
			SDGSupergraph forwards = new SDGSupergraph(sdg, src, backward);
			this.supergraph = backward ? BackwardsSupergraph.make(forwards) : forwards;
			f = new SliceFunctions();
		}

		/*
		 * @see com.ibm.wala.dataflow.IFDS.TabulationProblem#getDomain()
		 */
		public TabulationDomain<Statement> getDomain() {
			Assertions.UNREACHABLE();
			return null;
		}

		/*
		 * @see com.ibm.wala.dataflow.IFDS.TabulationProblem#getFunctionMap()
		 */
		public IFlowFunctionMap<Statement> getFunctionMap() {
			return f;
		}

		/*
		 * @see com.ibm.wala.dataflow.IFDS.TabulationProblem#getMergeFunction()
		 */
		public IMergeFunction getMergeFunction() {
			return null;
		}

		/*
		 * @see com.ibm.wala.dataflow.IFDS.TabulationProblem#getReachableOnEntry()
		 */
		public SparseIntSet getReachableOnEntry() {
			return SparseIntSet.singleton(0);
		}

		/*
		 * @see com.ibm.wala.dataflow.IFDS.TabulationProblem#getSupergraph()
		 */
		public ISupergraph<Statement, PDG> getSupergraph() {
			return supergraph;
		}
	}

}
