//package edu.berkeley.cs.bodik.svelte.experimental.autodebug;

//import java.io.IOException;
//import java.io.PrintWriter;

//import com.ibm.wala.shrikeBT.BinaryOpInstruction;
//import com.ibm.wala.shrikeBT.Constants;
//import com.ibm.wala.shrikeBT.Disassembler;
//import com.ibm.wala.shrikeBT.DupInstruction;
//import com.ibm.wala.shrikeBT.GetInstruction;
//import com.ibm.wala.shrikeBT.InvokeInstruction;
//import com.ibm.wala.shrikeBT.MethodData;
//import com.ibm.wala.shrikeBT.MethodEditor;
//import com.ibm.wala.shrikeBT.SwapInstruction;
//import com.ibm.wala.shrikeBT.BinaryOpInstruction.Operator;
//import com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch;
//import com.ibm.wala.shrikeBT.MethodEditor.Output;
//import com.ibm.wala.shrikeBT.MethodEditor.Patch;
//import com.ibm.wala.shrikeBT.analysis.Verifier;
//import com.ibm.wala.shrikeBT.analysis.Analyzer.FailureException;
//import com.ibm.wala.shrikeBT.shrikeCT.ClassInstrumenter;
//import com.ibm.wala.shrikeBT.shrikeCT.OfflineInstrumenter;
//import com.ibm.wala.shrikeCT.ClassReader;
//import com.ibm.wala.shrikeCT.ClassWriter;
//import com.ibm.wala.shrikeCT.InvalidClassFileException;

//public class ShrikeClassRewriter {

//public static void main(String args[]) {
//try {
//String fakeArgs[] = { "bogus", "/home/evan/t/shrikeinstrumentation/old.jar", "-o", "/home/evan/t/shrikeinstrumentation/new.jar" }; 
//OfflineInstrumenter instrumenter = new OfflineInstrumenter();
//instrumenter.parseStandardArgs(fakeArgs);
//instrumenter.setPassUnmodifiedClasses(true);
//instrumenter.beginTraversal();
//ClassInstrumenter ci;
//while ((ci = instrumenter.nextClass()) != null) {
//if ( ci.getReader().getName().equals("Main") )
//doClass(ci,instrumenter);
//}
//instrumenter.close();
//} catch (InvalidClassFileException e) {
////TODO Auto-generated catch block
//e.printStackTrace();
//} catch (IOException e) {
////TODO Auto-generated catch block
//e.printStackTrace();
//}

//}

//private static void doClass(ClassInstrumenter ci, OfflineInstrumenter instrumenter) throws InvalidClassFileException {
////TODO Auto-generated method stub
//final String className = ci.getReader().getName();
//System.out.println("looking at "+ className);

//for (int m = 0; m < ci.getReader().getMethodCount(); m++) {
//MethodData d = ci.visitMethod(m);
//System.out.println("kaputt method " + d.getName() + d.getSignature());

//try {
//(new Disassembler(d)).disassembleTo(new PrintWriter(System.out));
//System.out.println("ok");
//} catch (Exception e) {e.printStackTrace();}

//Verifier v = new Verifier(d);
//try {
//v.verify();
//} catch (FailureException e1) {
////TODO Auto-generated catch block
//e1.printStackTrace();
//}

//if ( d.getName().equals("main") && d.getSignature().equals("[Ljava/lang/String;)V" ) ) {
//System.out.println("messing with main");
//MethodEditor me = new MethodEditor(d);
//me.beginPass();
//me.insertBefore(13, new Patch() {

//@Override
//public void emitTo(Output w) {
//w.emit(DupInstruction.make(0));
//w.emit(BinaryOpInstruction.make("I",Operator.ADD));
////w.emit(GetInstruction.make("Ljava/io/PrintStream;","LJava/lang/System;","out",true));
////w.emit(SwapInstruction.make());
////w.emit(InvokeInstruction.make("V","Ljava/io/PrintStream;","println",Dispatch.VIRTUAL));
//}

//});
//me.applyPatches();
//me.endPass();

//try {
//(new Disassembler(d)).disassembleTo(new PrintWriter(System.out));
//} catch (Exception e) {e.printStackTrace();}
//}
//}
//if (ci.isChanged()) {
//ClassWriter cw = ci.emitClass();
////cw.addField(ClassReader.ACC_PUBLIC | ClassReader.ACC_STATIC, fieldName, Constants.TYPE_boolean, new ClassWriter.Element[0]);
//try {
//instrumenter.outputModifiedClass(ci, cw);
//} catch (IllegalStateException e) {
////TODO Auto-generated catch block
//e.printStackTrace();
//} catch (IOException e) {
////TODO Auto-generated catch block
//e.printStackTrace();
//}
//}
//}
//}
/*******************************************************************************
 * Copyright (c) 2002,2006 IBM Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package edu.berkeley.cs.bodik.svelte.experimental.autodebug;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.PrintStream;
import java.io.Writer;
import java.util.ArrayList;
import java.util.BitSet;

import com.ibm.wala.shrikeBT.Instruction;
import com.ibm.wala.shrikeBT.MethodData;
import com.ibm.wala.shrikeBT.MethodEditor;
import com.ibm.wala.shrikeBT.Util;
import com.ibm.wala.shrikeBT.analysis.Verifier;
import com.ibm.wala.shrikeBT.shrikeCT.ClassInstrumenter;
import com.ibm.wala.shrikeBT.shrikeCT.OfflineInstrumenter;
import com.ibm.wala.shrikeCT.ClassWriter;

/**
 * This is a demo class.
 * 
 * Class files are taken as input arguments (or if there are none, from standard input). The methods
 * in those files are instrumented: we insert a System.err.println() at ever method call, and a
 * System.err.println() at every method entry.
 * 
 * In Unix, I run it like this: java -cp ~/dev/shrike/shrike
 * com.ibm.wala.shrikeBT.shrikeCT.tools.Bench test.jar -o output.jar
 * 
 * The instrumented classes are placed in the directory "output" under the current directory.
 * Disassembled code is written to the file "report" under the current directory.
 */
public class Instrumenter {

	private static OfflineInstrumenter instrumenter;

	public static void instrument(ArrayList<String> input, String outputFilename,
			InstrumentationSet is) throws Exception {
		ArrayList<String> realargs = new ArrayList<String>();
		realargs.add("ignored"); // bogus first arg (name of program), doesn't matter
		realargs.addAll(input);
		realargs.add("-o");
		realargs.add(outputFilename);
		instrument(realargs.toArray(new String[realargs.size()]), is);
	}

	private static void instrument(String[] args, InstrumentationSet is) throws Exception {

		for (int i = 0; i < 1; i++) {
			instrumenter = new OfflineInstrumenter();

			Writer w = new BufferedWriter(new FileWriter("report", false));

			args = instrumenter.parseStandardArgs(args);
			instrumenter.setPassUnmodifiedClasses(true);
			instrumenter.beginTraversal();
			ClassInstrumenter ci;
			while ((ci = instrumenter.nextClass()) != null) {
				doClass(ci, w, is);
			}
			instrumenter.close();
		}
	}

	static final String fieldName = "_Bench_enable_trace";

	// Keep these commonly used instructions around
	static final Instruction getSysErr = Util.makeGet(System.class, "err");
	static final Instruction callPrintln = Util.makeInvoke(PrintStream.class, "println",
			new Class[] { String.class });

	private static void doClass(final ClassInstrumenter ci, Writer w, InstrumentationSet is)
			throws Exception {
		final String className = ci.getReader().getName();

		for (int m = 0; m < ci.getReader().getMethodCount(); m++) {
			MethodData d = ci.visitMethod(m);

			// d could be null, e.g., if the method is abstract or native
			if (d != null) {

				MethodEditor me = new MethodEditor(d);
				me.beginPass();
				String fullSig = className + "." + d.getName() + d.getSignature();
				if (is.toBeInstrumented(fullSig)) {
					// System.out.println("messing with main");

					Verifier v = new Verifier(d);
					BitSet all = new BitSet(d.getInstructions().length);

					for (InstrumentationPoint ip : is.instrumentationPointsForMethod(fullSig))
						all.set(ip.getInstructionIndex(), true);
					v.computeTypes(null, all, false);

					for (InstrumentationPoint ip : is.instrumentationPointsForMethod(fullSig)) {
						final String stackTypes[] = v.getStackTypes()[ip.getInstructionIndex()];
						ip.instrument(me, stackTypes);
					}
				}
				me.applyPatches();

			}
		}

		if (ci.isChanged()) {
			ClassWriter cw = ci.emitClass();
			instrumenter.outputModifiedClass(ci, cw);
		}
	}

}
