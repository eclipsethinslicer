package edu.berkeley.cs.bodik.svelte.experimental.agent;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.instrument.IllegalClassFormatException;
import java.lang.instrument.Instrumentation;

import java.lang.instrument.ClassFileTransformer;
import java.security.ProtectionDomain;
import java.util.BitSet;

import com.ibm.wala.shrikeBT.ConstantInstruction;
import com.ibm.wala.shrikeBT.DupInstruction;
import com.ibm.wala.shrikeBT.GetInstruction;
import com.ibm.wala.shrikeBT.InvokeInstruction;
import com.ibm.wala.shrikeBT.MethodData;
import com.ibm.wala.shrikeBT.MethodEditor;
import com.ibm.wala.shrikeBT.SwapInstruction;
import com.ibm.wala.shrikeBT.IInvokeInstruction.Dispatch;
import com.ibm.wala.shrikeBT.MethodEditor.Output;
import com.ibm.wala.shrikeBT.MethodEditor.Patch;
import com.ibm.wala.shrikeBT.analysis.Verifier;
import com.ibm.wala.shrikeBT.shrikeCT.ClassInstrumenter;
import com.ibm.wala.shrikeCT.ClassWriter;
import com.ibm.wala.shrikeCT.InvalidClassFileException;

public class InstrumenterAgent {
	public static void premain(String agentArgs, Instrumentation inst) {
		inst.addTransformer(new SvelteClassFileTransformer());
	}
}

class SvelteClassFileTransformer implements ClassFileTransformer {

	@Override
	public byte[] transform(ClassLoader arg0, String arg1, Class<?> arg2, ProtectionDomain arg3,
			byte[] arg4) throws IllegalClassFormatException {

		Writer w = null;
		try {
			w = new BufferedWriter(new FileWriter("report", false));
		} catch (IOException e1) {
			e1.printStackTrace();
		}


		try {
			ClassInstrumenter ci = new ClassInstrumenter(arg4);
			return doClass(ci,w,
					"net.sourceforge.ganttproject.time.gregorian.TimeFrameImpl.fillLine(Lnet.sourceforge.ganttproject.time.gregorian.TimeFrameImpl$LineHeader;Lnet.sourceforge.ganttproject.time.gregorian.TimeFrameImpl$LineHeader;)V",
					69);
		} catch (InvalidClassFileException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return null;
	}

	private static byte[] doClass(final ClassInstrumenter ci, Writer w, String sig, final int instructionIndex)
			throws Exception {
		final String className = ci.getReader().getName();

		for (int m = 0; m < ci.getReader().getMethodCount(); m++) {
			MethodData d = ci.visitMethod(m);

			// d could be null, e.g., if the method is abstract or native
			if (d != null) {

				MethodEditor me = new MethodEditor(d);
				me.beginPass();
				final String fullSig = className + "." + d.getName() + d.getSignature();
				System.err.println("not messing with main sig "+ fullSig + " not " + sig);
				if (sig.equals(fullSig)) {
					System.err.println("messing with main");

					Verifier v = new Verifier(d);
					BitSet all = new BitSet(d.getInstructions().length);

//					for (InstrumentationPoint ip : is.instrumentationPointsForMethod(fullSig))
//						all.set(ip.getInstructionIndex(), true);
					all.set(instructionIndex,true);
					v.computeTypes(null, all, false);
					final String stackTypes[] = v.getStackTypes()[instructionIndex];
					final String type = stackTypes[0];
//
//					for (InstrumentationPoint ip : is.instrumentationPointsForMethod(fullSig)) {
//						final String stackTypes[] = v.getStackTypes()[ip.getInstructionIndex()];
//						ip.instrument(me, stackTypes);
//					}

					System.err.println("inserting before " + instructionIndex + " in " + fullSig);

					me.insertBefore(instructionIndex, new Patch() {

						@Override
						public void emitTo(Output w) {
							System.out
									.println("type of top of stack at " + instructionIndex + "is " + type);

							if (type.startsWith("L")) {
								w.emit(GetInstruction.make("Ljava/io/PrintStream;", "Ljava/lang/System;",
										"err", true));
								w.emit(ConstantInstruction.makeString(fullSig + ":" + " "));
								w.emit(InvokeInstruction.make("(Ljava/lang/String;)V",
										"Ljava/io/PrintStream;", "print", Dispatch.VIRTUAL));
								w.emit(DupInstruction.make(0));
								if (!type.equals("Ljava/lang/String;"))
									w.emit(InvokeInstruction.make("()Ljava/lang/String;",
											"Ljava/lang/Object;", "toString", Dispatch.VIRTUAL));
								w.emit(GetInstruction.make("Ljava/io/PrintStream;", "Ljava/lang/System;",
										"err", true));
								w.emit(SwapInstruction.make());
								w.emit(InvokeInstruction.make("(Ljava/lang/String;)V",
										"Ljava/io/PrintStream;", "println", Dispatch.VIRTUAL));
							} else if (type.startsWith("I")) {
								w.emit(DupInstruction.make(0));
								w.emit(GetInstruction.make("Ljava/io/PrintStream;", "Ljava/lang/System;",
										"err", true));
								w.emit(SwapInstruction.make());
								w.emit(InvokeInstruction.make("(I)V", "Ljava/io/PrintStream;", "println",
										Dispatch.VIRTUAL));
							} // TODO: arrays
						}
					});

				}
				me.applyPatches();

			}
		}

		if (ci.isChanged()) {
			ClassWriter cw = ci.emitClass();
			return cw.makeBytes();
		}
		return null;
	}

}