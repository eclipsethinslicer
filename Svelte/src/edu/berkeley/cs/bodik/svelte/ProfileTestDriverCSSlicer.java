/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

//import org.eclipse.core.runtime.IProgressMonitor;

import com.ibm.wala.cast.java.ipa.modref.AstJavaModRef;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.eclipse.util.CancelException;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.SDG;
import com.ibm.wala.ipa.slicer.Slicer;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.Slicer.ControlDependenceOptions;
import com.ibm.wala.ipa.slicer.Slicer.DataDependenceOptions;

/**
 * Test driver to figure out where to put IProgressMonitor checks, CS slicer 
 */
public class ProfileTestDriverCSSlicer {

	public static void main(String args[]) {
		SvelteAnalysisEngine se = new SvelteAnalysisEngine();

		long millis = System.currentTimeMillis();
		System.out.println("Start:" + System.currentTimeMillis());
		try {
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/resources.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/rt.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/jsse.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/jce.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/charsets.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/sunjce_provider.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/dnsns.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/localedata.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/sunpkcs11.jar");

			se.addClassDir("/home/evan/t/runwork/palante/dmn-cspa/bin/");
			se.findEntrypointFromClassnameOfMain("Main");

			// se.addJavaDir("/home/evan/t/runwork/palante/jtopas_v1/src");
			// se.findEntrypointFromClassnameOfMain("de.susebox.java.util.AbstractTokenizer");

			// se.addClassDir("/home/evan/t/runwork/palante/polyglot-2.3.0-src/classes");
			// se.findEntrypointFromClassnameOfMain("polyglot.main.Main");

			// se.addClassDir ("/home/evan/t/runwork/palante/com.ibm.wala.cast/bin");
			// se.addClassDir ("/home/evan/t/runwork/palante/com.ibm.wala.cast.java/bin");
			// se.addClassDir ("/home/evan/t/runwork/palante/com.ibm.wala.cast.js/bin");
			// se.addClassDir ("/home/evan/t/runwork/palante/com.ibm.wala.core/bin");
			// se.addClassDir ("/home/evan/t/runwork/palante/com.ibm.wala.core.tests/bin");
			// se.addClassDir ("/home/evan/t/runwork/palante/com.ibm.wala.eclipse/bin");
			// se.addClassDir ("/home/evan/t/runwork/palante/com.ibm.wala.shrike/bin");
			// se.addClassDir ("/home/evan/t/runwork/palante/Svelte/bin");
			// se.addJar ("/home/evan/t/runwork/palante/com.ibm.wala.cast.java/lib/polyglot.jar");
			// se.addJar ("/home/evan/t/runwork/palante/com.ibm.wala.cast.java/lib/java_cup.jar");
			//
			// se.findEntrypointFromClassnameOfMain("edu.berkeley.cs.bodik.svelte.NewSlicingDriver");

			// -javaagent:/home/evan/t/programs/jip/profile/profile.jar
		} catch (IOException e) {
			e.printStackTrace();
		}

		CallGraph cg = se.getCallGraph();
		PointerAnalysis pa = se.getPointerAnalysis();

		NormalStatement seed = null;

		for (CGNode n : cg) {
			IMethod m = n.getMethod();
			if (m.getDeclaringClass().getName().toString().equals("LMain")
					&& m.getName().toString().equals("main")) {
				System.out.println(n.getIR());
				seed = new NormalStatement(n, 21);
			}
		}

		System.out
				.println("-------------------------------------- cs slicing results: -------------------------");

		ArrayList<Statement> seeds = new ArrayList<Statement>();
		seeds.add(seed);
		SDG sdg = new SDG(cg, pa, new AstJavaModRef(), DataDependenceOptions.NO_BASE_PTRS,
				ControlDependenceOptions.NONE, null);
		Collection<Statement> slice;
		try {
			slice = Slicer.computeBackwardSlice(sdg, seed);
			for (Statement s : slice)
				System.out.println(s);
			System.out.println("Done:" + System.currentTimeMillis());
			System.out.println("took " + (System.currentTimeMillis() - millis));
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (CancelException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

//	public static void mmain(String args[]) {
//		long millis = System.currentTimeMillis();
//		System.out.println("progresstest:" + System.currentTimeMillis());
//
//		IProgressMonitor x = new IProgressMonitor() {
//
//			public void beginTask(String name, int totalWork) {
//				// TODO Auto-generated method stub
//
//			}
//
//			public void done() {
//				// TODO Auto-generated method stub
//
//			}
//
//			public void internalWorked(double work) {
//				// TODO Auto-generated method stub
//
//			}
//
//			boolean somevar = false;
//
//			public boolean isCanceled() {
//				// TODO Auto-generated method stub
//				if (somevar == false)
//					return false;
//				return true;
//			}
//
//			public void setCanceled(boolean value) {
//				// TODO Auto-generated method stub
//
//			}
//
//			public void setTaskName(String name) {
//				// TODO Auto-generated method stub
//
//			}
//
//			public void subTask(String name) {
//				// TODO Auto-generated method stub
//
//			}
//
//			public void worked(int work) {
//				// TODO Auto-generated method stub
//
//			}
//
//		};
//		// // MonitorKeeper.setProgressMonitor(x);
//		// boolean hello;
//		// for ( int i = 0; i < 10000000; i ++ ) {
//		// // MonitorKeeper.getProgressMonitor();
//		// MonitorUtil.throwExceptionIfCanceled();
//		// hello = (i == -1);
//		// }
//
//		System.out.println("progresstest Done:" + System.currentTimeMillis());
//		System.out.println("progresstest took " + (System.currentTimeMillis() - millis));
//
//	}
}
