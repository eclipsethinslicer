/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte;

import java.io.IOException;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.Set;

import junit.framework.Assert;

import com.ibm.wala.cast.tree.CAstSourcePositionMap.Position;
import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IClassLoader;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.eclipse.util.EclipseProjectPath;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.types.Descriptor;
import com.ibm.wala.util.collections.HashSetFactory;
import com.ibm.wala.util.debug.Assertions;
import com.ibm.wala.util.strings.Atom;

/**
 * Utilities for dealing with call graphs and call graph
 * 
 * @author Evan Battaglia
 * 
 */
public class CallGraphUtils {

	/**
	 * Find a method whose signature matches <code>([Ljava/lang/String;)V</code>, that is,
	 * <code>void main(String args[])</code>
	 * 
	 * @param cg
	 *            The callgraph to search thru. WARNING: May not look thru all nodes (see code
	 *            details)
	 * 
	 * @return The CGNode representing the method, or null if it could not be found.
	 */
	public static CGNode findMainMethod(CallGraph cg) {
		Descriptor d = Descriptor.findOrCreateUTF8("([Ljava/lang/String;)V");
		Atom name = Atom.findOrCreateUnicodeAtom("main");
		for (Iterator<? extends CGNode> it = cg.getSuccNodes(cg.getFakeRootNode()); it.hasNext();) {
			CGNode n = it.next();
			if (n.getMethod().getName().equals(name) && n.getMethod().getDescriptor().equals(d)) {
				return n;
			}
		}
		Assertions.UNREACHABLE("failed to find main() method");
		return null;
	}

	/**
	 * Find any method in a callgraph. Usually not general enough to find all methods. Don't use
	 * this, use the code from the slicer plugin (added here later hopefully) Parameters same as
	 * findMainMethod.
	 * 
	 * @param cg
	 * @param name
	 * @return
	 */
	public static CGNode findMethod(CallGraph cg, String name) {
		Atom a = Atom.findOrCreateUnicodeAtom(name);
		for (Iterator<? extends CGNode> it = cg.iterator(); it.hasNext();) {
			CGNode n = it.next();
			if (n.getMethod().getName().equals(a)) {
				return n;
			}
		}
		Assertions.UNREACHABLE("failed to find method " + name);
		return null;
	}

	/**
	 * Print (to System.out) the IR of a Call Graph.
	 * 
	 * @param cg
	 * @param assertReachable
	 * @throws IOException
	 */
	protected static void dumpIR(CallGraph cg, boolean assertReachable) throws IOException {
		Set<IMethod> unreachable = HashSetFactory.make();
		IClassHierarchy cha = cg.getClassHierarchy();
		IClassLoader sourceLoader = cha.getLoader(EclipseProjectPath.SOURCE_REF);
		for (Iterator<IClass> iter = sourceLoader.iterateAllClasses(); iter.hasNext();) {
			IClass clazz = iter.next();
			PrintStream Trace = System.out; // CHANGED by evan
			Trace.println(clazz);
			if (clazz.isInterface())
				continue;

			for (IMethod m : clazz.getDeclaredMethods()) {
				if (m.isAbstract()) {
					Trace.println(m);
				} else {
					Iterator<CGNode> nodeIter = cg.getNodes(m.getReference()).iterator();
					if (!nodeIter.hasNext()) {
						Trace.println("Method " + m.getReference() + " not reachable?");
						unreachable.add(m);
						continue;
					}
					CGNode node = nodeIter.next();
					Trace.println(node.getIR());
				}
			}
		}

		if (assertReachable) {
			Assert.assertTrue(unreachable.toString(), unreachable.isEmpty());
		}
	}

	/**
	 * Find shortest method whose range of line numbers includes linenum, in the class classname.
	 * This is useful if you are trying to find the statement on a particular line number. You can
	 * first find the method with this, then look through and find the statement matching the line
	 * number.
	 * 
	 * @param cg
	 *            A call graph.
	 * @param linenum
	 *            A line number in the file, STARTING FROM 1. If this is between a method's start
	 *            and end line numbers, the method will be returned.
	 * @param classname
	 *            A fully qualified class name (i.e. com.example.mypackage.MyClass). For the case of inner
	 *            classes, the outer/enclosing class is acceptable (i.e. Foo for Foo$Bar or Foo$1)
	 * 
	 * @return The CGNode of the method found.
	 */
	public static CGNode findMethodIncludingLineNumInClass(CallGraph cg, int linenum,
			String classname) {
		String codedclassname = "L" + classname.replace('.', '/');
		String classnameplusdollarsign = codedclassname + "$"; // the code we want may actually be in Foo$Bar or Foo$1
		// we check if starts with Foo$ (same file -- line numbers are unique within this file)
		CGNode shortest_node = null;
		int len_shortest_node = Integer.MAX_VALUE;
		for (Iterator<? extends CGNode> it = cg.iterator(); it.hasNext();) {
			CGNode n = it.next();
			IMethod m = n.getMethod();

			if (m.getDeclaringClass().getName().toString().equals(codedclassname)
					|| m.getDeclaringClass().getName().toString().startsWith(classnameplusdollarsign)) {
				Position pos = CGNodeUtils.getSourcePosition(n);
				System.out.printf("***METHOD %s line %d to %d\n", m.toString(), pos.getFirstLine(),
						pos.getLastLine());
				if (linenum >= pos.getFirstLine() && linenum <= pos.getLastLine()) {
					if ((pos.getLastLine() - pos.getFirstLine()) < len_shortest_node) {
						len_shortest_node = pos.getLastLine() - pos.getFirstLine();
						shortest_node = n;
					}
				}
			}
		}
		return shortest_node;
	}

	/**
	 * Find a method whose range of line numbers includes linenum, in the file filename (absolute
	 * [?]) This is useful if you are trying to find the statement on a particular line number. You
	 * can first find the method with this, then look through and find the statement matching the
	 * line number.
	 * 
	 * @param cg
	 * @param linenum
	 * @param filename
	 * @return
	 */
	public static CGNode findMethodIncludingLineNum(CallGraph cg, int linenum,
			String filename) {
		String windowsFixedFilename = null;
		try {
			windowsFixedFilename = new URL("file:" + filename).getFile();
		} catch (MalformedURLException e) {
			Assertions.UNREACHABLE("Invalid filename in findMethodIncludingLineNum?!");
		}
		CGNode shortest_node = null;
		int len_shortest_node = Integer.MAX_VALUE;
		for (Iterator<? extends CGNode> it = cg.iterator(); it.hasNext();) {
			CGNode n = it.next();
			IMethod m = n.getMethod();

			if (m.getDeclaringClass().getSourceFileName() != null && m.getDeclaringClass().getSourceFileName().toString().equals(windowsFixedFilename)) {
				Position pos = CGNodeUtils.getSourcePosition(n);
				if ( pos != null ) {
					System.out.printf("***METHOD %s line %d to %d\n", m.toString(), pos.getFirstLine(),
							pos.getLastLine());
					if (linenum >= pos.getFirstLine() && linenum <= pos.getLastLine()) {
						if ((pos.getLastLine() - pos.getFirstLine()) < len_shortest_node) {
							len_shortest_node = pos.getLastLine() - pos.getFirstLine();
							shortest_node = n;
						}
					}
				}
			}
		}
		return shortest_node;
	}
}
