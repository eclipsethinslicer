/*******************************************************************************
 * Copyright (c) 2007 IBM Corporation.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 *     IBM Corporation - initial API and implementation
 *******************************************************************************/
package edu.berkeley.cs.bodik.svelte.cislicer;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.ibm.wala.cast.java.ipa.modref.AstJavaModRef;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.modref.DelegatingExtendedHeapModel;
import com.ibm.wala.ipa.modref.ExtendedHeapModel;
import com.ibm.wala.ipa.modref.ModRef;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.SDG;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.Slicer.ControlDependenceOptions;
import com.ibm.wala.ipa.slicer.Slicer.DataDependenceOptions;
import com.ibm.wala.util.collections.HashMapFactory;
import com.ibm.wala.util.collections.HashSetFactory;
import com.ibm.wala.util.graph.Graph;
import com.ibm.wala.util.graph.impl.GraphInverter;

/**
 * A cheap, context-insensitive slicer based on reachability over a custom SDG.
 * 
 * This is a prototype implementation; not tuned.
 * 
 * Currently supports backward slices only.
 * 
 * TODO: Introduce a slicer interface common between this and the CS slicer. TODO: This hasn't been
 * tested much. Need regression tests.
 * 
 * @author sjfink
 * 
 * This is a custom version for Svelte that supports CAst (uses AstJavaModRef)
 */
public class CISlicer {
	SDG sdg;

	/**
	 * the dependence graph used for context-insensitive slicing
	 */
	private final Graph<Statement> depGraph;

	public CISlicer(CallGraph cg, PointerAnalysis pa, DataDependenceOptions dOptions,
			ControlDependenceOptions cOptions) {
		this(cg, pa, new AstJavaModRef(), dOptions, cOptions);
	}

	public SDG getSDG() {
		return sdg;
	}

	public Graph<Statement> getDepGraph() {
		return depGraph;
	}

	public CISlicer(CallGraph cg, PointerAnalysis pa, ModRef modRef,
			DataDependenceOptions dOptions, ControlDependenceOptions cOptions)
			throws IllegalArgumentException {
		if (dOptions == null) {
			throw new IllegalArgumentException("dOptions == null");
		}
		if (dOptions.equals(DataDependenceOptions.NO_BASE_PTRS)
				|| dOptions.equals(DataDependenceOptions.FULL)) {
			throw new IllegalArgumentException("Heap data dependences requested in CISlicer!");
		}

		sdg = new SDG(cg, pa, modRef, dOptions, cOptions, null);

		Map<Statement, Set<PointerKey>> mod = scanForMod(sdg, pa);
		Map<Statement, Set<PointerKey>> ref = scanForRef(sdg, pa);

//		depGraph = new CISDG(sdg, mod, ref, pa);
		depGraph = GraphInverter.invert(new CISDG(sdg, mod, ref, pa));

	}

	/**
	 * 
	 * @param <T>
	 * @param G
	 * @param C
	 * @param level
	 *            depth. a value of one will only get the seeds.
	 * @return
	 */
	public static <T> Set<T> getReachableNodes(Graph<T> G, Collection<? extends T> C, int level) {
		if (C == null) {
			throw new IllegalArgumentException("C is null");
		}
		HashSet<T> result = HashSetFactory.make();
		BFSIterator<T> bfs = new BFSIterator<T>(G, C.iterator());
		System.out.println("level=" + level);
		if (level < 0) {
			while (bfs.hasNext()) {
				result.add(bfs.next());
			}
		} else {
			while (bfs.hasNext() && bfs.getLevel() < level) {
				result.add(bfs.next());
			}
		}
		return result;
	}

	// DO NOT USE, JUST A BAD IDEA. PROCESS THE SEED FIRST.
	// /**
	// * For invoke instructions, use method return statement.
	// * @param seed
	// * @return
	// */
	// public Collection<Statement> computeBackwardThinSliceMethodRet(Collection<Statement> seeds,
	// int depth) {
	//
	// ArrayList<Statement> replacements = new ArrayList<Statement>();
	// for ( Statement s: seeds ) {
	// if ( s instanceof NormalStatement ) {
	// SSAInstruction instr = ((NormalStatement)s).getInstruction();
	// boolean hasBeenReplaced = false;
	// if ( instr instanceof SSAAbstractInvokeInstruction ) {
	// PDG pdg = sdg.getPDG(s.getNode());
	// Set<Statement> rets = pdg.getCallerReturnStatements((SSAAbstractInvokeInstruction)instr);
	// for ( Statement ret: rets )
	// // TODO: figure out difference between Heap Return Caller and Normal Return Caller, and use a
	// positive test here
	// // (i.e. "if it's heap return caller or normal return caller", instead of "if it's not
	// exceptional return caller")
	// if ( ! (ret instanceof ExceptionalReturnCaller) ) {
	// replacements.add(ret);
	// hasBeenReplaced = true;
	// }
	// }
	// if ( !hasBeenReplaced )
	// replacements.add(s);
	// }
	// }
	// seeds = replacements;
	//      
	// Collection<Statement> slice = getReachableNodes(depGraph, seeds, depth);
	// return slice;
	// }

	public Collection<Statement> computeBackwardThinSlice(Statement seed) {
		Collection<Statement> slice = getReachableNodes(depGraph, Collections.singleton(seed), 1000);
		return slice;
	}

	public Collection<Statement> computeBackwardThinSlice(Collection<Statement> seeds, int depth) {
		Collection<Statement> slice = getReachableNodes(depGraph, seeds, depth);
		return slice;
	}

	/**
	 * Compute the set of pointer keys each statement mods
	 */
	private static Map<Statement, Set<PointerKey>> scanForMod(SDG sdg, PointerAnalysis pa) {
		ExtendedHeapModel h = new DelegatingExtendedHeapModel(pa.getHeapModel());
		Map<Statement, Set<PointerKey>> result = HashMapFactory.make();
		for (Iterator<? extends Statement> it = sdg.iterator(); it.hasNext();) {
			Statement st = it.next();

			switch (st.getKind()) {
			case NORMAL:
				Set<PointerKey> c = HashSetFactory.make((new AstJavaModRef()).getMod(st.getNode(),
						h, pa, ((NormalStatement) st).getInstruction(), null));
				result.put(st, c);
				break;
			}

		}
		return result;
	}

	/**
	 * Compute the set of PointerKeys each statement refs.
	 */
	private static Map<Statement, Set<PointerKey>> scanForRef(SDG sdg, PointerAnalysis pa) {
		ExtendedHeapModel h = new DelegatingExtendedHeapModel(pa.getHeapModel());
		Map<Statement, Set<PointerKey>> result = HashMapFactory.make();
		for (Iterator<? extends Statement> it = sdg.iterator(); it.hasNext();) {
			Statement st = it.next();
			switch (st.getKind()) {
			case NORMAL:
				Set<PointerKey> c = HashSetFactory.make((new AstJavaModRef()).getRef(st.getNode(),
						h, pa, ((NormalStatement) st).getInstruction(), null));
				result.put(st, c);
				break;
			}

		}
		return result;
	}

}
