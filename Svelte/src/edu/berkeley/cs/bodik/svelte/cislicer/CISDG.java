package edu.berkeley.cs.bodik.svelte.cislicer;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.ibm.wala.cast.ipa.callgraph.ScopeMappingInstanceKeys.ScopeMappingInstanceKey;
import com.ibm.wala.cast.ir.ssa.AstLexicalRead;
import com.ibm.wala.cast.ir.ssa.AstLexicalAccess.Access;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ipa.slicer.ISDG;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.PDG;
import com.ibm.wala.ipa.slicer.SDG;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.Slicer.ControlDependenceOptions;
import com.ibm.wala.util.collections.HashSetFactory;
import com.ibm.wala.util.collections.Iterator2Collection;
import com.ibm.wala.util.collections.IteratorUtil;
import com.ibm.wala.util.collections.MapUtil;
import com.ibm.wala.util.debug.Assertions;
import com.ibm.wala.util.intset.IntSet;

import edu.berkeley.cs.bodik.svelte.CGNodeUtils;

/**
 * A context-insensitive SDG. This class assumes that it is given a normal NO_HEAP SDG. It adds
 * context-insensitive heap information directly from heap stores to corresponding loads, based on
 * an underlying pointer analysis.
 * 
 * This is a custom version for Svelte that supports CAst (uses AstJavaModRef)
 */
public class CISDG implements ISDG {
	
	private final static boolean DEBUG = false;

	/**
	 * the basic SDG, without interprocedural heap edges
	 */
	final SDG noHeap;

	/**
	 * What pointer keys does each statement ref?
	 */
	private final Map<Statement, Set<PointerKey>> ref;

	/**
	 * What pointer keys does each statement ref?
	 */
	private final Map<Statement, Set<PointerKey>> mod;

	/**
	 * What statements write each pointer key?
	 */
	final Map<PointerKey, Set<Statement>> invMod;

	/**
	 * What statements ref each pointer key?
	 */
	final Map<PointerKey, Set<Statement>> invRef;

	private final PointerAnalysis pa;

	public CISDG(SDG noHeap, Map<Statement, Set<PointerKey>> mod,
			Map<Statement, Set<PointerKey>> ref, PointerAnalysis pa) {
		this.noHeap = noHeap;
		this.mod = mod;
		this.ref = ref;
		invMod = MapUtil.inverseMap(mod);
		invRef = MapUtil.inverseMap(ref);
		this.pa = pa;
	}

	public void addEdge(Statement src, Statement dst) {
		Assertions.UNREACHABLE();
		noHeap.addEdge(src, dst);
	}

	public void addNode(Statement n) {
		Assertions.UNREACHABLE();
		noHeap.addNode(n);
	}

	public boolean containsNode(Statement N) {
		Assertions.UNREACHABLE();
		return noHeap.containsNode(N);
	}

	@Override
	public boolean equals(Object obj) {
		Assertions.UNREACHABLE();
		return noHeap.equals(obj);
	}

	public ControlDependenceOptions getCOptions() {
		Assertions.UNREACHABLE();
		return noHeap.getCOptions();
	}

	public int getMaxNumber() {
		return noHeap.getMaxNumber();
	}

	public Statement getNode(int number) {
		Assertions.UNREACHABLE();
		return noHeap.getNode(number);
	}

	public int getNumber(Statement N) {
		return noHeap.getNumber(N);
	}

	public int getNumberOfNodes() {
		return noHeap.getNumberOfNodes();
	}

	public PDG getPDG(CGNode node) {
		Assertions.UNREACHABLE();
		return noHeap.getPDG(node);
	}

	public int getPredNodeCount(Statement N) {
		Assertions.UNREACHABLE();
		return noHeap.getPredNodeCount(N);
	}

	public IntSet getPredNodeNumbers(Statement node) {
		Assertions.UNREACHABLE();
		return noHeap.getPredNodeNumbers(node);
	}

	public Iterator<? extends Statement> getPredNodes(Statement N) {
		// WARNING: follow-back lexical read hack
		// CAst inner class hack
		if (N instanceof NormalStatement) {
			NormalStatement ns = (NormalStatement) N;
			if (ns.getInstruction() instanceof AstLexicalRead) {
				ArrayList<Statement> result = new ArrayList<Statement>();
				AstLexicalRead alr = (AstLexicalRead) ns.getInstruction();

				Access[] accesses = alr.getAccesses();
				// inner node & alr.getAccesses();

				for (int i = 0; i < accesses.length; i++) {
					final String definer = accesses[i].variableDefiner;
					final int vn = accesses[i].valueNumber;

					PointerKey pk = new LocalPointerKey(N.getNode(), 1);
					for (InstanceKey ik : pa.getPointsToSet(pk)) {
						if (ik instanceof ScopeMappingInstanceKey) {
							CGNode s = ((ScopeMappingInstanceKey) ik).getDefiningNode(definer);
							result.add(CGNodeUtils.findStatementFromSSAInstruction(s, s.getDU()
									.getDef(vn)));
						}
					}
				}
				return result.iterator();

				// just go back to the def and return a NormalStatement for that.
				// return new SingletonList(new NormalStatement(outernode,
				// instructionIndex)).iterator();
			}
		}
		// WARNING: end follow-back lexical read hack

		if (ref.get(N) == null) {
			return noHeap.getPredNodes(N);
		} else {
			Collection<Statement> pred = HashSetFactory.make();
			for (PointerKey p : ref.get(N)) {
				if (invMod.get(p) != null) {
					// TODO: WTF? HOW CAN IT BE NULL?
					pred.addAll(invMod.get(p));
				}
			}
			pred.addAll(Iterator2Collection.toCollection(noHeap.getPredNodes(N)));
			return pred.iterator();
		}
	}

	public int getSuccNodeCount(Statement N) {
		return IteratorUtil.count(getSuccNodes(N));
	}

	public IntSet getSuccNodeNumbers(Statement node) {
		Assertions.UNREACHABLE();
		return noHeap.getSuccNodeNumbers(node);
	}

	public Iterator<? extends Statement> getSuccNodes(Statement N) {
		if (DEBUG) {
			System.err.println("getSuccNodes " + N);
		}
		if (mod.get(N) == null) {
			return noHeap.getSuccNodes(N);
		} else {
			Collection<Statement> succ = HashSetFactory.make();
			for (PointerKey p : mod.get(N)) {
				if (invRef.get(p) != null) {
					succ.addAll(invRef.get(p));
				}
			}
			succ.addAll(Iterator2Collection.toCollection(noHeap.getSuccNodes(N)));
			return succ.iterator();
		}
	}

	public boolean hasEdge(Statement src, Statement dst) {
		Assertions.UNREACHABLE();
		return noHeap.hasEdge(src, dst);
	}

	@Override
	public int hashCode() {
		Assertions.UNREACHABLE();
		return noHeap.hashCode();

	}

	public Iterator<? extends Statement> iterateLazyNodes() {
		Assertions.UNREACHABLE();
		return noHeap.iterateLazyNodes();
	}

	public Iterator<Statement> iterator() {
		return noHeap.iterator();
	}

	public Iterator<Statement> iterateNodes(IntSet s) {
		Assertions.UNREACHABLE();
		return noHeap.iterateNodes(s);
	}

	public void removeAllIncidentEdges(Statement node) {
		Assertions.UNREACHABLE();
		noHeap.removeAllIncidentEdges(node);
	}

	public void removeEdge(Statement src, Statement dst) {
		Assertions.UNREACHABLE();
		noHeap.removeEdge(src, dst);
	}

	public void removeIncomingEdges(Statement node) {
		Assertions.UNREACHABLE();
		noHeap.removeIncomingEdges(node);
	}

	public void removeNode(Statement n) {
		Assertions.UNREACHABLE();
		noHeap.removeNode(n);
	}

	public void removeNodeAndEdges(Statement N) {
		Assertions.UNREACHABLE();
		noHeap.removeNodeAndEdges(N);
	}

	public void removeOutgoingEdges(Statement node) {
		Assertions.UNREACHABLE();
		noHeap.removeOutgoingEdges(node);
	}

	@Override
	public String toString() {
		Assertions.UNREACHABLE();
		return noHeap.toString();
	}

	public IClassHierarchy getClassHierarchy() {
		return noHeap.getClassHierarchy();
	}

}