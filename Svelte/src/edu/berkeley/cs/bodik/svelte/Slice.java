/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte;

import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

import com.ibm.wala.cast.java.loader.JavaSourceLoaderImpl.ConcreteJavaMethod;
import com.ibm.wala.classLoader.ShrikeBTMethod;
import com.ibm.wala.ipa.slicer.ExceptionalReturnCaller;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.PDG;
import com.ibm.wala.ipa.slicer.SDG;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSAInstruction;

import edu.berkeley.cs.bodik.svelte.slicers.AbstractSliceEnvironment;

/**
 * A Slice is simply a Collection of Statements. This wrapper provides some utility functions, such
 * as dumping the slice for debugging, or retrieving the starting column and line numbers.
 * 
 * @author evan
 * 
 */
public class Slice implements Iterable<Statement> {
	private Collection<Statement> statements;
	private AbstractSliceEnvironment environment;

	public Slice(Collection<Statement> statements, AbstractSliceEnvironment environment) {
		this.statements = statements;
		this.environment = environment;
	}

	public AbstractSliceEnvironment getEnvironment() {
		return environment;
	}

	public Collection<Statement> getStatements() {
		return statements;
	}

	public boolean isEmpty() {
		return this.statements == null;
	}

	/**
	 * Deletes statements from the slice if they aren't NormalStatements. Also optionally adds call
	 * site statements for calls in the slice. Then just use
	 * <code>CGNodeUtils.getSourcePositionOfInstructionIndex(s.getNode(), ((NormalStatement)s).getInstructionIndex() )</code>
	 * on each statement in the slice to get the source position.
	 * 
	 * 
	 * @param includeCallSites
	 * @return
	 */
	public void prune(boolean includeCallSites) {
		// use a HashSet to prevent duplicates.
		HashSet<Statement> newstatements = new HashSet<Statement>();

		for (Statement s : statements) {
			// ignore special kinds of statements
			if (s instanceof NormalStatement
					&& (s.getNode().getMethod() instanceof ConcreteJavaMethod || s.getNode()
							.getMethod() instanceof ShrikeBTMethod))
				newstatements.add(s);
			else {
				if (includeCallSites) {
					// calls that don't get in slice

					NormalStatement scall = StatementUtils.getCallerStatement(s);
					if (scall != null)
						newstatements.add(scall);
				}
			}
		}

		statements = newstatements;
	}

	public void dump() {
		dump(new PrintWriter(System.err));
	}

	/*
	 * from Manu: (see bookmark) private int getLineNumber(CGNode node, int index) {
	 * com.ibm.wala.classLoader.IMethod method = node.getMethod(); int bcIndex = 0; try { bcIndex =
	 * ((ShrikeCTMethodWrapper) method).getBytecodeIndex(index); } catch (InvalidClassFileException
	 * e) { e.printStackTrace(); throw new RuntimeException(); } return
	 * method.getLineNumber(bcIndex); }
	 */

	public void dump(PrintWriter w) {
		w.println("SLICE:\n");
		int i = 1;
		for (Statement s : statements) {
			String line = (i++) + "   KIND=" + s.getKind().toString() + "   " + s.toString();
			w.println(line);

			if (s.getKind() == Statement.Kind.NORMAL) {
				int bcIndex, instructionIndex = ((NormalStatement) s).getInstructionIndex();
				w.println("Normal Statement instr index=" + instructionIndex);
				try {
					bcIndex = ((ShrikeBTMethod) s.getNode().getMethod())
							.getBytecodeIndex(instructionIndex);
					w.println("Bytecode index=" + bcIndex);
					try {
						w.println(s.getNode().getMethod().getDeclaringClass());
						w.println(s.getNode().getMethod().getDeclaringClass().getSourceFileName());
						w.println(s.getNode().getMethod().getLineNumber(bcIndex));
					} catch (Exception e) {
						w.println("oops bcindex no good");
						w.println(e.getMessage());
					}
				} catch (Exception e) {
					w.println("it's prolly not a BT method");
					w.println(e.getMessage());
				}
			}

			w.println("");
			w.flush();
		}
	}

	public void dumpConcise() {
		dump(new PrintWriter(System.err));
	}

	public void dumpConcise(PrintWriter w) {
		w.println("SLICE:\n");
		// int i = 1;
		for (Statement s : statements) {
			// String line = (i++) + " KIND=" + s.getKind().toString() + " " + s;
			// w.println(line);

			// s.getNode().getMethod().getReference().getDeclaringClass().getClassLoader().equals(ClassLoaderReference.Application)
			if (s.getKind() == Statement.Kind.NORMAL) {
				int bcIndex, instructionIndex = ((NormalStatement) s).getInstructionIndex();
				w.println("Normal Statement instr index=" + instructionIndex);
				try {
					bcIndex = ((ShrikeBTMethod) s.getNode().getMethod())
							.getBytecodeIndex(instructionIndex);
					w.println("Bytecode index=" + bcIndex);
					try {
						w.println(s.getNode().getMethod().getLineNumber(bcIndex));
					} catch (Exception e) {
						w.println("oops bcindex no good");
						w.println(e.getMessage());
					}
				} catch (Exception e) {
					w.println("it's probably not a BT method");
					w.println(e.getMessage());
				}
			}

			// w.println("");
			w.flush();
		}
	}

	public Iterator<Statement> iterator() {
		return statements.iterator();
	}

	public void fixMethodSeedStatements(SDG sdg) {
		ArrayList<Statement> replacements = new ArrayList<Statement>();
		for (Statement s : statements) {
			if (s instanceof NormalStatement) {
				SSAInstruction instr = ((NormalStatement) s).getInstruction();
				boolean hasBeenReplaced = false;
				if (instr instanceof SSAAbstractInvokeInstruction) {
					PDG pdg = sdg.getPDG(s.getNode());
					Set<Statement> rets = pdg
							.getCallerReturnStatements((SSAAbstractInvokeInstruction) instr);
					for (Statement ret : rets)
						// TODO: figure out difference between Heap Return Caller and Normal Return
						// Caller, and use a positive test here
						// (i.e. "if it's heap return caller or normal return caller", instead of
						// "if it's not exceptional return caller")
						if (!(ret instanceof ExceptionalReturnCaller)) {
							replacements.add(ret);
							hasBeenReplaced = true;
						}
				}
				if (!hasBeenReplaced)
					replacements.add(s);
			}
		}
		statements = replacements;
	}
}
