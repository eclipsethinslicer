/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin.bfsliceview;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.eclipse.jface.resource.ImageDescriptor;
import org.eclipse.jface.viewers.ILabelProviderListener;
import org.eclipse.jface.viewers.ITableLabelProvider;
import org.eclipse.swt.graphics.Image;

import com.ibm.wala.ipa.slicer.NormalReturnCallee;
import com.ibm.wala.ipa.slicer.NormalReturnCaller;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.ParamCallee;
import com.ibm.wala.ipa.slicer.ParamCaller;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSABinaryOpInstruction;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SSAReturnInstruction;

import edu.berkeley.cs.bodik.svelte.CGNodeUtils;
import edu.berkeley.cs.bodik.svelte.plugin.SveltePlugin;

public class BFSliceLabelProvider implements ITableLabelProvider {

	private Map<ImageDescriptor, Image> imageCache = new HashMap<ImageDescriptor, Image>(11);
	private BFSliceContentProvider contentProvider;

	public BFSliceLabelProvider(BFSliceContentProvider contentProvider) {
		this.contentProvider = contentProvider;
	}

	/*
	 * @see ILabelProvider#getImage(Object)
	 */
	public Image getColumnImage(Object element, int column) {
		if (column != 0)
			return null;

		ImageDescriptor descriptor = null;

		if (!(element instanceof BFSliceStatement))
			return null;

		BFSliceStatement s = (BFSliceStatement) element;

		// could change this to check is has children (and let there be a concept of a statement
		// being "open" or "closed")
		if (contentProvider.inPredStack(s))
			descriptor = SveltePlugin.getImageDescriptor("icons/bfsv/downx.png");
		else if (s.isMarkedChecked())
			descriptor = SveltePlugin.getImageDescriptor("icons/bfsv/check.png");
		else {
			if (s.hasChildren())
				descriptor = SveltePlugin.getImageDescriptor("icons/bfsv/plus.png");
			else
				descriptor = SveltePlugin.getImageDescriptor("icons/bfsv/questionmark.png");
		}

		// obtain the cached image corresponding to the descriptor
		Image image = imageCache.get(descriptor);
		if (image == null) {
			image = descriptor.createImage();
			imageCache.put(descriptor, image);
		}
		return image;
	}

	public void dispose() {
		for (Iterator<Image> i = imageCache.values().iterator(); i.hasNext();) {
			(i.next()).dispose();
		}
		imageCache.clear();
	}

	public String getColumnText(Object element, int columnIndex) {
		if (element == null || !(element instanceof BFSliceStatement))
			return "";
		Statement s = ((BFSliceStatement) element).getStatement();
		if (s == null)
			return columnIndex == 0 ? element.toString() : "";

		switch (columnIndex) {
		case 0:
			return getStatementText((BFSliceStatement) element);
		case 1:
			return getFunctionText(s);
		case 2:
			return getInstructionText(s);
		default:
			return "";
		}
	}

	private String getInstructionText(Statement s) {
		if (s instanceof NormalStatement)
			return ((NormalStatement) s).getInstruction().toString();
		return s.toString();
	}

	private String getFunctionText(Statement s) {
		return s.getNode().getMethod().getDeclaringClass().getName().getClassName() + "."
				+ s.getNode().getMethod().getName() + s.getNode().getMethod().getDescriptor();
	}

	public String getStatementText(BFSliceStatement bfss) {
		Statement s = bfss.getStatement();
		Statement sparent = bfss.getParent().getStatement();
		if (s instanceof NormalReturnCaller) {
			NormalReturnCaller ss = (NormalReturnCaller) s;

			// SSAInstruction inst = ss.getCall();

			// ss.getNode().getMethod().getLocalVariableName(bcIndex, localNumber)

			return "Return value of " + ss.getInstruction().getDeclaredTarget().getName()
					+ ss.getInstruction().getDeclaredTarget().getDescriptor().toString();
		} else if (s instanceof NormalReturnCallee) {
			NormalReturnCallee ss = (NormalReturnCallee) s;

			return "Return value of "
					+ ss.getNode().getMethod().getDeclaringClass().getName().getClassName() + "."
					+ ss.getNode().getMethod().getName() + ss.getNode().getMethod().getDescriptor();
			// return "Return value of " + ss.getNode().getMethod().getSignature();
		} else if (s instanceof ParamCallee) {
			ParamCallee ss = (ParamCallee) s;

			String paramname = "value";
			String paramnames[] = ss.getNode().getIR().getLocalNames(0, ss.getValueNumber());
			if (paramnames != null && paramnames.length > 0)
				paramname = '"' + paramnames[0] + '"';

			return "Parameter " + paramname + " in "
					+ ss.getNode().getMethod().getDeclaringClass().getName().getClassName() + "."
					+ ss.getNode().getMethod().getName() + ss.getNode().getMethod().getDescriptor();
		} else if (s instanceof ParamCaller) {
			ParamCaller ss = (ParamCaller) s;

			return "Argument value in call to " + ss.getInstruction().getDeclaredTarget().getName()
					+ ss.getInstruction().getDeclaredTarget().getDescriptor().toString();
		} else if (s instanceof NormalStatement) {
			NormalStatement ss = (NormalStatement) s;
			SSAInstruction inst = ss.getInstruction();

			// uses & defs
			// int i = 0;
			// String str = "";
			// for ( SSAInstruction inst2: ss.getNode().getIR().getInstructions() ) {
			// if ( inst2 == inst ) {
			// for ( int u = 0; u < inst.getNumberOfUses(); u++ ) {
			// str += "uses ";
			// String usenames[] = ss.getNode().getIR().getLocalNames(i, inst.getUse(u));
			// if (usenames != null)
			// for (String usename: usenames)
			// str += usename+",";
			// str+="; ";
			// }
			// for ( int d = 0; d < inst.getNumberOfDefs(); d++ ) {
			// str += "defs ";
			// String defnames[] = ss.getNode().getIR().getLocalNames(i, inst.getDef(d));
			// if (defnames != null)
			// for (String defname: defnames)
			// str += defname+",";
			// str+="; ";
			// }
			// break;
			// }
			// i++;
			// }

			if (inst instanceof SSANewInstruction) {
				String result = "new "
						+ ((SSANewInstruction) inst).getNewSite().getDeclaredType().getName()
								.getClassName() + "(...)";

				if (sparent != null
						&& sparent instanceof NormalStatement
						&& ((NormalStatement) sparent).getInstruction() instanceof SSAGetInstruction) {
					result += " [default value for field \"";
					result += ((SSAGetInstruction) ((NormalStatement) sparent).getInstruction())
							.getDeclaredField().getName();
					result += "\"]";
				}

				return result;
			} else if (inst instanceof SSAGetInstruction) {
				SSAGetInstruction geti = (SSAGetInstruction) inst;
				return "get " + (geti.isStatic() ? "static \"" : "field \"")
						+ geti.getDeclaredField().getName() + "\" from \""
						+ geti.getDeclaredField().getDeclaringClass().getName().getClassName()
						+ "\" " + (geti.isStatic() ? "class" : "object");
			} else if (inst instanceof SSAPutInstruction) {
				SSAPutInstruction puti = (SSAPutInstruction) inst;
				return "put " + (puti.isStatic() ? "static \"" : "field \"")
						+ puti.getDeclaredField().getName() + "\" into \""
						+ puti.getDeclaredField().getDeclaringClass().getName().getClassName()
						+ "\" " + (puti.isStatic() ? "class" : "object");
			} else if (inst instanceof SSAReturnInstruction) {
				SSAReturnInstruction puti = (SSAReturnInstruction) inst;
				int index = CGNodeUtils.getIndexOfSSAInstruction(s.getNode(), inst);
				if (index != -1 ) {
					String uses[] = s.getNode().getIR().getLocalNames(index, puti.getUse(0));
					if (uses != null && uses.length > 0) {
						return "Return statement \"return " + uses[0] + "\"";
					}
				}
				return "Return statement";
			} else if (inst instanceof SSAAbstractInvokeInstruction) {
				SSAAbstractInvokeInstruction invi = (SSAAbstractInvokeInstruction) inst;
				return "Values of" + ((invi.isStatic() ? "" : " instance and"))
						+ " all arguments to " + invi.getDeclaredTarget().getName()
						+ invi.getDeclaredTarget().getDescriptor();
			} else if (inst instanceof SSABinaryOpInstruction) {
				SSABinaryOpInstruction bini = (SSABinaryOpInstruction) inst;
				return "Binary operation: " + bini.getOperator().toString();
			}
			return "Instr " + ss.getInstruction().toString();
		}
		// custom labels for various types of statements

		return s.toString();
	}

	public void addListener(ILabelProviderListener listener) {
	}

	public boolean isLabelProperty(Object element, String property) {
		return false;
	}

	public void removeListener(ILabelProviderListener listener) {
	}

}
