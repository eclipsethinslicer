/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin.sgview;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.slicer.NormalReturnCallee;
import com.ibm.wala.ipa.slicer.NormalReturnCaller;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.ParamCallee;
import com.ibm.wala.ipa.slicer.ParamCaller;
import com.ibm.wala.ipa.slicer.PhiStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSAArrayLoadInstruction;
import com.ibm.wala.ssa.SSAArrayStoreInstruction;
import com.ibm.wala.ssa.SSABinaryOpInstruction;
import com.ibm.wala.ssa.SSACheckCastInstruction;
import com.ibm.wala.ssa.SSAGetInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.ssa.SSAPutInstruction;
import com.ibm.wala.ssa.SSAReturnInstruction;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.graph.Graph;

import edu.berkeley.cs.bodik.svelte.Slice;

public class SliceGraph {
	private Graph<Statement> depGraph;
	private Slice seed;

	// if you want to work on performance, you could try having using CGNode for this instead (need
	// another hash for string lookups)
	Hashtable<String, Integer> classesToIndices;

	private Hashtable<CGNode, ArrayList<Statement>> clusters = null;
	private Hashtable<CGNode, Integer> clusterIndices;
	private Hashtable<Statement, Integer> statementIndices;

	class Node {
		boolean drawPredNodes = false;
	}

	class RegularNode extends Node {
		CGNode node;

		public RegularNode(CGNode node) {
			this.node = node;
		}

		@Override
		public int hashCode() {
			return node.getMethod().getSignature().hashCode();
		}

		@Override
		public boolean equals(Object xx) {
			RegularNode x;
			return (xx instanceof RegularNode) && ((x = (RegularNode) xx) != null)
					&& x.node.getMethod().getSignature().equals(node.getMethod().getSignature());
			// compare based on signature.
		}

		@Override
		public String toString() {
			return node.getMethod().getSignature();
		}
	}

	class FakeNewNode extends Node {
		TypeReference typref; // reference to the constructor function

		public FakeNewNode(TypeReference newInstType) {
			this.typref = newInstType;
		}

		@Override
		public int hashCode() {
			return typref.toString().hashCode();
		}

		@Override
		public boolean equals(Object xx) {
			FakeNewNode x;
			return (xx instanceof FakeNewNode) && ((x = (FakeNewNode) xx) != null)
					&& x.typref.toString().equals(typref.toString());
			// compare based on signature.
		}

		@Override
		public String toString() {
			return typref.toString();
		}
	}

	public SliceGraph(Graph<Statement> depGraph, Slice seed, int depth) {
		this.depGraph = depGraph;
		this.seed = seed;
	}

	public StringBuffer generateDot() {
		// NEVER fix method seed statements here. They should have already been fixed in the seed.
		clusters = new Hashtable<CGNode, ArrayList<Statement>>();
		clusterIndices = new Hashtable<CGNode, Integer>();
		statementIndices = new Hashtable<Statement, Integer>();

		ArrayList<Statement> worklist = new ArrayList<Statement>();
		HashSet<Statement> serviced = new HashSet<Statement>();
		// BFS time, baby
		worklist.addAll(seed.getStatements());

		int clusterIndex = 0;
		// do a BFS and add all statements
		int index = -1;

		int level = 0;
		int nextlevelindex = worklist.size();
		int depth = 17;

		while ((index + 1) < worklist.size()) {
			index++;
			if (index == nextlevelindex) {
				nextlevelindex = worklist.size();
				level++;
				if (level > depth)
					break;
			}

			Statement s = worklist.get(index);

			// service it
			if (serviced.contains(s))
				continue;
			serviced.add(s);

			// look thru the succ nodes, if theres any 'return val' ones or something, build the
			// bridge.
			Iterator<? extends Statement> succNodes = depGraph.getSuccNodes(s);

			ArrayList<Statement> cluster = clusters.get(s.getNode());
			if (cluster == null) {
				cluster = new ArrayList<Statement>();
				clusters.put(s.getNode(), cluster);
				clusterIndices.put(s.getNode(), new Integer(clusterIndex++));
			}
			statementIndices.put(s, cluster.size());
			cluster.add(s);

			Statement ss;
			while (succNodes.hasNext()) {
				ss = succNodes.next();
				// add succ nodes to worklist if not already serviced.
				if (!serviced.contains(ss))
					worklist.add(ss);
			}
		}
		// /////////////////////////////////////////////////

		StringBuffer sb = new StringBuffer();
		sb.append("digraph sg {\n  rankdir=RL;\n  ranksep=0.2;\n  nodesep=0.2;\n\n");

		// NODES
		for (CGNode n : clusters.keySet()) {
			int clusternum = clusterIndices.get(n);
			sb.append("  subgraph cluster");
			sb.append(clusternum);
			sb
					.append(" {\n    node [style=filled,color=gray,fillcolor=white,shape=rect,height=0,width=0];\n    style=filled;\n    fillcolor=aliceblue;\n    label=\"");
			sb.append(n.getMethod().getName().toString());
			sb.append("\";\n");

			int statementnum = 0;
			for (Statement s : clusters.get(n)) {
				sb.append("    c" + clusternum + "s" + statementnum);
				sb.append(" [label=\"");
				sb.append(getStatementLabel(s));
				sb.append("\"];\n");
				statementnum++;
			}
			sb.append("  }\n\n");
		}

		// EDGES
		for (CGNode n : clusters.keySet()) {
			int clusternum = clusterIndices.get(n);
			int statementnum = 0;
			for (Statement s : clusters.get(n)) {
				String sID = "c" + clusternum + "s" + statementnum;
				Iterator<? extends Statement> succNodes = depGraph.getSuccNodes(s);
				while (succNodes.hasNext()) {
					Statement ss = succNodes.next();
					String ssID = "c" + clusterIndices.get(ss.getNode()) + "s"
							+ statementIndices.get(ss);
					sb.append("    " + ssID + " -> " + sID + ";\n");
				}
				statementnum++;
			}
		}

		sb.append("\n}\n");

		//
		// if ( s instanceof ParamStatement.ParamCallee && ss instanceof ParamStatement.ParamCaller
		// ) {
		// // node of ss calls node of s, passing in the data as a parameter
		// // tonode = getNodeFromCGNode(((ParamStatement.ParamCallee)s).getNode());
		// // fromnode = getNodeFromCGNode(((ParamStatement.ParamCaller)ss).getNode());
		// // addEdge ( new Edge(Edge.TYPE_THRU_PARAM, fromnode,tonode), false );
		// } else if ( s instanceof ParamStatement.NormalReturnCaller && ss instanceof
		// ParamStatement.NormalReturnCallee ) {
		// // // node of s calls node of ss, gets data from node of ss as a return value
		// // tonode = getNodeFromCGNode(((ParamStatement.NormalReturnCaller)s).getNode());
		// // fromnode = getNodeFromCGNode(((ParamStatement.NormalReturnCallee)ss).getNode());
		// // addEdge ( new Edge(Edge.TYPE_THRU_RETVAL, fromnode, tonode), false );
		// } else if ( s instanceof NormalStatement && ss instanceof NormalStatement &&
		// ((NormalStatement)s).getNode() != ((NormalStatement)ss).getNode()) {
		// // ss writes to some pointerkey which s reads from, thereby passing data (possibly)
		//
		// // tonode = getNodeFromCGNode(((NormalStatement)s).getNode());
		// // fromnode = getNodeFromCGNode(((NormalStatement)ss).getNode());
		//
		// TypeReference newInstType = StatementUtils.getNewInstructionDeclaredType(ss);
		// if ( newInstType != null ) { // if it's a SSANewInstruction
		// // System.out.println("fakenewnode from " + ss + " --to-- " + s);
		// // Node fakenewnode = getOrMakeFakeNewNode(newInstType);
		// // addEdge ( new Edge(Edge.TYPE_THRU_ALIASING, fakenewnode, tonode), true ); // don't
		// count
		// duplicity for this edge
		// // addEdge ( new Edge(Edge.TYPE_THRU_PARAM, fromnode, fakenewnode), false );
		// } else {
		// // addEdge ( new Edge(Edge.TYPE_THRU_ALIASING, fromnode, tonode), false );
		// }
		// }
		//
		return sb;

		// System.out.println("! total # of nodes: "+allnodes.size());
		// System.out.println("! total # of edges: "+alledges.size());
		// System.out.println("! total # of statements: "+index);
	}

	private static String getStatementLabel(Statement s) {
		if (s instanceof NormalReturnCaller) {
			return "rcvd ret value";
		} else if (s instanceof NormalReturnCallee) {
			return "sent ret value";
		} else if (s instanceof ParamCallee) {
			return "param";
		} else if (s instanceof ParamCaller) {
			return "arg";
		} else if (s instanceof NormalStatement) {
			NormalStatement ss = (NormalStatement) s;
			SSAInstruction inst = ss.getInstruction();
			if (inst instanceof SSANewInstruction) {
				return "new";
			} else if (inst instanceof SSAArrayLoadInstruction) {
				return "arrayload";
			} else if (inst instanceof SSAArrayStoreInstruction) {
				return "arraystore";
			} else if (inst instanceof SSAGetInstruction) {
				return "get";
			} else if (inst instanceof SSAPutInstruction) {
				return "put";
			} else if (inst instanceof SSAReturnInstruction) {
				return "ret";
			} else if (inst instanceof SSAAbstractInvokeInstruction) {
				return "invoke";
			} else if (inst instanceof SSABinaryOpInstruction) {
				return "binop";
			} else if (inst instanceof SSACheckCastInstruction) {
				return "checkcast";
			}
			return inst.getClass().getSimpleName().toString();
		} else if (s instanceof PhiStatement) {
			return "phi";
		}
		return s.getClass().getSimpleName().toString();
	}

	// public StringBuffer generateDot(Collection<Node> nodes, Collection<Edge> edges) {
	// StringBuffer buf = new StringBuffer();
	//
	// // general stuff
	// buf.append("digraph callgraphslice {\n");
	// buf.append("rankdir=RL;\n");
	// buf.append("node
	// [shape=rect,height=0,width=0,fontsize=12,fillcolor=mediumaquamarine,style=filled]\n");
	//
	// // nodes (need URL property)
	//
	// String[] colors = {"greenyellow", "aliceblue", "salmon", "lightskyblue", "lightslateblue",
	// "peru"
	// };
	//
	// for ( Node n: nodes ) {
	// String sig = n.toString();
	// String label = sig;
	// Integer colorindex = null;
	// try {
	// int lastdot = sig.lastIndexOf('.');
	// String classname = sig.substring(0, lastdot);
	// String methodname = sig.substring(lastdot+1,sig.length());
	// lastdot = classname.lastIndexOf('.');
	// String classclassname = classname.substring(lastdot+1,classname.length());
	//
	//
	// label = methodname+" -- "+classclassname;
	// colorindex = classesToIndices.get(classname);
	// } catch (Exception e) {
	//				
	// }
	// String color="lightgrey";
	// if ( colorindex != null )
	// color = colors[colorindex.intValue() % colors.length];
	//
	// buf.append(String.format("\"%s\" [URL=\"%s\",fillcolor=\"%s\",label=\"%s\",color=\"%s\"]\n",
	// n,
	// n, color,label,
	// ((!n.drawPredNodes) && predEdges.get(n) != null) ? "black" : color));
	// }
	//
	// // edges
	// for ( Edge e: edges ) {
	// String s = String.format("\"%s\" -> \"%s\" [", e.from, e.to);
	// if ( e.duplicity > 1 )
	// s += String.format("label=\"x%2d\",", e.duplicity);
	//
	// if ( e.type == Edge.TYPE_THRU_PARAM )
	// s += "color=blue";
	// else if ( e.type == Edge.TYPE_THRU_RETVAL )
	// s += "color=red,arrowhead=inv";
	// else if( e.type == Edge.TYPE_THRU_ALIASING )
	// s += "style=dashed";
	//
	// s += "]\n";
	// buf.append(s);
	// }
	//
	// buf.append("}\n");
	// return buf;
	//
	// }

}
