/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin.sgview;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import java.util.ArrayList;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

public class ImageMap {

	private class NamedRect {
		int x1, y1, x2, y2;
		String name;

		public NamedRect(int x1, int y1, int x2, int y2, String name) {
			super();
			this.x1 = x1;
			this.y1 = y1;
			this.x2 = x2;
			this.y2 = y2;
			this.name = name;
		}
	}

	private static final String areaRegex = ".*<area shape=\"rect\".*href=\"(([^\"\\\\]|\\\\.)*)\".*coords=\"([0-9]*),([0-9]*),([0-9]*),([0-9]*)\"";

	ArrayList<NamedRect> namedRects = new ArrayList<NamedRect>();

	public ImageMap(String cmapxFile) throws IOException {
		// load it up

		Pattern p = Pattern.compile(areaRegex);

		BufferedReader f = new BufferedReader(new FileReader(cmapxFile));

		if (!f.ready())
			throw new IOException();

		String line;
		while ((line = f.readLine()) != null) {
			Matcher m = p.matcher(line);
			if (m.find()) {
				try {
					String name = m.group(1); // TODO: deslashify
					int x1 = Integer.parseInt(m.group(3));
					int y1 = Integer.parseInt(m.group(4));
					int x2 = Integer.parseInt(m.group(5));
					int y2 = Integer.parseInt(m.group(6));
					namedRects.add(new NamedRect(x1, y1, x2, y2, name));
				} catch (NumberFormatException nfe) {
					// ignore incorrect line
				}

			}
		}

	}

	public String getLinkForCoord(int x, int y) {
		for (NamedRect nr : namedRects) {
			if (x >= nr.x1 && x <= nr.x2 && y >= nr.y1 && y <= nr.y2)
				return nr.name;
		}
		return null;
	}

}
