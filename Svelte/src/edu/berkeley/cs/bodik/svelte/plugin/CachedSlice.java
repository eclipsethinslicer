package edu.berkeley.cs.bodik.svelte.plugin;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;

import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.util.collections.EmptyIterator;
import com.ibm.wala.util.graph.Graph;

/**
 * An in-memory graph representing the slice. We can do operations (like chopping) to modify the
 * slice.
 * 
 * @author evan
 * 
 */
public class CachedSlice implements Graph<Statement> {
	SliceEnvironment env;
	Graph<Statement> origDepGraph;
	Collection<Statement> seeds;

	Hashtable<Statement, ArrayList<Statement>> predEdges;
	Hashtable<Statement, ArrayList<Statement>> succEdges;

	public CachedSlice(SliceEnvironment env) {
		this.env = env;
		this.origDepGraph = SliceEnvironment.getDepGraph();
		this.seeds = new ArrayList<Statement>();
		this.seeds.addAll(SliceEnvironment.getSeed().getStatements());

		generatePredAndSuccNodes();

	}

	private void generatePredAndSuccNodes() {
		// NEVER fix method seed statements here. They should have already been fixed in the seed.
		ArrayList<Statement> worklist = new ArrayList<Statement>();
		HashSet<Statement> serviced = new HashSet<Statement>();

		predEdges = new Hashtable<Statement, ArrayList<Statement>>();
		succEdges = new Hashtable<Statement, ArrayList<Statement>>();

		// BFS time, baby
		worklist.addAll(seeds);

		// do a BFS and add all statements
		int index = -1;

		while ((index + 1) < worklist.size()) {
			index++;
			// if ( depth != -1 && index == nextlevelindex ) {
			// nextlevelindex = worklist.size();
			// level ++;
			// if ( level > depth )
			// break;
			// }

			Statement s = worklist.get(index);

			// service it
			if (serviced.contains(s))
				continue;
			serviced.add(s);

			// look thru the succ nodes, if theres any 'return val' ones or something, build the
			// bridge.
			Iterator<? extends Statement> succNodes = origDepGraph.getSuccNodes(s);

			Statement ss;
			while (succNodes.hasNext()) {
				ss = succNodes.next();
				// add succ nodes to worklist if not already serviced.
				if (!serviced.contains(ss))
					worklist.add(ss);

				addEdge(succEdges, s, ss);
				addEdge(predEdges, ss, s);
			}
		}
	}

	private void addEdge(Hashtable<Statement, ArrayList<Statement>> edges, Statement from,
			Statement to) {
		ArrayList<Statement> nodes = edges.get(from);
		if (nodes == null) {
			nodes = new ArrayList<Statement>();
			edges.put(from, nodes);
		}
		nodes.add(to);

	}

	public Collection<Statement> getSeeds() {
		return seeds;
	}

	public void chop(Collection<Statement> leaves) {
		Hashtable<Statement, ArrayList<Statement>> newPredEdges = new Hashtable<Statement, ArrayList<Statement>>();
		Hashtable<Statement, ArrayList<Statement>> newSuccEdges = new Hashtable<Statement, ArrayList<Statement>>();
		seeds.clear();
		ArrayList<Statement> worklist = new ArrayList<Statement>();
		HashSet<Statement> serviced = new HashSet<Statement>();
		worklist.addAll(leaves);

		int index = -1;
		while ((index + 1) < worklist.size()) {
			index++;
			Statement s = worklist.get(index);

			// service it
			if (serviced.contains(s))
				continue;
			serviced.add(s);

			// look thru the succ nodes, if theres any 'return val' ones or something, build the
			// bridge.
			ArrayList<Statement> parents = predEdges.get(s);
			if (parents == null)
				seeds.add(s); // has no parents, must be a seed.
			else {
				for (Statement ss : parents) {
					// add parent nodes to worklist if not already serviced.
					if (!serviced.contains(ss))
						worklist.add(ss);

					addEdge(newSuccEdges, ss, s); // succ is from parent to child
					addEdge(newPredEdges, s, ss); // pred is from child to parent
				}
			}
		}

		System.out.printf("CHOP pred %d/succ %d -> pred %d/succ %d", predEdges.size(), succEdges
				.size(), newPredEdges.size(), newSuccEdges.size());
		predEdges = newPredEdges;
		succEdges = newSuccEdges;
	}

	public Iterator<? extends Statement> getSuccNodes(Statement N) {
		ArrayList<Statement> succNodes = succEdges.get(N);
		if (succNodes == null)
			return EmptyIterator.instance();
		return succNodes.iterator();
	}

	public void removeNodeAndEdges(Statement N) throws UnsupportedOperationException {
		// TODO Auto-generated method stub

	}

	public void addNode(Statement n) {
		// TODO Auto-generated method stub

	}

	public boolean containsNode(Statement N) {
		// TODO Auto-generated method stub
		return false;
	}

	public int getNumberOfNodes() {
		// TODO Auto-generated method stub
		return 0;
	}

	public Iterator<Statement> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

	public void removeNode(Statement n) {
		// TODO Auto-generated method stub

	}

	public void addEdge(Statement src, Statement dst) {
		// TODO Auto-generated method stub

	}

	public int getPredNodeCount(Statement N) {
		// TODO Auto-generated method stub
		return 0;
	}

	public Iterator<? extends Statement> getPredNodes(Statement N) {
		// TODO Auto-generated method stub
		return null;
	}

	public int getSuccNodeCount(Statement N) {
		// TODO Auto-generated method stub
		return 0;
	}

	public boolean hasEdge(Statement src, Statement dst) {
		// TODO Auto-generated method stub
		return false;
	}

	public void removeAllIncidentEdges(Statement node) throws UnsupportedOperationException {
		// TODO Auto-generated method stub

	}

	public void removeEdge(Statement src, Statement dst) throws UnsupportedOperationException {
		// TODO Auto-generated method stub

	}

	public void removeIncomingEdges(Statement node) throws UnsupportedOperationException {
		// TODO Auto-generated method stub

	}

	public void removeOutgoingEdges(Statement node) throws UnsupportedOperationException {
		// TODO Auto-generated method stub

	}
}
