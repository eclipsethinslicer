/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin.bfsliceview;

import java.util.ArrayList;

import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IToolBarManager;
import org.eclipse.jface.viewers.DoubleClickEvent;
import org.eclipse.jface.viewers.IDoubleClickListener;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.TreeColumn;
import org.eclipse.ui.part.ViewPart;

import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;

import edu.berkeley.cs.bodik.svelte.plugin.EclipseUtils;
import edu.berkeley.cs.bodik.svelte.plugin.SliceEnvironment;
import edu.berkeley.cs.bodik.svelte.plugin.SveltePlugin;

public class BFSliceView extends ViewPart {
	TreeViewer viewer;
	BFSliceInput input;
	BFSliceContentProvider bfscp;

	public BFSliceView() {
	}

	@Override
	public void createPartControl(Composite parent) {
		viewer = new TreeViewer(parent);
		input = new BFSliceInput(SliceEnvironment.getDepGraph(), SliceEnvironment.getSeed());
		// need mutex here?!
		bfscp = new BFSliceContentProvider();
		viewer.setContentProvider(bfscp);
		viewer.setLabelProvider(new BFSliceLabelProvider(bfscp));
		viewer.setInput(input);

		this.viewer.setExpandedState(bfscp.getElements(input)[0], true);

		TreeColumn column = new TreeColumn(viewer.getTree(), SWT.NONE);
		column.setWidth(400);
		column.setText("Statement");
		column = new TreeColumn(viewer.getTree(), SWT.NONE);
		column.setWidth(400);
		column.setText("Function");
		column = new TreeColumn(viewer.getTree(), SWT.NONE);
		column.setWidth(400);
		column.setText("SSA Instruction");
		viewer.getTree().setLinesVisible(true);
		viewer.getTree().setHeaderVisible(true);
		viewer.getTree().setLinesVisible(true);
		viewer.getTree().setHeaderVisible(true);

		viewer.refresh();
		viewer.setExpandedState(input.getRoot(), true);
		ArrayList<BFSliceStatement> seeds = input.getRoot().getChildren();
		if (seeds.size() > 0) {
			viewer.setSelection(new StructuredSelection(seeds.get(0)));
			EclipseUtils.gotoStatement(seeds.get(0).getStatement());
		}

		createActions();

		setUpListeners();
	}

	private void setUpListeners() {
		viewer.addDoubleClickListener(new IDoubleClickListener() {

			public void doubleClick(DoubleClickEvent event) {
				IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
				EclipseUtils.gotoStatement(((BFSliceStatement) selection.getFirstElement())
						.getStatement());

				// /********************** CHOP *********************/
				// // it slices dices, minces, rinses, shreds, cuts, chops, beats, blends, frappes,
				// liquifies, ...
				// SliceEnvironment.getCurrent().revertToOriginalDepGraph();
				// CachedSlice cs = new CachedSlice(SliceEnvironment.getCurrent());
				// cs.chop(Collections.singleton(((BFSliceStatement)selection.getFirstElement()).getStatement()));
				// SliceEnvironment.getCurrent().setDepGraph(cs);
				// /********************** CHOP *********************/

				Statement s = ((BFSliceStatement) selection.getFirstElement()).getStatement();
				String methodSignature = s.getNode().getMethod().getSignature();
				methodSignature = methodSignature.replace("/", ".");
				
				if ( s instanceof NormalStatement ) {
					System.out.println("instrument:");
					System.out.println(methodSignature);
					System.out.println(((NormalStatement)s).getInstructionIndex());
				}

//				/** ************** INSTRUMENTATION *************** */
//
//				
//				InstrumentationSet is = new InstrumentationSet();
//				is.addInstrumentationPoint(((BFSliceStatement) selection.getFirstElement())
//						.getStatement());
//				for (Statement s : SliceEnvironment.getSlice())
//					is.addInstrumentationPoint(s);
//
//				ArrayList<String> filenames = new ArrayList<String>();
//				 for (String classname : is.getFullyQualifiedClassNames()) {
//				 String classfilename = EclipseJdtUtils.findClassFileForClassStripL(classname,
//						 EclipseJdtUtils.getJavaProjectFromProject(SliceEnvironment.env().getProject()));
//				 if (classfilename != null)
//				 filenames.add(classfilename);
//				 }
//				try {
//					Instrumenter.instrument(filenames, "/tmp/SveltedInstrumented.jar", is);
//				} catch (Exception e) {
//					e.printStackTrace();
//				}
//				/** ************** INSTRUMENTATION *************** */

//				Statement s = ((BFSliceStatement) selection.getFirstElement()).getStatement();
//				System.out.println("****IR****\n****IR****\n" + s.getNode().getIR());

			}
		});
	}

	@Override
	public void setFocus() {
		viewer.getControl().setFocus();
	}

	public void createActions() {

		Action followDownAction = new Action("Follow down (Statement bad)") {
			@Override
			public void run() {
				followDown();
			}
		};
		followDownAction.setImageDescriptor(SveltePlugin
				.getImageDescriptor("icons/bfsv/downxred.png"));

		Action nextCheckAction = new Action("Next at this level (Statement good)") {
			@Override
			public void run() {
				nextCheck();
			}
		};
		nextCheckAction.setImageDescriptor(SveltePlugin
				.getImageDescriptor("icons/bfsv/nextcheck.png"));

		Action nextQuestionAction = new Action("Next at this level (Statement maybe bad)") {
			@Override
			public void run() {
				nextQuestion();
			}
		};
		nextQuestionAction.setImageDescriptor(SveltePlugin
				.getImageDescriptor("icons/bfsv/nextquestionmark.png"));

		Action backUpAction = new Action("Back up") {
			@Override
			public void run() {
				backUp();
			}
		};
		backUpAction.setImageDescriptor(SveltePlugin.getImageDescriptor("icons/bfsv/up.png"));

		IToolBarManager mgr = getViewSite().getActionBars().getToolBarManager();
		mgr.add(followDownAction);
		mgr.add(nextCheckAction);
		mgr.add(nextQuestionAction);
		mgr.add(backUpAction);
	}

	protected void backUp() {
		bfscp.backUp();
	}

	protected void nextQuestion() {
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		bfscp.nextQuestion(selection.getFirstElement());
	}

	protected void nextCheck() {
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		bfscp.nextCheck(selection.getFirstElement());
	}

	protected void followDown() {
		IStructuredSelection selection = (IStructuredSelection) viewer.getSelection();
		bfscp.followDown(selection.getFirstElement());

	}
}
