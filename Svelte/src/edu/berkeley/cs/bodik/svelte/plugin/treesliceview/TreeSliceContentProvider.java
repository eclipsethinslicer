/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin.treesliceview;

import java.util.ArrayList;
import java.util.Iterator;

import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.Viewer;

import com.ibm.wala.ipa.slicer.Statement;

public class TreeSliceContentProvider implements ITreeContentProvider // , WordFile.Listener
{
	TreeSliceInput input;

	/**
	 * @see IStructuredContentProvider#getElements(Object)
	 */
	public Object[] getElements(Object element) {
		return input.getRoots().toArray();
	}

	/**
	 * @see IContentProvider#dispose()
	 */
	public void dispose() {
		input = null;
	}

	/**
	 * @see IContentProvider#inputChanged(Viewer, Object, Object)
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if (newInput instanceof TreeSliceInput) {
			// TODO -- multiple seeds! duh d'oh!
			input = (TreeSliceInput) newInput;
		}
	}

	public Object[] getChildren(Object parentElement) {
		ArrayList<Statement> children = new ArrayList<Statement>();
		Iterator<? extends Statement> iter = input.getChildren((Statement) parentElement);
		while (iter.hasNext()) {
			children.add(iter.next());
		}
		return children.toArray();
	}

	public Object getParent(Object element) {
		return null;// activeNode(); // in children
	}

	public boolean hasChildren(Object element) {
		return input.getChildren((Statement) element).hasNext();
	}

	// /**
	// * @see Listener#added()
	// */
	// public void added(Word e) {
	// if (viewer != null)
	// viewer.add(e);
	// }
	//	
	// /**
	// * @see Listener#removed()
	// */
	// public void removed(Word e) {
	// if (viewer != null) {
	// viewer.setSelection(null);
	// viewer.remove(e);
	// }
	// }
}
