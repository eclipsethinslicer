/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin.cgview;

import java.io.File;
import java.io.IOException;

import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

import edu.berkeley.cs.bodik.svelte.plugin.DotUtils;
import edu.berkeley.cs.bodik.svelte.plugin.EclipseJdtUtils;
import edu.berkeley.cs.bodik.svelte.plugin.SliceEnvironment;
import edu.berkeley.cs.bodik.svelte.plugin.cgview.CallGraphSliceView.IClickHandler;

public class CGViewActionDelegate implements IEditorActionDelegate {
	private CallGraphSlice cgs;
	private IJavaProject proj;

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		// TODO Auto-generated method stub
		IJavaElement compunit = JavaUI.getWorkingCopyManager().getWorkingCopy(
				targetEditor.getEditorInput());
		proj = compunit.getJavaProject();

	}

	public void run(IAction action) {
		// make the slice
		cgs = new CallGraphSlice(SliceEnvironment.getSeed(), SliceEnvironment
				.getDepth());
		reload();
	}

	public void reload() {
		assert (cgs != null);
		StringBuffer buf = cgs.generateDot();

		// TODO FIXME: how / when to delete?
		File tmpfile_dot = null, tmpfile_png = null, tmpfile_cmapx = null;
		try {
			tmpfile_dot = File.createTempFile("tmp_eclipsethinslicer", ".dot");
			tmpfile_png = File.createTempFile("tmp_eclipsethinslicer", ".png");
			tmpfile_cmapx = File.createTempFile("tmp_eclipsethinslicer",
					".cmapx");

			// TODO FIXME: delete it before exit! these PNGS can be big.
			tmpfile_dot.deleteOnExit();
			tmpfile_png.deleteOnExit();
			tmpfile_cmapx.deleteOnExit();

		} catch (IOException e1) {
			e1.printStackTrace();
			return; // TODO FIXME: should really show an error message!
		}

		try {
			DotUtils.writeDotFile(buf.toString(), tmpfile_dot);
			DotUtils.spawnDot(tmpfile_png, tmpfile_cmapx, tmpfile_dot);
		} catch (IOException e1) {
			e1.printStackTrace();
			return;
		}

		// find the cgview and load the png
		// ResourcesPlugin.getWorkspace()
		try {
			CallGraphSliceView cgviewpart = (CallGraphSliceView) PlatformUI
					.getWorkbench()
					.getActiveWorkbenchWindow()
					.getActivePage()
					.showView(
							"edu.berkeley.cs.bodik.svelte.plugin.CallGraphSliceView");
			cgviewpart.setImage(tmpfile_png.getAbsolutePath());
			cgviewpart.setImageMap(tmpfile_cmapx.getAbsolutePath());

			// set up handling of clicks to do various things

			cgviewpart.setClickHandler(new IClickHandler() {

				public void nodeClicked(String node) {
					EclipseJdtUtils.gotoNode(node, proj);
				}

				public void nodeLeftClicked(String node) {
					cgs.toggleCollapsed(node);
					reload();
				}

			});

		} catch (PartInitException e) {
			e.printStackTrace();
		}

	}

	public void selectionChanged(IAction action, ISelection selection) {
		// TODO Auto-generated method stub
	}

}
