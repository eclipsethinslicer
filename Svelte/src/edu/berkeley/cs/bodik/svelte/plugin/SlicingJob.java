/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;

import com.ibm.wala.ipa.slicer.Statement;

import edu.berkeley.cs.bodik.svelte.Slice;
import edu.berkeley.cs.bodik.svelte.SourcePosition;
import edu.berkeley.cs.bodik.svelte.Slicing.SliceType;
import edu.berkeley.cs.bodik.svelte.slicers.AbstractSliceEnvironment;

public class SlicingJob extends Job {
	private IFile file;
	private IEditorPart editor;
	private SliceType sliceType;
	private ITextSelection selection;
	private int depth;

	public SlicingJob(SliceType sliceType, int depth, IEditorPart editor, ITextSelection selection) {
		super("Slicing");
		this.sliceType = sliceType;
		this.editor = editor;
		this.file = ((IFileEditorInput) editor.getEditorInput()).getFile();
		this.selection = selection;
		this.depth = depth;
		setUser(true);
	}

	/**
	 * Draws the markers in the editor corresponding to the given slice
	 * 
	 * @author DarkWulf
	 */
	public class MarkerDrawerRunnable implements IWorkspaceRunnable {
		private Slice slice;
		public String markerType = "edu.berkeley.cs.bodik.svelte.plugin.sliceMarker";

		public MarkerDrawerRunnable(Slice slice) {
			this.slice = slice;
		}

		public void run(IProgressMonitor monitor) {
			IProject proj = file.getProject();

			// clear old markers
			try {
				proj.deleteMarkers(markerType, true, IResource.DEPTH_INFINITE);
			} catch (CoreException e) {
				e.printStackTrace();
			}

			Set<String> markersAdded = new HashSet<String>(slice.getStatements().size());

			// TODO: lots of resource change signals could cause slow down. temporarily remove
			// listener? or some other way?
			for (Statement ss : slice) {

				SourcePosition s = slice.getEnvironment().statementToPosition(ss);

				// skip if have a null source position
				if (s == null)
					continue;

				// watch for duplicate lines; only for Shrike (no column info)
				// may want to do for CAst, too -- use minimum & maximum column #s on that line.
				if (s.offsetStart <= 0) {
					String tohash = s.lineStart + " " + s.filename;
					if (markersAdded.contains(tohash))
						continue;
					markersAdded.add(tohash);
				}

				IFile f = EclipseUtils.findIFileForFilename(s.filename);

				try {
					if (f != null) {
						// draw the marker
						IMarker marker = f.createMarker(markerType);
						marker.setAttribute(IMarker.SEVERITY, 0);
						marker.setAttribute(IMarker.PRIORITY, IMarker.PRIORITY_HIGH);

						// set position
						marker.setAttribute(IMarker.LINE_NUMBER, s.lineStart);
						if (s.offsetStart > 0) {
							marker.setAttribute(IMarker.CHAR_START, s.offsetStart);
							marker.setAttribute(IMarker.CHAR_END, s.offsetEnd);
						}

						String descr = ss.toString().replaceFirst("[^:]*:", "");
						if ((selection.getStartLine() + 1) == s.lineStart) {
							marker.setAttribute(IMarker.MESSAGE, "Seed: " + descr);
						} else {
							marker.setAttribute(IMarker.MESSAGE, "Slice: " + descr);
						}
					} else {
						System.err.println("couldn't find filename " + s.filename);
					}
				} catch (JavaModelException e) {
					e.printStackTrace();
				} catch (CoreException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	protected IStatus run(IProgressMonitor monitor) {
		try {
			return realrun(monitor);
		} catch (RuntimeException e) {
			System.err.println("CAUGHT RUNTIMEEXCEPTION IN SLICINGJOB:\n");
			e.printStackTrace();
			throw(e);
		}
	}
	
	protected IStatus realrun(IProgressMonitor monitor) {
		// find the slice
		AbstractSliceEnvironment env = SliceEnvironmentCache.getInstance().getSliceEnvironment(this.file);
		if (env == null) {
			return new Status(IStatus.WARNING, SveltePlugin.PLUGIN_ID, 0, "Unsupported filetype!",
					null);
		}

		SourcePosition position = SourcePosition.makeSourcePosition(editor, selection);
		Collection<Statement> seed = env.generateSeed(position,editor);
		if ( seed == null || seed.size() == 0 || (seed.size() == 1 && seed.toArray()[0] == null) ) {
			return new Status(IStatus.WARNING, SveltePlugin.PLUGIN_ID, 0, "No seed", null);
		}
		Slice slice = env.getSlice(sliceType, depth, seed);
		
		if (slice == null) {
			return new Status(IStatus.WARNING, SveltePlugin.PLUGIN_ID, 0, "NULL Slice/empty slice",
					null);
		}

		try {
			// draw markers for the slice
			ResourcesPlugin.getWorkspace().run(new MarkerDrawerRunnable(slice), null);
		} catch (CoreException e1) {
			e1.printStackTrace();
		}

		return new Status(IStatus.OK, SveltePlugin.PLUGIN_ID, 0, "Finished", null);
	}

}
