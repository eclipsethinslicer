/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin;

import org.eclipse.core.resources.IMarker;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IWorkspaceRunnable;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.action.IAction;
import edu.berkeley.cs.bodik.svelte.Slicing;

public class SubsliceEditorActionDelegate extends SlicingActionDelegate {

	public void run(IAction action) {
		final IJavaProject proj = cu.getJavaProject();
		// first: change all subslices to regular slices
		try {
			ResourcesPlugin.getWorkspace().run(new IWorkspaceRunnable() {
				public void run(IProgressMonitor monitor) {
					IMarker[] oldmarkers;
					try {
						oldmarkers = proj.getResource().findMarkers(
								"edu.berkeley.cs.bodik.svelte.plugin.subSliceMarker", true,
								IResource.DEPTH_INFINITE);
						for (IMarker marker : oldmarkers) {
							IMarker newmarker = marker.getResource().createMarker(
									"edu.berkeley.cs.bodik.svelte.plugin.sliceMarker");
							newmarker.setAttribute(IMarker.CHAR_START, marker
									.getAttribute(IMarker.CHAR_START));
							newmarker.setAttribute(IMarker.CHAR_END, marker
									.getAttribute(IMarker.CHAR_END));
							newmarker.setAttribute(IMarker.LINE_NUMBER, marker
									.getAttribute(IMarker.LINE_NUMBER));
							newmarker.setAttribute(IMarker.PRIORITY, marker
									.getAttribute(IMarker.PRIORITY));
							newmarker.setAttribute(IMarker.MESSAGE, marker
									.getAttribute(IMarker.MESSAGE));
							newmarker.setAttribute(IMarker.SEVERITY, marker
									.getAttribute(IMarker.SEVERITY));
							marker.delete();
						}
					} catch (CoreException e) {
						e.printStackTrace();
					}
				}
			}, null);
		} catch (CoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		run(action, Slicing.SliceType.CI_THIN_SLICE,
				"edu.berkeley.cs.bodik.svelte.plugin.subSliceMarker", 2);
	}
}
