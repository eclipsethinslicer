/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin.sgview;

import java.io.IOException;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.part.ViewPart;

public class SliceGraphView extends ViewPart {
	Label label;
	ScrolledComposite sc1;
	IClickHandler clickHandler = null;

	private ImageMap imageMap = null;
	private Composite c1;

	public SliceGraphView() {
	}

	public interface IClickHandler {
		public void nodeClicked(String node);

		public void nodeLeftClicked(String node); // or middle, etc.
	}

	public void setImage(String filename) {
		if ( label.getImage() != null )
			label.getImage().dispose();
		Image image = new Image(Display.getCurrent(), filename);
		label.setImage(image);
		c1.setSize(c1.computeSize(SWT.DEFAULT, SWT.DEFAULT));
	}

	public void setImageMap(String cmapxFilename) {
		try {
			imageMap = new ImageMap(cmapxFilename);
		} catch (IOException e) {
			imageMap = null;
			System.err.println("Couldn't load image map!");
			// give an error, or something
		}
	}

	public void setClickHandler(IClickHandler ich) {
		this.clickHandler = ich;
	}

	@Override
	public void createPartControl(Composite parent) {
		// set the size of the scrolled content - method 1
		sc1 = new ScrolledComposite(parent, SWT.H_SCROLL | SWT.V_SCROLL | SWT.BORDER);
		final Composite c1 = new Composite(sc1, SWT.NONE);

		this.c1 = c1;

		sc1.setContent(c1);
		// GridLayout layout = new GridLayout();
		// layout.numColumns = 1;
		RowLayout layout = new RowLayout();
		layout.marginBottom = layout.marginTop = layout.marginLeft = layout.marginRight = 0;
		layout.spacing = 0;
		c1.setLayout(layout);
		label = new Label(c1, SWT.WRAP);
		label.setImage(null);
		c1.setSize(c1.computeSize(SWT.DEFAULT, SWT.DEFAULT));

		// l.addMouseWheelListener(listener)
		label.addMouseListener(new MouseListener() {

			public void mouseDoubleClick(MouseEvent e) {
				// TODO Auto-generated method stub

			}

			public void mouseDown(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("mouse down @(" + e.x + "," + e.y + ") ");
				String s;
				if (imageMap != null && (s = imageMap.getLinkForCoord(e.x, e.y)) != null) {
					s = s.replace("&gt;", ">");
					s = s.replace("&lt;", "<");
					System.out.println("link clicked " + s);
					if (clickHandler != null) {
						if (e.button == 1)
							clickHandler.nodeClicked(s);
						else
							clickHandler.nodeLeftClicked(s);
					}

				} else
					System.out.println("no link clicked");
				System.out.println(imageMap.namedRects);
				System.out.println("button " + e.button);
			}

			public void mouseUp(MouseEvent e) {
				// TODO Auto-generated method stub
			}

		});

//		image.dispose();
	}

	@Override
	public void setFocus() {
		// set focus to my widget. For a label, this doesn't
		// make much sense, but for more complex sets of widgets
		// you would decide which one gets the focus.

	}
}
