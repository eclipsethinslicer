package edu.berkeley.cs.bodik.svelte.plugin;

import org.eclipse.jface.dialogs.PopupDialog;
import org.eclipse.swt.widgets.Shell;

public class StatementPopupDialog extends PopupDialog {

	public StatementPopupDialog(Shell parent, int shellStyle, boolean takeFocusOnOpen,
			boolean persistBounds, boolean showDialogMenu, boolean showPersistAction,
			String titleText, String infoText) {
		super(parent, shellStyle, takeFocusOnOpen, persistBounds, showDialogMenu,
				showPersistAction, titleText, infoText);
	}

}
