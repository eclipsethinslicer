package edu.berkeley.cs.bodik.svelte.plugin;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IProjectNature;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaElement;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IPackageFragment;
import org.eclipse.jdt.core.IPackageFragmentRoot;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.internal.core.JarPackageFragmentRoot;
import org.eclipse.jdt.internal.core.JavaModel;
import org.eclipse.jdt.internal.corext.dom.NodeFinder;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.AbstractTextEditor;

/**
 * Miscellaneous helper methods for working with Jdt based files. (Extracted from EclipseUtils,
 * since they require more than just the Eclipse platform)
 * 
 * @author DarkWulf
 */
public class EclipseJdtUtils extends EclipseUtils {

	/**
	 * Same as findSourceFileForClass, but arguments must begin with "L", which this function
	 * removes before passing on to findSourceFileForClass.
	 * 
	 * @param classnamePrefixedByL
	 * @param proj
	 * @return
	 * @see findSourceFileForClass
	 */
	public static String findSourceFileForClassStripL(String classnamePrefixedByL, IJavaProject proj) {
		assert (classnamePrefixedByL.startsWith("L"));
		return EclipseJdtUtils.findSourceFileForClass(classnamePrefixedByL.substring(1), proj);
	}

	/**
	 * Hack to find the source filename for a given class, assuming it's one of the source files in
	 * the given project. Looks thru all source directories. Example. for class name
	 * com/example/pack/MyClass$InnerClass, looks for file com/example/pack/MyClass.java in all the
	 * project's source directories.
	 * 
	 * @param classname
	 *            Fully qualified classname, e.g. com/example/pack/MyClass$InnerClass. CANNOT HAVE L
	 *            AT BEGINNING (use findSourceFileForClassStripL). Separated by either '.' or '/'.
	 * 
	 * @param proj
	 * 
	 * @return The absolute filename which contains the class, or null.
	 */
	public static String findSourceFileForClass(String classname, IJavaProject proj) {
		if (classname == null || proj == null)
			return null;

		// Strip characters after "$" that is after the last "/" (inner
		// classes), and add ".java"
		classname = classname.replaceAll("\\.", "/"); // java.lang.String ->
		// java/lang/String
		Path javafilenamepath = new Path(classname.replaceAll("\\$[^/]*$", "") + ".java");
		IClasspathEntry[] classpaths = null;
		try {
			classpaths = proj.getResolvedClasspath(true);
			for (IClasspathEntry cpe : classpaths) {
				if (cpe.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
					// how do we know if the project path is absolute or not???
					IPath srcpath = getAbsolutePath(cpe).append(javafilenamepath);
					String srcstr = srcpath.toOSString();
					if (new File(srcpath.toOSString()).exists())
						return srcstr;

				} else if (cpe.getEntryKind() == IClasspathEntry.CPE_PROJECT && cpe.isExported()) {
					IJavaProject reqdproj = JavaCore.create((IProject) ResourcesPlugin
							.getWorkspace().getRoot().findMember(cpe.getPath()));
					String filename = EclipseJdtUtils.findSourceFileForClassStripL(classname,
							reqdproj);
					if (filename != null)
						return filename;
				}
			}
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static String findClassFileForClass(String classname, IJavaProject proj) {
		if (classname == null || proj == null)
			return null;

		// Strip characters after "$" that is after the last "/" (inner
		// classes), and add ".java"
		classname = classname.replaceAll("\\.", "/"); // java.lang.String ->
		// java/lang/String
		Path classfilenamepath = new Path(classname + ".class");
		return classPathToFilename(classfilenamepath, proj);
	}

	public static String classPathToFilename(Path path, IJavaProject proj) {
		IClasspathEntry[] classpaths = null;

		// see if it's directly in this project's output location.
		String inthisbindir;
		try {
			inthisbindir = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(
					proj.getOutputLocation()).append(path).toOSString();
			if (new File(inthisbindir).exists())
				return inthisbindir;
		} catch (JavaModelException e1) {
			e1.printStackTrace();
		}

		// otherwise, try it's libraries and projects (recursively)
		try {
			classpaths = proj.getResolvedClasspath(true);
			for (IClasspathEntry cpe : classpaths) {
				if (cpe.getEntryKind() == IClasspathEntry.CPE_PROJECT) {
					IProject project = (IProject) ResourcesPlugin.getWorkspace().getRoot()
							.findMember(cpe.getPath());
					String result = classPathToFilename(path, JavaCore.create(project));
					if (result != null)
						return result;
				} else if (cpe.getEntryKind() == IClasspathEntry.CPE_LIBRARY) {
					// look for it physically
					IPath srcpath = ResourcesPlugin.getWorkspace().getRoot().getLocation().append(
							cpe.getPath()).append(path);
					String srcstr = srcpath.toOSString();
					if (new File(srcpath.toOSString()).exists())
						return srcstr;
				}
			}
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		return null;
	}

	public static void gotoNode(String nodeString, IJavaProject proj) {
		if (nodeString == null || proj == null)
			return;

		EclipseUtils.MethodSignature met = new EclipseUtils.MethodSignature(nodeString);

		// TODO: associate cgslice with project beforehand
		// find the method in source
		try {
			IFile f = EclipseUtils.findIFileForFilename(EclipseJdtUtils.findSourceFileForClass(met
					.getQualifiedClassName(), proj));

			EclipseUtils.openFile(f);

			IEditorPart iep = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
					.getActiveEditor();
			AbstractTextEditor e = (AbstractTextEditor) iep;

			ICompilationUnit cu = JavaCore.createCompilationUnitFrom(f);
			IType type = cu.getType(met.getClassName());
			IMethod m = type.getMethod(met.getMethodName(), met.getUnqualifiedParameterTypes());
			e.selectAndReveal(m.getNameRange().getOffset(), m.getNameRange().getLength());
		} catch (NullPointerException e) {
			System.err.println("couldn't find src method. problem:");
			e.printStackTrace(System.err);
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
	}

	public static boolean fileHasMain(IFile f) {
		ICompilationUnit cu = JavaCore.createCompilationUnitFrom(f);
		IType t = cu.findPrimaryType();
		
		try {
			for (IMethod met: t.getMethods())
				if ( met.isMainMethod() )
					return true;
		} catch (JavaModelException e) {
		}
		return false;
	}
	
	@SuppressWarnings( { "deprecation", "restriction", "unchecked" })
	public static void gotoNodeParam(String nodeString, int arg_index, IJavaProject proj) {
		if (nodeString == null || proj == null)
			return;

		EclipseUtils.MethodSignature met = new EclipseUtils.MethodSignature(nodeString);

		// TODO: associate cgslice with project beforehand
		// find the method in source
		try {
			IFile f = EclipseUtils.findIFileForFilename(EclipseJdtUtils.findSourceFileForClass(met
					.getQualifiedClassName(), proj));

			EclipseUtils.openFile(f);

			IEditorPart iep = PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage()
					.getActiveEditor();
			AbstractTextEditor e = (AbstractTextEditor) iep;

			ICompilationUnit cu = JavaCore.createCompilationUnitFrom(f);
			
			String classname = met.getClassName();
			IType type;
			if ( classname.contains("$") ) { // the joy of inner classes
				String[] classes = classname.split("\\$");
				type = cu.getType(classes[0]);
				for ( int i=1; i < classes.length; i++ )
					type = type.getType(classes[i]);
			} else {
				type = cu.getType(met.getClassName());
			}	
			
			
			IMethod m = type.getMethod(met.getMethodName(), met.getUnqualifiedParameterTypes());
			// e.selectAndReveal(m.getNameRange().getOffset(),m.getNameRange().getLength());

			// TODO: hack (should do this right)
			ASTNode aSTNode = AST.parseCompilationUnit(cu, false);
			NodeFinder nf = new NodeFinder(m.getNameRange().getOffset(), m.getNameRange()
					.getLength());
			aSTNode.accept(nf);
			ASTNode node = nf.getCoveringNode();
			if (node.getParent() instanceof MethodDeclaration) {
				MethodDeclaration md = (MethodDeclaration) node.getParent();
				List params = md.parameters();
				if (arg_index < params.size()) {
					Object param = params.get(arg_index);
					if (param instanceof SingleVariableDeclaration) {
						SingleVariableDeclaration svd = (SingleVariableDeclaration) param;
						e.selectAndReveal(svd.getStartPosition(), svd.getLength());
					}
				}
			}
		} catch (NullPointerException e) {
			System.err.println("couldn't find src method. problem:");
			e.printStackTrace(System.err);
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
	}

	
	public static IJavaProject getJavaProjectFromProject(IProject proj) {
		IProjectNature projectnature;
		try {
			projectnature = proj.getNature(JavaCore.NATURE_ID);
			if (projectnature instanceof IJavaProject)
				return (IJavaProject) projectnature;
		} catch (CoreException e) {
			return null;
		}
		return null;
	}
	
	public static IJavaProject getJavaProjectFromOpenEditor() {
		IEditorInput activeEdInput = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
				.getActivePage().getActiveEditor().getEditorInput();
		if (activeEdInput instanceof IFileEditorInput) {
			IProject proj = ((IFileEditorInput) activeEdInput).getFile().getProject();
			IProjectNature projectnature;
			try {
				projectnature = proj.getNature(JavaCore.NATURE_ID);
				if (projectnature instanceof IJavaProject)
					return (IJavaProject) projectnature;
			} catch (CoreException e) {
				return null;
			}
		}
		return null;
	}

	public static String findClassFileForClassStripL(String classnamePrefixedByL,
			IJavaProject javaProject) {
		assert (classnamePrefixedByL.startsWith("L"));
		return findClassFileForClass(classnamePrefixedByL.substring(1), javaProject);
	}

	/**
	 * 
	 * @param cu
	 * @param root
	 * @param selected
	 * @return
	 */
	public static String getQualifiedClassName(ICompilationUnit cu, ASTNode root, ASTNode selected) {
		ASTNode iter = selected;

		String classname = null;

		while (iter != null) {
			if (iter.getNodeType() == ASTNode.TYPE_DECLARATION) {
				TypeDeclaration td = (TypeDeclaration) iter;
				if (td.isInterface()) {
					return null; // error: in an interface
				} else {
					if (classname == null)
						classname = td.getName().getFullyQualifiedName();
					else
						classname = td.getName().getFullyQualifiedName() + "$" + classname;
				}
			}
			iter = iter.getParent();
		}
		if (classname == null)
			return null;
		if (root instanceof org.eclipse.jdt.core.dom.CompilationUnit) {
			PackageDeclaration packkage = ((org.eclipse.jdt.core.dom.CompilationUnit) root)
					.getPackage();
			if (packkage != null)
				return packkage.getName().getFullyQualifiedName() + "." + classname;
		}
		return classname;
	}

	public static int selectedFormalParameter(ICompilationUnit cu, ASTNode selected) {
		// ascend the tree until we find a method.
		ASTNode iter = selected;
		while (iter != null) {
			if (iter.getNodeType() == ASTNode.SINGLE_VARIABLE_DECLARATION) {
				SingleVariableDeclaration svd = (SingleVariableDeclaration) iter;
				System.out.println("testing the singlevariabledeclaration");
				System.out.println(svd.getLocationInParent());
				MethodDeclaration md = (MethodDeclaration) svd.getParent();
				return md.parameters().indexOf(svd);
			}
			iter = iter.getParent();
		}
		return -1;
	}

	public static String getQualifiedClassName(ICompilationUnit cu, int offset, int length) {
		ASTNode root = EclipseUtils.getRootNode(cu);
		ASTNode selected = EclipseUtils.getCoveringNode(root, offset, length);
		return getQualifiedClassName(cu, root, selected);
	}


	  /**
	   * Returns all compilation units of a given project.
	   * @param javaProject Project to collect units
	   * @return List of org.eclipse.jdt.core.ICompilationUnit
	   * from org.eclipse.jdt.core.tests.performance
	   */
	  public static ArrayList<IFile> getProjectCompilationUnits(IJavaProject javaProject) throws JavaModelException {
	      IPackageFragmentRoot[] fragmentRoots = javaProject.getPackageFragmentRoots();
	      int length = fragmentRoots.length;
	      ArrayList<IFile> allUnits = new ArrayList<IFile>();
	      for (int i=0; i<length; i++) {
	          if (fragmentRoots[i] instanceof JarPackageFragmentRoot) continue;
	          IJavaElement[] packages= fragmentRoots[i].getChildren();
	          for (int k= 0; k < packages.length; k++) {
	              IPackageFragment pack = (IPackageFragment) packages[k];
	              ICompilationUnit[] units = pack.getCompilationUnits();
	              for (int u=0; u<units.length; u++) {
	                  allUnits.add((IFile)units[u].getResource());
//	                allUnits.add(units[u]);
	              }
	          }
	      }
	      return allUnits;
	  }


	  /**
	   * Convert a classpathEntry to an absolute path.
	   * @param currentEntry
	   * @return
	   */
	  public static IPath getAbsolutePath(IClasspathEntry currentEntry) {
		  return getAbsolutePath(currentEntry.getPath());
	  }

	  /**
	   * Convert a classpathEntry to an absolute path.
	   * @param currentEntry
	   * @return
	   */
	  public static IPath getAbsolutePath(IPath path) {
		  IPath absolutePath = null;
		  Object target = JavaModel.getTarget(ResourcesPlugin.getWorkspace().getRoot(), path, true);
		  if (target instanceof IResource) {
			  IResource res = (IResource) target;
			  absolutePath = res.getLocation();
		  }
		  if (absolutePath==null)
			  return null;
		  return absolutePath;
	  }

}
