package edu.berkeley.cs.bodik.svelte.plugin;

import java.util.Collection;

import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.action.Action;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.action.IMenuManager;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.jface.text.source.IVerticalRulerInfo;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.texteditor.AbstractRulerActionDelegate;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;
import org.eclipse.ui.texteditor.IUpdate;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.util.collections.Pair;

import edu.berkeley.cs.bodik.svelte.CGNodeUtils;
import edu.berkeley.cs.bodik.svelte.CallGraphUtils;

public class StatementPopupDelegate extends AbstractRulerActionDelegate {

	@Override
	protected IAction createAction(ITextEditor editor, IVerticalRulerInfo rulerInfo) {
		return new PopupAction(editor, rulerInfo);
	}

	@Override
	public void menuAboutToShow(IMenuManager m) {
		System.out.println("menu about to show! menu about to show! news @ 11");
	}

	private static class PopupAction extends Action implements IUpdate {
		private ITextEditor editor;
		private IVerticalRulerInfo rulerInfo;
		private ICompilationUnit cu;

		public PopupAction(ITextEditor editor, IVerticalRulerInfo rulerInfo) {
			this.editor = editor;
			this.rulerInfo = rulerInfo;
			this.cu = JavaUI.getWorkingCopyManager().getWorkingCopy(editor.getEditorInput());
		}

		@Override
		public void run() {
			StatementPopupDialog spd = new StatementPopupDialog(editor.getSite().getShell(), 0,
					true, true, true, true, "Hello", "World");
			spd.open();

			// get call graph
			IFile file = (IFile) cu.getResource();

			Pair<CallGraph, PointerAnalysis> cgpa = SliceEnvironmentCache.getInstance()
					.getCallGraphIfBuilt(file);
			if (cgpa == null)
				return;
			CallGraph cg = cgpa.fst;
			PointerAnalysis pa = cgpa.snd;

			// get location in file
			IDocumentProvider prov = editor.getDocumentProvider();
			IEditorInput input = editor.getEditorInput();
			IDocument document = prov.getDocument(input);

			int eclipseLine = rulerInfo.getLineOfLastMouseButtonActivity();
			int walaLine = eclipseLine + 1; // line numbers in eclipse start
			// from zero...

			try {
				String classname = EclipseJdtUtils.getQualifiedClassName(cu, document
						.getLineOffset(eclipseLine), document.getLineLength(eclipseLine));
				// find method
				CGNode cgn = CallGraphUtils.findMethodIncludingLineNumInClass(cg, walaLine,
						classname);
				if (cgn == null) {
					System.out.println("ERROR: couldn't find method include line " + walaLine
							+ " in class " + classname);
					return;
				}

				System.out.println(cgn.getIR());

				Collection<Statement> statements = CGNodeUtils.getStatementsFromLineNumber(cgn,
						walaLine);

				// do something with statements
				for (Statement s : statements) {
					if (s instanceof NormalStatement) {
						NormalStatement ns = (NormalStatement) s;
						SSAInstruction instr = ns.getInstruction();
						if (instr.hasDef()) {
							PointerKey pk = new LocalPointerKey(cgn, instr.getDef());
							System.out.println("-------------------------------------------");
							System.out.println("points-to set for def of statement: " + s);
							for (InstanceKey ik : pa.getPointsToSet(pk))
								System.out.println("  instance key: " + ik + " ; class "
										+ ik.getClass());
							System.out.println("-------------------------------------------");
							System.out.println("uses:");
							for (int u = 0; u < instr.getNumberOfUses(); u++) {
								System.out.println("use v" + instr.getUse(u));
								for (InstanceKey ik : pa.getPointsToSet(new LocalPointerKey(cgn,
										instr.getUse(u))))
									System.out.println("  instance key: " + ik + " ; class "
											+ ik.getClass());

							}

							System.out.println("-------------------------------------------");
							System.out.println("-------------------------------------------");
						}
					}
					System.out.println(s.getNode().getMethod().getSignature());
				}
			} catch (BadLocationException e) {
				e.printStackTrace();
			}

		}

		public void update() {
			setEnabled(true);
			setText("(changed) Statements "); // when is this executed? only
			// after the action is
			// actually run?
		}

	}

}
