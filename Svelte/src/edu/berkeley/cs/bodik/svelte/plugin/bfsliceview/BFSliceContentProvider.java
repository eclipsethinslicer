/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin.bfsliceview;

import java.util.ArrayList;

import org.eclipse.jface.viewers.IContentProvider;
import org.eclipse.jface.viewers.IStructuredContentProvider;
import org.eclipse.jface.viewers.ITreeContentProvider;
import org.eclipse.jface.viewers.StructuredSelection;
import org.eclipse.jface.viewers.TreeViewer;
import org.eclipse.jface.viewers.Viewer;

import edu.berkeley.cs.bodik.svelte.plugin.EclipseUtils;

public class BFSliceContentProvider implements ITreeContentProvider // , WordFile.Listener
{
	BFSliceInput input;
	TreeViewer viewer;
	ArrayList<BFSliceStatement> predStack;
	ArrayList<BFSliceStatement> children;

	private BFSliceStatement activeNode() {
		return predStack.get(predStack.size() - 1);
	}

	/**
	 * @param s
	 * @return true if the Statement (not BFSliceStatement, but Statement) is in the predStack
	 *         (including activeNode)
	 */
	public boolean inPredStack(BFSliceStatement s1) {
		for (BFSliceStatement s2 : predStack)
			if (s1.getStatement() == s2.getStatement())
				return true;
		return false;
	}

	private void recalculateChildren() {
		children = activeNode().getChildren();
	}

	/**
	 * @see IStructuredContentProvider#getElements(Object)
	 */
	public Object[] getElements(Object element) {
		return predStack.toArray();
	}

	/**
	 * @see IContentProvider#dispose()
	 */
	public void dispose() {
		input = null;
		viewer = null;
		predStack = null;
		children = null;
	}

	/**
	 * @see IContentProvider#inputChanged(Viewer, Object, Object)
	 */
	public void inputChanged(Viewer viewer, Object oldInput, Object newInput) {
		if (viewer instanceof TreeViewer) {
			this.viewer = (TreeViewer) viewer;
			if (newInput instanceof BFSliceInput) {
				input = (BFSliceInput) newInput;
				BFSliceStatement root = input.getRoot();
				predStack = new ArrayList<BFSliceStatement>();
				predStack.add(root); // first seed now active node
				children = new ArrayList<BFSliceStatement>();
				recalculateChildren();
				// this.viewer.setExpandedState(root, true);
				// if ( children.size() > 0 )
				// viewer.setSelection(new StructuredSelection(children.get(0)));
				// input.setListener(this);
			}
		}
	}

	public Object[] getChildren(Object parentElement) {
		if (parentElement == activeNode())
			return children.toArray();

		// TRUETREE
		// return ((BFSliceStatement)parentElement).getChildren().toArray();
		return null;
	}

	public Object getParent(Object element) {
		if (predStack.contains(element))
			return input;
		return activeNode(); // in children
	}

	public boolean hasChildren(Object element) {
		return element == activeNode();
		// TRUETREE
		// return true;
	}

	public void followDown(Object firstElement) {
		// TODO: could change to not-linear search, or search predStack like done above
		if (firstElement instanceof BFSliceStatement && children.contains(firstElement)) {
			predStack.add((BFSliceStatement) firstElement);
			recalculateChildren();
			viewer.refresh(predStack.get(predStack.size() - 2)); // old activeNode() -- upon
			// calling
			// getChildren()/hasChildren(),
			// viewer suddenly realizes it
			// doesn't have any
			viewer.add(input, firstElement);

			viewer.setExpandedState(firstElement, true);
			if (children.size() > 0) {
				viewer.setSelection(new StructuredSelection(children.get(0)));
				EclipseUtils.gotoStatement(children.get(0).getStatement());
			}
		}
	}

	public void backUp() {
		if (predStack.size() >= 2) {
			BFSliceStatement oldActiveNode = predStack.get(predStack.size() - 1);
			BFSliceStatement newActiveNode = predStack.get(predStack.size() - 2);

			oldActiveNode.freeChildren(); // frees memory by forgetting about children, we can
			// make more later
			viewer.remove(input, predStack.size() - 1); // remove oldActiveNode from the view (it
			// will be added back as a child)
			predStack.remove(predStack.size() - 1); // pop oldActiveNode
			recalculateChildren();
			viewer.refresh(newActiveNode); // re-expand old activeNode, it will discovered the new
			// children.

			viewer.setExpandedState(newActiveNode, true);
			viewer.setSelection(new StructuredSelection(oldActiveNode));
			EclipseUtils.gotoStatement(oldActiveNode.getStatement());
		}
	}

	public void nextQuestion(Object object) {
	}

	public void nextCheck(Object object) {
		int childindex = children.indexOf(object);
		if (childindex != -1) {
			((BFSliceStatement) object).markCheck();
			viewer.refresh(object);
		}
		if (childindex + 1 < children.size()) {
			viewer.setSelection(new StructuredSelection(children.get(childindex + 1)));
			EclipseUtils.gotoStatement(children.get(childindex + 1).getStatement());
		}
	}

	// /**
	// * @see Listener#added()
	// */
	// public void added(Word e) {
	// if (viewer != null)
	// viewer.add(e);
	// }

	// /**
	// * @see Listener#removed()
	// */
	// public void removed(Word e) {
	// if (viewer != null) {
	// viewer.setSelection(null);
	// viewer.remove(e);
	// }
	// }
}
