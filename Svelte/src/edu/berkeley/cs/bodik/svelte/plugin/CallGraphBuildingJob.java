/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin;

import java.util.Hashtable;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;

public class CallGraphBuildingJob extends Job {

	/**
	 * Expire and regenerate slice environments in a separate job thread
	 * 
	 * @author DarkWulf
	 */
	private class CallGraphBuildingJobThread extends Thread {
		@Override
		public void run() {
			for (IFile file : projectsAndMainFiles.values()) {
				registry.expireFile(file);
				registry.getCallGraph(file);
			}
		}
	}

	SliceEnvironmentCache registry = SliceEnvironmentCache.getInstance();
	Hashtable<IProject, IFile> projectsAndMainFiles;

	public CallGraphBuildingJob(Hashtable<IProject, IFile> projectsAndMainFiles) {
		super("Building call graph for project(s)" + projectsAndMainFiles.keySet());
		assert projectsAndMainFiles.size() > 0;
		this.projectsAndMainFiles = projectsAndMainFiles;
		setUser(true);
	}

	@SuppressWarnings("deprecation")
	@Override
	protected IStatus run(IProgressMonitor monitor) {
		// use a thread to allow cancelling
		// we're still waiting on WALA implementing CancelException, current implementation is
		// pretty evil. (Doesn't clean up file handles, etc)
		Thread builderThread = new CallGraphBuildingJobThread();
		builderThread.start();
		while (builderThread.isAlive()) {
			try {
				builderThread.join(100);
				if (monitor.isCanceled()) {
					builderThread.stop();
					for (IProject project : projectsAndMainFiles.keySet()) {
						registry.expireProject(project);
					}
					return Status.CANCEL_STATUS;
				}
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new Status(IStatus.OK, SveltePlugin.PLUGIN_ID, 0, "Finished", null);
	}

}
