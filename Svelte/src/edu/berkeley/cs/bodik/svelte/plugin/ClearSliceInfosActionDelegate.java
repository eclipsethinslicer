/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin;

import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaModelException;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;


public class ClearSliceInfosActionDelegate implements IEditorActionDelegate {

	IResource resource;
	IJavaProject proj;

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		// remember proj
		try {
			resource = JavaUI.getWorkingCopyManager().getWorkingCopy(targetEditor.getEditorInput())
					.getUnderlyingResource();
		} catch (JavaModelException e) {
			e.printStackTrace();
		}
		proj = JavaUI.getWorkingCopyManager().getWorkingCopy(targetEditor.getEditorInput())
				.getJavaProject();
	}

	public void run(IAction action) {
		try {
			ResourcesPlugin.getWorkspace().getRoot().deleteMarkers(
					"edu.berkeley.cs.bodik.svelte.plugin.sliceMarker", true,
					IResource.DEPTH_INFINITE);
			ResourcesPlugin.getWorkspace().getRoot().deleteMarkers(
					"edu.berkeley.cs.bodik.svelte.plugin.subSliceMarker", true,
					IResource.DEPTH_INFINITE);
		} catch (CoreException e) {
			e.printStackTrace();
		}
	}

	public void selectionChanged(IAction action, ISelection selection) {
	}
}

//import java.io.File;
//import java.io.FileNotFoundException;
//import java.io.IOException;
//import java.io.PrintWriter;
//import java.util.ArrayList;
//import java.util.LinkedList;
//import java.util.List;
//import java.util.Properties;
//import java.util.jar.JarFile;
//import org.eclipse.core.resources.IFile;
//import org.eclipse.jdt.core.ICompilationUnit;
//import org.eclipse.jdt.core.IJavaElement;
//import org.eclipse.jdt.core.IPackageFragment;
//import org.eclipse.jdt.core.IPackageFragmentRoot;
//import org.eclipse.jdt.core.JavaCore;
//import org.eclipse.jdt.core.dom.AST;
//import org.eclipse.jdt.core.dom.ASTParser;
//import org.eclipse.jdt.core.dom.AbstractTypeDeclaration;
//import org.eclipse.jdt.core.dom.Assignment;
//import org.eclipse.jdt.core.dom.CompilationUnit;
//import org.eclipse.jdt.core.dom.ExpressionStatement;
//import org.eclipse.jdt.core.dom.Modifier;
//import org.eclipse.jdt.core.dom.SimpleName;
//import org.eclipse.jdt.core.dom.TypeDeclaration;
//import org.eclipse.jdt.internal.core.JarPackageFragmentRoot;
//import org.eclipse.ui.IFileEditorInput;
//import com.ibm.wala.cast.java.client.JdtJavaSourceAnalysisEngine;
//import com.ibm.wala.cast.java.loader.JavaSourceLoaderImpl.ConcreteJavaMethod;
//import com.ibm.wala.classLoader.EclipseSourceFileModule;
//import com.ibm.wala.classLoader.JarFileModule;
//import com.ibm.wala.classLoader.SourceFileModule;
//import com.ibm.wala.core.tests.callGraph.CallGraphTestUtil;
//import com.ibm.wala.eclipse.util.EclipseProjectPath;
//import com.ibm.wala.ipa.callgraph.AnalysisScope;
//import com.ibm.wala.ipa.callgraph.CGNode;
//import com.ibm.wala.ipa.callgraph.CallGraph;
//import com.ibm.wala.ipa.callgraph.Entrypoint;
//import com.ibm.wala.ipa.callgraph.impl.Util;
//import com.ibm.wala.ipa.cha.IClassHierarchy;
//import com.ibm.wala.properties.WalaProperties;

//
//public class ClearSliceInfosActionDelegate implements IEditorActionDelegate {
//	private IFile file;
//
//	
//	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
//		this.file = ((IFileEditorInput) targetEditor.getEditorInput()).getFile();
//		
//	}
//
//
//	public void run(IAction action) {
//
//		// find name, yucky yuck
//		ASTParser parser = ASTParser.newParser(AST.JLS3);
//		ICompilationUnit icu = JavaCore.createCompilationUnitFrom(file);
//		parser.setResolveBindings(true);
//		parser.setSource(icu);
//		CompilationUnit unit = (CompilationUnit) parser.createAST(null);
//		String name = null;
//		for (Object o: unit.types()) {
//			AbstractTypeDeclaration atd = (AbstractTypeDeclaration)o;
//			if ((atd.getModifiers() & Modifier.PUBLIC) != 0)
//				name = "L" + atd.resolveBinding().getBinaryName().replace('.', '/');
//		}
//
//		String[] mainClassDescriptors = { name };
//        JdtJavaSourceAnalysisEngine engine = staticGetAnalysisEngine(mainClassDescriptors);
//        
//        try {
//			for ( IFile f: EclipseJdtUtils.getProjectCompilationUnits(JavaCore.create(file.getProject())) )
//		engine.addSourceModule(new EclipseSourceFileModule(f));
//		} catch (JavaModelException e1) {
//			e1.printStackTrace();
//		}
////        engine.addSourceModule(new EclipseSourceFileModule(file));
//        
//		for (String lib : rtJar) {
//			File libFile = new File(lib);
//			if (libFile.exists()) {
//				try {
//					engine.addSystemModule(new JarFileModule(new JarFile(libFile)));
//				} catch (IOException e) {
//					e.printStackTrace();
//				}
//			}
//		}
//		
//		PrintWriter printWriter = null;
//		try {
//			printWriter = new PrintWriter(new File("/home/evan/cmp-ir-jdt.txt"));
//			try {
//				CallGraph cg = engine.buildDefaultCallGraph();
//				for (CGNode n : cg) {// .getEntrypointNodes() ) {
//				// if ( n.getMethod().getSignature().contains("hello()"))
//					if (n.getMethod() instanceof ConcreteJavaMethod) {
//						printWriter.println(n.getIR());
//
//						System.out.println(n.getIR());
//					}
//				}
//				System.out.println("DONE");
//			} catch (Throwable e) {
//				e.printStackTrace();
//			} finally {
//				printWriter.close();
//			}
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//
//// ASTParser parser = ASTParser.newParser(AST.JLS3);
//// ICompilationUnit icu = JavaCore.createCompilationUnitFrom(file);
//// parser.setResolveBindings(true);
////		parser.setSource(icu);
////		CompilationUnit unit = (CompilationUnit) parser.createAST(null);
//////		((SimpleName)((Assignment)((ExpressionStatement)((TypeDeclaration)unit.types().get(0)).getMethods()[0].getBody().statements().get(2)).getExpression()).getLeftHandSide()).resolveBinding()		
////		unit.equals(icu);
//	}
//
//	public void selectionChanged(IAction action, ISelection selection) {
//	}
//
//
//
//	protected static String javaHomePath;
//	public static List<String> rtJar;
//	static {
//		boolean found = false;
//		try {
//			rtJar = new LinkedList<String>();
//
//			Properties p = WalaProperties.loadProperties();
//			javaHomePath = p.getProperty(WalaProperties.J2SE_DIR);
//
//			if (new File(javaHomePath).isDirectory()) {
//				if ("Mac OS X".equals(System.getProperty("os.name"))) { // nick
//					/**
//					 * todo: {@link WalaProperties#getJ2SEJarFiles()}
//					 */
//					rtJar.add(javaHomePath + "/Classes/classes.jar");
//					rtJar.add(javaHomePath + "/Classes/ui.jar");
//				} else {
//					rtJar.add(javaHomePath + File.separator + "classes.jar");
//					rtJar.add(javaHomePath + File.separator + "rt.jar");
//					rtJar.add(javaHomePath + File.separator + "core.jar");
//					rtJar.add(javaHomePath + File.separator + "vm.jar");
//				}
//				found = true;
//			}
//		} catch (Exception e) {
//			// no properties
//		}
//
//		if (!found) {
//			javaHomePath = System.getProperty("java.home");
//			if ("Mac OS X".equals(System.getProperty("os.name"))) { // nick
//				rtJar.add(javaHomePath + "/../Classes/classes.jar");
//				rtJar.add(javaHomePath + "/../Classes/ui.jar");
//			} else {
//				rtJar.add(javaHomePath + File.separator + "lib" + File.separator + "rt.jar");
//				rtJar.add(javaHomePath + File.separator + "lib" + File.separator + "core.jar");
//				rtJar.add(javaHomePath + File.separator + "lib" + File.separator + "vm.jar");
//				rtJar.add(javaHomePath + File.separator + "lib" + File.separator + "classes.jar");
//			}
//		}
//	}
//
//    protected static JdtJavaSourceAnalysisEngine staticGetAnalysisEngine(final String[] mainClassDescriptors) {
//    	JdtJavaSourceAnalysisEngine engine = new JdtJavaSourceAnalysisEngine() {
//                protected Iterable<Entrypoint> makeDefaultEntrypoints(AnalysisScope scope, IClassHierarchy cha) {
//                        return Util.makeMainEntrypoints(EclipseProjectPath.SOURCE_REF, cha, mainClassDescriptors);
//                }
//        };
//        engine.setExclusionsFile(CallGraphTestUtil.REGRESSION_EXCLUSIONS);
//        return engine;
//    }
//
//}
