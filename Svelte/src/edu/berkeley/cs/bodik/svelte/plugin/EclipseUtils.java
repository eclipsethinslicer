/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Status;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.Signature;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ASTParser;
import org.eclipse.jdt.internal.corext.dom.NodeFinder;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorDescriptor;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorRegistry;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.internal.WorkbenchPlugin;
import org.eclipse.ui.internal.dialogs.DialogUtil;
import org.eclipse.ui.part.FileEditorInput;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.ui.texteditor.IDocumentProvider;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.slicer.NormalReturnCallee;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.ParamCallee;
import com.ibm.wala.ipa.slicer.Statement;

import edu.berkeley.cs.bodik.svelte.CGNodeUtils;
import edu.berkeley.cs.bodik.svelte.SourcePosition;
import edu.berkeley.cs.bodik.svelte.StatementUtils;

/**
 * Should probably move some of this to SliceEnvironment.
 * 
 * @author evan
 * 
 */
public class EclipseUtils {

	public static IFile findIFileForFilename(String filename) {
		if (filename != null) {
			IWorkspace workspace = ResourcesPlugin.getWorkspace();
			IPath location = Path.fromOSString(filename);
			IFile f = workspace.getRoot().getFileForLocation(location);
			return f;
		}
		return null;
	}

	public static class MethodSignature {
		private String packageName;
		private String className;
		private String methodName;
		private String methodParams;
		private String methodReturnValue;

		public MethodSignature(String signature) {
			// find it and go to it!
			int class_met_boundary = signature.lastIndexOf('.');

			String pkg_and_classname = signature.substring(0, class_met_boundary);
			String method_signature = signature.substring(class_met_boundary + 1);

			int pkg_class_boundary = pkg_and_classname.lastIndexOf('.');

			// TODO: if in default package?
			if (pkg_class_boundary == -1) {
				this.packageName = null;
				this.className = pkg_and_classname;
			} else {
				this.packageName = pkg_and_classname.substring(0, pkg_class_boundary);
				this.className = pkg_and_classname.substring(pkg_class_boundary + 1);
			}

			int met_param_boundary = method_signature.indexOf('(');
			int param_ret_boundary = method_signature.indexOf(')');

			this.methodName = method_signature.substring(0, met_param_boundary);
			if (this.methodName.equals("<init>"))
				this.methodName = this.className;
			this.methodParams = method_signature.substring(met_param_boundary + 1,
					param_ret_boundary);
			this.methodReturnValue = method_signature.substring(param_ret_boundary + 1);
		}

		public String getPackageName() {
			return packageName;
		}

		public String getClassName() {
			return className;
		}

		public String getMethodSignature() {
			return methodName + '(' + methodParams + ')' + methodReturnValue;
		}

		public String getQualifiedClassName() {
			if (packageName == null)
				return className;
			return packageName + "." + className;
		}

		public String getSignature() {
			return getQualifiedClassName() + "." + getMethodSignature();
		}

		public String[] getUnqualifiedParameterTypes() {
			return Signature.getParameterTypes(getMethodSignature().replace('/', '.').replaceAll(
					"L[^;]*\\.([^;\\.]*);", "Q$1;")); // java/lang/String -> QString
		}

		public String getMethodName() {
			return methodName;
		}

		public boolean matchesMethod(IMethod m) {
			String paramTypes[] = m.getParameterTypes();
			for ( int i = 0; i < paramTypes.length; i++ )
				paramTypes[i] = paramTypes[i].replaceAll("L[^;]*\\.([^;\\.]*);", "Q$1;");
			if ( getUnqualifiedParameterTypes().equals(paramTypes) )
				return true;
			return false;
		}
	}

	public static void openFile(IFile file) {
		// get default editor descriptor
		IEditorRegistry editorRegistry = WorkbenchPlugin.getDefault().getEditorRegistry();
		IEditorDescriptor defaultEditorDescriptor = editorRegistry
				.getDefaultEditor(file.toString());
		// // || defaultEditorDescriptor.isOpenExternal() is only eclipse 3.x!!!
		if (defaultEditorDescriptor == null) {
			defaultEditorDescriptor = editorRegistry.getDefaultEditor("dummy.txt");
		}

		// Open new file in editor
		IWorkbenchWindow dw = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		FileEditorInput fileEditorInput = new FileEditorInput(file);
		try {
			IWorkbenchPage page = dw.getActivePage();
			if (page != null)
				// page.openEditor(fileEditorInput,"org.eclipse.ui.DefaultTextEditor");
				page.openEditor(fileEditorInput, defaultEditorDescriptor.getId());
		} catch (PartInitException e) {
			DialogUtil.openError(dw.getShell(), "Could not open new file", e.getMessage(), e);
		}

	}

	/**
	 * Uses the Slice Environment's project to look for the file.
	 * 
	 * Passing a ParamCallee statement will highlight the line of the function definition.
	 * 
	 * @param s
	 */
	public static void gotoStatement(Statement ss) {
		if (ss == null)
			return;

		if (ss instanceof ParamCallee) {
			ParamCallee pac = (ParamCallee) ss;
			CGNode node = ((ParamCallee) ss).getNode();

			int arg_index = pac.getValueNumber() - 1;
			if (!node.getMethod().isStatic())
				arg_index--;

			EclipseJdtUtils.gotoNodeParam(node.getMethod().getSignature().toString(), arg_index,
					EclipseJdtUtils.getJavaProjectFromProject(SliceEnvironment.env().getProject()));
		} else if (ss instanceof NormalReturnCallee) {
			CGNode node = ((NormalReturnCallee) ss).getNode();
			EclipseJdtUtils.gotoNode(node.getMethod().getSignature().toString(), EclipseJdtUtils.getJavaProjectFromProject(SliceEnvironment.env().getProject()));
		} else if (!(ss instanceof NormalStatement)) {
			ss = StatementUtils.getCallerStatement(ss);
		}

		if (ss != null && ss instanceof NormalStatement) {
			SourcePosition sp = CGNodeUtils.getSourcePositionOfInstructionIndex(ss.getNode(),
					((NormalStatement) ss).getInstructionIndex(), EclipseJdtUtils
							.getJavaProjectFromOpenEditor());
			if (sp != null) {
				if (sp.filename == null)
					return;
				IFile f = EclipseUtils.findIFileForFilename(sp.filename);
				if (f != null) {
					EclipseUtils.openFile(f);

					IEditorPart iep = PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage().getActiveEditor();
					AbstractTextEditor e = (AbstractTextEditor) iep;

					if ( sp.offsetStart == -1 ) {
						// Shrike: line numbers only.
						IDocumentProvider prov = ((AbstractTextEditor) iep).getDocumentProvider();
						IEditorInput input = iep.getEditorInput();
						IDocument document = prov.getDocument(input);
						try {
							int start = document.getLineOffset(sp.lineStart-1); // line numbers off by one.
							int end = document.getLineOffset(sp.lineEnd-1)+document.getLineLength(sp.lineEnd-1)-1;
							e.selectAndReveal(start,end-start);
						} catch (BadLocationException e1) {
						}
					} else {
						e.selectAndReveal(sp.offsetStart, sp.offsetEnd - sp.offsetStart);
					}
				}
			}

		}
	}

	@SuppressWarnings("deprecation")
	public static ASTNode getRootNode(ICompilationUnit cu) {
		ASTParser parser = ASTParser.newParser(AST.JLS3);
		parser.setResolveBindings(false);
		parser.setSource(cu);
		ASTNode root = parser.createAST(null);
		return root;
	}
	
	@SuppressWarnings("deprecation")
	public static ASTNode getRootNodeWithBindings(ICompilationUnit cu) {
		ASTParser parser = ASTParser.newParser(AST.JLS3);
		parser.setResolveBindings(true);
		parser.setSource(cu);
		ASTNode root = parser.createAST(null);
		return root;
	}

	public static ASTNode getCoveringNode(ASTNode root, int offset, int length) {
		NodeFinder nf = new NodeFinder(offset, length);
		root.accept(nf);
		ASTNode selected = nf.getCoveringNode();
		return selected;
	}

	public static void errorMsgInUIThread(final String title, final String msg) {
		new UIJob("errormessage") {
			@Override
			public IStatus runInUIThread(IProgressMonitor monitor) {
				MessageDialog
						.openError(
								PlatformUI.getWorkbench().getActiveWorkbenchWindow()
										.getShell(),
								title,
								msg);
				return new Status(IStatus.OK, SveltePlugin.PLUGIN_ID, null);
			}
		}.schedule();

	}
}
