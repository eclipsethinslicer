/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin;

import java.util.Hashtable;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;

import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.util.collections.Pair;

import edu.berkeley.cs.bodik.svelte.slicers.AbstractSliceEnvironment;
import edu.berkeley.cs.bodik.svelte.slicers.SlicerRegistry;

/**
 * The CallGraphProjectCache is a map of Java projects to their call graph and pointer analysis.
 * 
 * Should generally be used as a singleton global to an Eclipse instance.
 * 
 * @author darkwulf
 */
public class SliceEnvironmentCache {
	static SliceEnvironmentCache instance = null;

	/**
	 * Retrieves the ProjectCallGraphCache singleton instance
	 * 
	 * @return
	 */
	public static SliceEnvironmentCache getInstance() {
		if (instance == null)
			instance = new SliceEnvironmentCache();
		return instance;
	}

	private Map<IProject, AbstractSliceEnvironment> callGraphs = new Hashtable<IProject, AbstractSliceEnvironment>();

	protected SliceEnvironmentCache() {
	}

	/**
	 * Retrieves a cg/pa if it has been built, and returns null otherwise
	 * 
	 * (Call graph/pointer analysis builds are potentially quite slow, so it is not always safe to
	 * built it as a side effect)
	 * 
	 * NOTE: should rename this method...
	 * 
	 * @param file
	 *            The file whose cg/pa to retrieve
	 * @return a cg/pa pair or returns null if the pair has not already been cached
	 */
	public Pair<CallGraph, PointerAnalysis> getCallGraphIfBuilt(IFile file) {
		if (callGraphs.containsKey(file.getProject())) {
			AbstractSliceEnvironment env = callGraphs.get(file.getProject());
			return Pair.make(env.getCallGraph(), env.getPointerAnalysis());
		} else {
			return null;
		}
	}

	public AbstractSliceEnvironment getSliceEnvironmentIfBuilt(IFile file) {
		return this.callGraphs.get(file.getProject());
	}

	public AbstractSliceEnvironment getSliceEnvironment(IFile file) {
		AbstractSliceEnvironment se = this.getSliceEnvironmentIfBuilt(file);
		if (se != null) {
			return se;
		} else {
			this.generateCallGraph(file);
			se = this.getSliceEnvironmentIfBuilt(file);
			return se;
		}
	}

	/**
	 * Retrieves a cg/pa (may build if they have not already been cached!)
	 * 
	 * NOTE: should rename this method...
	 * 
	 * @param file
	 * @return a cg/pa pair
	 */
	public Pair<CallGraph, PointerAnalysis> getCallGraph(IFile file) {
		Pair<CallGraph, PointerAnalysis> i = this.getCallGraphIfBuilt(file);
		if (i != null) {
			return i;
		} else {
			return this.generateCallGraph(file);
		}
	}

	public void expireFile(IFile file) {
		if (callGraphs.containsKey(file.getProject())) {
			callGraphs.remove(file.getProject());
		}
	}

	public void expireProject(IProject proj) {
		if (callGraphs.containsKey(proj)) {
			callGraphs.remove(proj);
		}
	}

	/**
	 * Creates the call graph for a file and saves it as a persistent property
	 * 
	 * @param file
	 */
	private Pair<CallGraph, PointerAnalysis> generateCallGraph(IFile file) {
		AbstractSliceEnvironment se = SlicerRegistry.getInstance().getEnvironment(file,
				System.currentTimeMillis());
		this.callGraphs.put(file.getProject(), se);
		return Pair.make(se.getCallGraph(), se.getPointerAnalysis());
	}

	/**
	 * TODO: move this someplace maybe. Returns true if, the project source files were modified in
	 * Eclipse after the last call graph for the project was built. If there is no call graph /
	 * SliceEnvironment, returns false (slicing thread will take care of it.)
	 * 
	 * @param proj
	 * @return
	 */
	public boolean isProjectOutdated(IProject proj) {
		AbstractSliceEnvironment se = callGraphs.get(proj);
		if (se == null)
			return false;
		return CallGraphUpdaterDelegate.getProjectModTimestamp(proj) > se.getModTimestamp();
	}

}
