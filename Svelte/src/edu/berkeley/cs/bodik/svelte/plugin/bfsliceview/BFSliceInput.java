/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin.bfsliceview;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Iterator;

import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.util.graph.Graph;

import edu.berkeley.cs.bodik.svelte.Slice;

interface BFSliceStatement {
	Statement getStatement();

	public ArrayList<BFSliceStatement> getChildren();

	public boolean hasChildren();

	/**
	 * A BFSliceStatement normally keeps a cache of its children. When the children are no longer
	 * needed (i.e. when we go up a level) they can forgotten about so the GC can free the memory.
	 * This is necessary because the <code>BFSliceInput.getChildren()</code> function must
	 * generate its children for each parent so we can have different BFSliceStatements referring to
	 * the same Statement.
	 */
	void freeChildren();

	/**
	 * Markings. Along with freeChildren, these don't really make sense for the RootStatement.
	 */
	public void markCheck();

	public void unmarkCheck();

	public boolean isMarkedChecked();

	public BFSliceStatement getParent();

}

public class BFSliceInput {
	private Graph<Statement> graph;
	private Slice seed;
	private RootStatement root;

	private Hashtable<Statement, Boolean> markings = new Hashtable<Statement, Boolean>();

	/**
	 * Wrapper around a real statement. This allows one Statement in the slice to have more than one
	 * incarnation in the viewer, since they are only equal if they are the same object.
	 * 
	 * @author evan
	 * 
	 */
	private class RealStatement implements BFSliceStatement {
		Statement s;
		ArrayList<BFSliceStatement> children;
		private BFSliceStatement parent;

		public RealStatement(Statement s, BFSliceStatement parent) {
			this.s = s;
			children = null;
			this.parent = parent;
		}

		@Override
		public String toString() {
			return s.toString();
		}

		@Override
		public boolean equals(Object x) {
			return x == this;
		}

		public Statement getStatement() {
			return s;
		}

		public ArrayList<BFSliceStatement> getChildren() {
			if (children == null) {
				children = new ArrayList<BFSliceStatement>();
				Iterator<? extends Statement> iter = graph.getSuccNodes(s);
				while (iter.hasNext()) {
					children.add(new RealStatement(iter.next(), this));
				}
			}
			return children;
		}

		public void freeChildren() {
			if (children != null) {
				children.clear();
				children = null;
			}
		}

		public void markCheck() {
			markings.put(s, new Boolean(true));
		}

		public void unmarkCheck() {
			if (markings.containsKey(s))
				markings.remove(s);
		}

		public boolean isMarkedChecked() {
			return markings.containsKey(s) && markings.get(s).booleanValue();
		}

		public BFSliceStatement getParent() {
			return parent;
		}

		public boolean hasChildren() {
			return graph.getSuccNodes(s).hasNext();
		}
	}

	/**
	 * Fake statement representing parent of all seeds in a slice.
	 * 
	 * @author evan
	 */
	private class RootStatement implements BFSliceStatement {
		ArrayList<BFSliceStatement> children;

		@Override
		public boolean equals(Object obj) {
			return obj == root;
		}

		@Override
		public int hashCode() {
			return graph.hashCode() + seed.hashCode();
		}

		@Override
		public String toString() {
			return "All Seed Statements";
		}

		public Statement getStatement() {
			return null;
		}

		public void freeChildren() {
			// you cannot back up beyond the root node
		}

		public ArrayList<BFSliceStatement> getChildren() {
			if (children == null) {
				children = new ArrayList<BFSliceStatement>();
				Iterator<? extends Statement> iter = seed.iterator();
				while (iter.hasNext()) {
					children.add(new RealStatement(iter.next(), root));
				}
			}
			return children;
		}

		public boolean isMarkedChecked() {
			return false;
		}

		public void markCheck() {
		}

		public void unmarkCheck() {
		}

		public BFSliceStatement getParent() {
			// TODO Auto-generated method stub
			return null;
		}

		public boolean hasChildren() {
			return true;
		}

	}

	public BFSliceInput(Graph<Statement> graph, Slice seed) {
		this.graph = graph;
		this.seed = seed;
		this.root = new RootStatement();
	}

	// in the future we might wasn't abstractions to different kinds of slices (CI thin, CS thin, CS
	// thick) in here.
	public BFSliceStatement getRoot() {
		return root;
	}

	// public void add(Word word) {
	// list.add(word);
	// writeFile();
	// if (listener != null)
	// listener.added(word);
	// }

	// public void remove(Word word) {
	// list.remove(word);
	// writeFile();
	// if (listener != null)
	// listener.removed(word);
	// }

	// public Word find(String str) {
	// Iterator iter = list.iterator();
	// while (iter.hasNext()) {
	// Word word = (Word)iter.next();
	// if (str.equals(word.toString()))
	// return word;
	// }
	// return null;
	// }
}
