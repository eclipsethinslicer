package edu.berkeley.cs.bodik.svelte.plugin;

import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.util.graph.Graph;

import edu.berkeley.cs.bodik.svelte.Slice;
import edu.berkeley.cs.bodik.svelte.cislicer.ThinSlicer;
import edu.berkeley.cs.bodik.svelte.slicers.AbstractSliceEnvironment;

/**
 * Stub implementation to maintain BC until I rewrite old code :/
 * 
 * @author DarkWulf
 * 
 */
public class SliceEnvironment {
	private static AbstractSliceEnvironment env;
	private static Slice seed;
	private static Slice sliceslice;
	private static ThinSlicer thinslicer;
	private static Graph<Statement> csDepGraph;

	public static void setCurrentSliceData(AbstractSliceEnvironment env, Slice seed, Slice sliceslice,
			ThinSlicer thinslicer, Graph<Statement> csDepGraph) {
		SliceEnvironment.env = env;
		SliceEnvironment.seed = seed;
		SliceEnvironment.sliceslice = sliceslice;
		SliceEnvironment.thinslicer = thinslicer;
		SliceEnvironment.csDepGraph = csDepGraph;
	}

	public static Graph<Statement> getDepGraph() {
		if ( csDepGraph != null )
			return csDepGraph;
		return thinslicer.getDepGraph();
	}

	public static Slice getSeed() {
		return seed;
	}

	public static Slice getSlice() {
		return sliceslice;
	}

	public static AbstractSliceEnvironment env() {
		return env;
	}

	public static int getDepth() {
		return -1;
	}

}
