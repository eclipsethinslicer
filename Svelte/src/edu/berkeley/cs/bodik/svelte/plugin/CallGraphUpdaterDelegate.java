/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin;

import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.IResourceChangeEvent;
import org.eclipse.core.resources.IResourceChangeListener;
import org.eclipse.core.resources.IResourceDelta;
import org.eclipse.core.resources.IResourceDeltaVisitor;
import org.eclipse.core.resources.IWorkspace;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.jobs.ISchedulingRule;
import org.eclipse.core.runtime.jobs.MultiRule;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;

public class CallGraphUpdaterDelegate implements IEditorActionDelegate {
	private static Map<IProject,Long> projectModTimestamps = new Hashtable<IProject,Long>(); 
	static CallGraphUpdater callGraphUpdater = null;
	static private boolean autoUpdate;
	
	public static class CallGraphUpdater implements IResourceChangeListener {
		/**
		 * A ResourceDeltaVisitor that adds all java source files in the resource delta to the given
		 * list. This is essentially a glorified filter.
		 * 
		 * @author darkwulf
		 */
		private class ResourceDeltaJavaSourceFinder implements IResourceDeltaVisitor {
			private List<IFile> javaSources;

			public ResourceDeltaJavaSourceFinder(List<IFile> javaSources) {
				this.javaSources = javaSources;
			}

			public boolean visit(IResourceDelta delta) throws CoreException {
				// Only care about new or changed files/directories, and not deletions
				if ((delta.getKind() & IResourceDelta.REMOVED) != 0)
					return false;

				// we only care about java files who have been added, or whose CONTENT has changed
				// (markers changed doesn't count!)
				IResource resource = delta.getResource();
				if (resource.getType() == IResource.FILE
						&& "java".equalsIgnoreCase(resource.getFileExtension())) {
					if ((delta.getKind() & IResourceDelta.ADDED) != 0
							|| ((delta.getKind() & IResourceDelta.CHANGED) != 0 && ((delta
									.getFlags() & IResourceDelta.CONTENT) != 0))) {

						javaSources.add((IFile) resource);
					}
				}
				return true;
			}
		}

		/**
		 * On resource change, scan for Java source files and generate call graph and pointer
		 * analysis to speed slicing.
		 */
		public void resourceChanged(IResourceChangeEvent event) {
			// we are only interested in POST_CHANGE events
			if (event.getType() != IResourceChangeEvent.POST_CHANGE)
				return;

			// find changed files
			List<IFile> files = new java.util.ArrayList<IFile>();
			try {
				event.getDelta().accept(new ResourceDeltaJavaSourceFinder(files));
			} catch (CoreException e) {
				e.printStackTrace();
			}

			if ( files == null )
				return;
				
			// record "last modified" timestamps for projects.
			long modTimestamp = System.currentTimeMillis();
			for (IFile f : files)
				projectModTimestamps.put(f.getProject(), new Long(modTimestamp));
			
			if ( files.size() != 0 && autoUpdate ) {
				// rebuild...

				// make a list of projects, each with a "principally modified file" (serves as
				// entrypoint for now)
				Hashtable<IProject, IFile> projsChanged = new Hashtable<IProject, IFile>();
				int n_projects = 0;
				for (IFile f : files) {
					if (!projsChanged.contains(f.getProject())) {
						projsChanged.put(f.getProject(), f);
						n_projects++;
					}
				}

				// calculate schedule rule -- combine changed projects
				// TODO: figure this out for real. lock all projects of all files, plus req'd
				// projects?
				ISchedulingRule rules[] = new ISchedulingRule[n_projects];
				int i = 0;
				for (IProject p : projsChanged.keySet())
					rules[i++] = ResourcesPlugin.getWorkspace().getRuleFactory().modifyRule(p);
				ISchedulingRule rule = MultiRule.combine(rules);

				CallGraphBuildingJob job = new CallGraphBuildingJob(projsChanged);
				job.setRule(rule);
				job.schedule();
			}

		}
	}


	/**
	 * Adds a CallGraphUpdater listener to the workspace that will invalidate (and then rebuild) the
	 * call graph cache as needed.
	 */
	public static void addResourceListener() {
		IWorkspace workspace = ResourcesPlugin.getWorkspace();
		callGraphUpdater = new CallGraphUpdater();
		workspace.addResourceChangeListener(callGraphUpdater);
	}

	public void run(IAction action) {
		autoUpdate = action.isChecked();
	}

	public void setActiveEditor(IAction action, IEditorPart targetEditor) {
		// do nothing
	}

	public void selectionChanged(IAction action, ISelection selection) {
		// do nothing
	}
	
	// TODO: where to put this function?
	/**
	 * @return
	 * 0 if project has not been modified that we know of. 
	 */
	public static long getProjectModTimestamp(IProject proj) {
		Long timestamp = projectModTimestamps.get(proj);
		if ( timestamp == null )
			return 0;
		return timestamp.longValue();
	}

}
