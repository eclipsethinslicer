package edu.berkeley.cs.bodik.svelte.plugin;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class DotUtils {
	private static String dotLocation = null;
	private static final String[] commonLocations = { "/usr/bin/dot",
			"/usr/local/bin/dot",
			"c:\\Program Files\\Graphviz2.18\\bin\\dot.exe",
			"c:\\Program Files\\Graphviz2.16.1\\bin\\dot.exe", };

	public static String getDotLocation() throws FileNotFoundException {
		if (dotLocation == null) {
			dotLocation = whereIsDot();
			if (dotLocation == null)
				throw new FileNotFoundException(
						"StupidDotFinder could not find dot, please make sure Dot installed or add path in StupidDotFinder");
		}
		return dotLocation;
	}

	private static String whereIsDot() {
		for (String s : commonLocations)
			if (new File(s).exists())
				return s;
		return null;

	}

	/**
	 * Turn a .dot file into a .png file and a .cmapx file giving the coordinate map
	 * @param pngFile
	 * @param cmapxFile
	 * @param dotFile
	 * @throws IOException
	 */
	public static void spawnDot(File pngFile, File cmapxFile, File dotFile)
			throws IOException {
		if (dotFile == null) {
			throw new IllegalArgumentException("dotFile is null");
		}
		String[] cmdarray = { getDotLocation(), "-Tpng", "-o", pngFile.getAbsolutePath(),
				"-Tcmapx", "-o", cmapxFile.getAbsolutePath(), "-v", dotFile.getAbsolutePath() };
		System.out.println("spawning process " + Arrays.toString(cmdarray));
		Process p = Runtime.getRuntime().exec(cmdarray);
		BufferedInputStream output = new BufferedInputStream(p.getInputStream());
		BufferedInputStream error = new BufferedInputStream(p.getErrorStream());
		boolean repeat = true;
		while (repeat) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e1) {
				e1.printStackTrace();
				// just ignore and continue
			}
			if (output.available() > 0) {
				byte[] data = new byte[output.available()];
				int nRead = output.read(data);
				System.err.println("read " + nRead
						+ " bytes from output stream");
			}
			if (error.available() > 0) {
				byte[] data = new byte[error.available()];
				int nRead = error.read(data);
				System.err
						.println("read " + nRead + " bytes from error stream");
			}
			try {
				p.exitValue();
				// if we get here, the process has terminated
				repeat = false;
				System.out.println("process terminated with exit code "
						+ p.exitValue());
			} catch (IllegalThreadStateException e) {
				// this means the process has not yet terminated.
				repeat = true;
			}
		}
	}

	/**
	 * Dump the string s into the file f.
	 * @param s
	 * @param f
	 * @throws IOException
	 */
	public static void writeDotFile(String s, File f) throws IOException {
		FileWriter fw = new FileWriter(f);
		fw.write(s);
		fw.close();
	}
}
