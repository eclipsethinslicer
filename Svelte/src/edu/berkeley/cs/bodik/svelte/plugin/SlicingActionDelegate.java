/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte.plugin;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.PackageDeclaration;
import org.eclipse.jdt.core.dom.TypeDeclaration;
import org.eclipse.jdt.ui.IWorkingCopyManager;
import org.eclipse.jdt.ui.JavaUI;
import org.eclipse.jface.action.IAction;
import org.eclipse.jface.dialogs.MessageDialogWithToggle;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.jface.text.Position;
import org.eclipse.jface.viewers.ISelection;
import org.eclipse.ui.IEditorActionDelegate;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.texteditor.AbstractTextEditor;

import edu.berkeley.cs.bodik.svelte.Slicing.SliceType;

/**
 * Base class for the various *EditorActionDelegates that provides the bootstrapping required to
 * take a slice, and then takes it.
 * 
 * @author evan
 * 
 */
public abstract class SlicingActionDelegate implements IEditorActionDelegate {
	private IEditorPart editor;
	protected ICompilationUnit cu;

	public void selectionChanged(IAction action, ISelection selection) {
	}

	public static Position getPositionFromLineNum(String s, int linenum) {
		int start = 0;
		int l = 0;
		int i = 0;
		for (i = 0; i < s.length(); i++) {
			if (s.charAt(i) == '\n') {
				l++;
				if (l == linenum)
					start = i + 1;
				if (l == linenum + 1)
					return new Position(start, i - start);
			}
		}
		return new Position(start, i - start);
	}

	public static String getPackagePlusClassName(ASTNode root, ASTNode selected) {
		ASTNode iter = selected;
		while (iter != null) {
			if (iter.getNodeType() == ASTNode.TYPE_DECLARATION) {
				TypeDeclaration td = (TypeDeclaration) iter;
				if (td.isInterface()) {
					return null; // error: in an interface
				} else {
					String classname = td.getName().getFullyQualifiedName();
					if (root instanceof org.eclipse.jdt.core.dom.CompilationUnit) {
						PackageDeclaration packkage = ((org.eclipse.jdt.core.dom.CompilationUnit) root)
								.getPackage();
						if (packkage != null)
							return packkage.getName().getFullyQualifiedName() + "." + classname;
					}
					return classname;
				}
			}
			iter = iter.getParent();
		}
		return null;
	}

	public void run(IAction action, SliceType slicerType) {
		run(action, slicerType, "edu.berkeley.cs.bodik.svelte.plugin.sliceMarker", 16);
	}

	public void run(IAction action, SliceType slicerType, String markerType, int depth) {
		ITextSelection its = (ITextSelection) ((AbstractTextEditor) editor).getSelectionProvider()
				.getSelection();
		int line_num = its.getStartLine();
		line_num = line_num + 1; // their line numbers start at 0.
		IFile file = ((IFileEditorInput) editor.getEditorInput()).getFile();
		IProject proj = file.getProject();

		// check for dirty editors in the project. This isn't strictly necessary (handled by
		// saveAllEditors), but helps us out later when we want to know whether we should prompt
		// whether to rebuild CG
		boolean dirty = false;
		for (IWorkbenchWindow window : PlatformUI.getWorkbench().getWorkbenchWindows()) {
			for (IWorkbenchPage page : window.getPages()) {
				for (IEditorPart editor : page.getDirtyEditors()) {
					if (editor.getEditorInput() instanceof IFileEditorInput
							&& ((IFileEditorInput) editor.getEditorInput()).getFile().getProject() == proj) {
						dirty = true;
						break;
					}
				}
				if (dirty)
					break;
			}
			if (dirty)
				break;
		}
		// ask if we want to save.
		if (dirty)
			if (PlatformUI.getWorkbench().saveAllEditors(true) == false)
				return;

		// may need to rebuild call graph
		if (SliceEnvironmentCache.getInstance().isProjectOutdated(file.getProject())) {

			if (dirty) // the files have just changed because we saved them (probably). don't even
				// ask, just rebuild the call graph.
				SliceEnvironmentCache.getInstance().expireProject(file.getProject());
			else {
				MessageDialogWithToggle rebuildDia = MessageDialogWithToggle
						.openYesNoCancelQuestion(
								PlatformUI.getWorkbench().getActiveWorkbenchWindow().getShell(),
								"Rebuild project call graph?",
								"The project source files have changed since the call graph was last built. As a result slicing may not be synchronized with the latest source code. Do you wish to rebuild the callgraph?",
								null, false, null, null);
				// TODO: preferences
				int result = rebuildDia.getReturnCode();
				if (result == 2) {
					SliceEnvironmentCache.getInstance().expireProject(file.getProject());
				} else if (result == MessageDialogWithToggle.CANCEL || result == -1) {
					return;
				}
			}
		}

		System.out.println("Slicing from: " + file.getLocation().toString());

		System.out.println(" **** OK, BEGINNING SLICING OPERATION **** ");

		SlicingJob job = new SlicingJob(slicerType, depth, editor, its);
		job.setRule(ResourcesPlugin.getWorkspace().getRuleFactory().modifyRule(file.getProject()));
		job.schedule();

		System.out.println(" ****      END SLICING OPERATION      **** ");

	}

	public void setActiveEditor(IAction action, IEditorPart editor) {
		cu = JavaUI.getWorkingCopyManager().getWorkingCopy(editor.getEditorInput());
		// how the heck am I supposed to know the above?
		this.editor = editor;
		IWorkingCopyManager wcm = JavaUI.getWorkingCopyManager();
		wcm.getWorkingCopy(editor.getEditorInput());
	}

}
