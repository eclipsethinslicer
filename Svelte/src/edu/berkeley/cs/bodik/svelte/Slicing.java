/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte;

import java.util.Collection;

import com.ibm.wala.cast.java.ipa.modref.AstJavaModRef;
import com.ibm.wala.eclipse.util.CancelException;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.slicer.SDG;
import com.ibm.wala.ipa.slicer.Slicer;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.Slicer.ControlDependenceOptions;
import com.ibm.wala.ipa.slicer.Slicer.DataDependenceOptions;
import com.ibm.wala.util.collections.Pair;
import com.ibm.wala.util.graph.Graph;

import edu.berkeley.cs.bodik.svelte.cislicer.ThinSlicer;
import edu.berkeley.cs.bodik.svelte.experimental.csslicer.DepthSlicer;
import edu.berkeley.cs.bodik.svelte.plugin.SliceEnvironment;
import edu.berkeley.cs.bodik.svelte.slicers.AbstractSliceEnvironment;

public class Slicing {
	public enum SliceType {
		CI_THIN_SLICE, CS_THIN_SLICE, CS_THICK_SLICE
	};

	public static final int CI_THIN_SLICE = 1;
	public static final int CS_THIN_SLICE = 2;
	public static final int CS_THICK_SLICE = 3;

	static final boolean DEBUG_WRITE_SDGS = true;

	public static Slice doSlice(SliceType slicerType, AbstractSliceEnvironment se, Slice seed) {
		return doSlice(slicerType, se, seed, -1);
	}

	public static Slice doSlice(SliceType slicerType, AbstractSliceEnvironment se, Slice seed, int depth) {
		Collection<Statement> slice = null;
		CallGraph cg = se.getCallGraph();
		PointerAnalysis pa = se.getPointerAnalysis();
		ThinSlicer thinslicer = null;
		Graph<Statement> csDepGraph = null;

		System.out.println("-------------------- S E E D ------------------------");
		for (Statement s : seed)
			System.out.println(s);
		System.out.println("-------------------- S E E D ------------------------");

		if (slicerType == SliceType.CI_THIN_SLICE) {

			thinslicer = new ThinSlicer(cg, pa);
			slice = thinslicer.computeBackwardThinSlice(seed.getStatements(), depth);

		} else if (slicerType == SliceType.CS_THIN_SLICE) {
			DataDependenceOptions dOptions = DataDependenceOptions.NO_BASE_PTRS;
			ControlDependenceOptions cOptions = ControlDependenceOptions.NONE;
			SDG sdg = new SDG(cg, pa, new AstJavaModRef(), dOptions, cOptions, null);

			try {
				Pair<Collection<Statement>, Graph<Statement>> pair = DepthSlicer
						.computeBackwardsSliceAndDepGraph(sdg, seed.getStatements(), depth);
				slice = pair.fst;
				csDepGraph = pair.snd;
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CancelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			DataDependenceOptions dOptions = DataDependenceOptions.FULL;
			ControlDependenceOptions cOptions = ControlDependenceOptions.FULL;
			SDG sdg = new SDG(cg, pa, new AstJavaModRef(), dOptions, cOptions, null);

			try {
				slice = Slicer.computeBackwardSlice(sdg, seed.getStatements());
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (CancelException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if (slice == null)
			return null;

		Slice sliceslice = new Slice(slice, se);

		// TODO: FIXME: still pretty much a hack!
		SliceEnvironment.setCurrentSliceData(se, seed, sliceslice, thinslicer, csDepGraph);

		return sliceslice;
	}
}
