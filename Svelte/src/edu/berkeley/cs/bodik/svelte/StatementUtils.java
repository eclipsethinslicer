/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte;

import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.ShrikeBTMethod;
import com.ibm.wala.ipa.slicer.ExceptionalReturnCaller;
import com.ibm.wala.ipa.slicer.NormalReturnCaller;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.ParamCaller;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ipa.slicer.StatementWithInstructionIndex;
import com.ibm.wala.ipa.slicer.HeapStatement.HeapReturnCaller;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.SSANewInstruction;
import com.ibm.wala.types.TypeReference;

public class StatementUtils {
	/**
	 * For Shrike ONLY!
	 * 
	 * @return -1 if no line number info available
	 */
	public static int getShrikeLineNumber(Statement s) {
		if (s instanceof NormalStatement) {
			IMethod met = s.getNode().getMethod();
			if (met instanceof ShrikeBTMethod) {
				int bcIndex;
				try {
					bcIndex = ((ShrikeBTMethod) met).getBytecodeIndex(((NormalStatement) s)
							.getInstructionIndex());
					return s.getNode().getMethod().getLineNumber(bcIndex);
				} catch (InvalidClassFileException e) {
					e.printStackTrace();
					throw new RuntimeException();
				}
			}
		}
		return -1;
	}

	public static TypeReference getNewInstructionDeclaredType(Statement s) {
		if (s instanceof NormalStatement) {
			NormalStatement ns = (NormalStatement) s;
			if (ns.getInstruction() instanceof SSANewInstruction) {
				return ((SSANewInstruction) ns.getInstruction()).getNewSite().getDeclaredType();
			}
		}
		return null;
	}

	/**
	 * If the passed-in statement is a returncaller or paramcaller, this will return the statement
	 * of the invoke instruction.
	 * 
	 * @param s
	 * @return
	 */
	public static NormalStatement getCallerStatement(Statement s) {
		NormalStatement scall = null;
		// if ( s instanceof HeapStatement.ParamCaller )
		// scall = new NormalStatement(s.getNode(), ((HeapStatement.ParamCaller) s).getCallIndex());
		if (s instanceof HeapReturnCaller)
			scall = new NormalStatement(s.getNode(), ((HeapReturnCaller) s).getCallIndex());
		else if (s instanceof ParamCaller)
			scall = new NormalStatement(s.getNode(), ((StatementWithInstructionIndex) s)
					.getInstructionIndex());
		else if (s instanceof NormalReturnCaller)
			scall = new NormalStatement(s.getNode(), ((StatementWithInstructionIndex) s)
					.getInstructionIndex());
		else if (s instanceof ExceptionalReturnCaller)
			scall = new NormalStatement(s.getNode(), ((StatementWithInstructionIndex) s)
					.getInstructionIndex());
		if (scall != null && scall.getInstructionIndex() == -1)
			scall = null;
		return scall;
	}

}
