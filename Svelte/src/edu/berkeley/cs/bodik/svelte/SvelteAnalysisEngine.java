/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.jar.JarFile;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.core.runtime.IPath;
import org.eclipse.jdt.core.IClasspathEntry;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.JavaModelException;

import com.ibm.wala.cast.java.client.JavaSourceAnalysisEngine;
import com.ibm.wala.cast.java.client.JdtJavaSourceAnalysisEngine;
import com.ibm.wala.cast.java.client.impl.ZeroOneContainerCFABuilderFactory;
import com.ibm.wala.classLoader.BinaryDirectoryTreeModule;
import com.ibm.wala.classLoader.EclipseSourceFileModule;
import com.ibm.wala.classLoader.JarFileModule;
import com.ibm.wala.classLoader.Module;
import com.ibm.wala.classLoader.SourceDirectoryTreeModule;
import com.ibm.wala.classLoader.SourceFileModule;
import com.ibm.wala.core.tests.callGraph.CallGraphTestUtil;
import com.ibm.wala.eclipse.util.CancelException;
import com.ibm.wala.eclipse.util.EclipseProjectPath;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilder;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.properties.WalaProperties;
import com.ibm.wala.util.debug.Assertions;
import com.ibm.wala.util.debug.UnimplementedError;

import edu.berkeley.cs.bodik.svelte.plugin.EclipseJdtUtils;

/**
 * A SvelteAnalysisEngine simplifies the process of setting up the analysis scope and getting the
 * call graph and pointer analysis. To get a CallGraph and PointerAnalysis, add the files you wish
 * to analyze (e.g. with <code>addSourceDir()</code> and
 * <code>addJar()<code>, and define the entrypoints. You should probably
 * also add <code>setExclusionsFile()</code>. You may then <code>getCallGraph()</code> and
 * <code>getPointerAnalysis()</code>, which implicitly build the call graph if it has not
 * been build with <code>build()</code>.
 *  
 * @author evan
 *
 */
public class SvelteAnalysisEngine extends JdtJavaSourceAnalysisEngine {

	// /////////////////////////////////////////
	// /// RT JAR FINDER -- SHOULD REPLACE /////
	// /////////////////////////////////////////

	// TODO: why does this throw NullPointerException ? ...
	// TODO: simplify. all we need is rtJar, get rid of special WALA stuff
	// All this does is figure out the Java System libraries... what a waste...
	protected static String javaHomePath;
	public static List<String> rtJar;
	static {
		boolean found = false;
		try {
			rtJar = new LinkedList<String>();

			Properties p = WalaProperties.loadProperties();
			javaHomePath = p.getProperty(WalaProperties.J2SE_DIR);

			if (new File(javaHomePath).isDirectory()) {
				if ("Mac OS X".equals(System.getProperty("os.name"))) { // nick
					/**
					 * todo: {@link WalaProperties#getJ2SEJarFiles()}
					 */
					rtJar.add(javaHomePath + "/Classes/classes.jar");
					rtJar.add(javaHomePath + "/Classes/ui.jar");
				} else {
					rtJar.add(javaHomePath + File.separator + "classes.jar");
					rtJar.add(javaHomePath + File.separator + "rt.jar");
					rtJar.add(javaHomePath + File.separator + "core.jar");
					rtJar.add(javaHomePath + File.separator + "vm.jar");
				}
				found = true;
			}
		} catch (Exception e) {
			// no properties
		}

		if (!found) {
			javaHomePath = System.getProperty("java.home");
			if ("Mac OS X".equals(System.getProperty("os.name"))) { // nick
				rtJar.add(javaHomePath + "/../Classes/classes.jar");
				rtJar.add(javaHomePath + "/../Classes/ui.jar");
			} else {
				rtJar.add(javaHomePath + File.separator + "lib" + File.separator + "rt.jar");
				rtJar.add(javaHomePath + File.separator + "lib" + File.separator + "core.jar");
				rtJar.add(javaHomePath + File.separator + "lib" + File.separator + "vm.jar");
				rtJar.add(javaHomePath + File.separator + "lib" + File.separator + "classes.jar");
			}
		}
	}

	// //////////////////////////////////////////////////////
	// /// CORE -- CONTRUCTOR AND ANALSYSIS SCOPE SETUP /////
	// //////////////////////////////////////////////////////

	protected boolean built = false;
	protected CallGraph cg;

	// entry point stuff
	protected String mainClassDescriptors[] = null;

	/**
	 * Default constructor. By default, only system jars are added to analysis scope.
	 */
	public SvelteAnalysisEngine() {
		setExclusionsFile(CallGraphTestUtil.REGRESSION_EXCLUSIONS);
		// setExclusionsFile("dat/Java60RegressionExclusions.txt");
	}

	public void addRtJar() {
		for (String lib : rtJar) {
			File libFile = new File(lib);
			if (libFile.exists()) {
				try {
					addSystemModule(new JarFileModule(new JarFile(libFile)));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	// called by some WALA thing when making call graph
	@Override
	protected Iterable<Entrypoint> makeDefaultEntrypoints(AnalysisScope scope, IClassHierarchy cha) {
		if (mainClassDescriptors == null)
			throw new UnimplementedError(
					"Cannot use SvelteAnalysisEngine without defining entrypoints!");
		return Util.makeMainEntrypoints(EclipseProjectPath.SOURCE_REF, cha, mainClassDescriptors);
	}

	/**
	 * Tell the analysis engine the entrypoints for the call graph. Given a fully qualified
	 * classname, this function will look for a method
	 * <code>public static void main(String[])</code> in the classname and use that as the sole
	 * entrypoint.
	 * 
	 * @param packageAndClass
	 *            A fully qualified class name, such as "com.example.mypackage.MyClass"
	 * @return
	 */
	public void findEntrypointFromClassnameOfMain(String packageAndClass) {
		mainClassDescriptors = new String[] { "L" + packageAndClass.replace('.', '/') };
	}

	/**
	 * Tell the analysis engine the entrypoints for the call graph. Given a fully qualified
	 * classname, this function will look for a method
	 * <code>public static void main(String[])</code> in the classname and use that as the sole
	 * entrypoint.
	 * 
	 * @param packageAndClass
	 *            A fully qualified class name, such as "com.example.mypackage.MyClass"
	 * @return
	 */
	public void findEntrypointFromClassnamesOfMain(Collection<String> packagesAndClasses) {
		mainClassDescriptors = new String[packagesAndClasses.size()];
		int i = 0;
		for (String s : packagesAndClasses) {
			mainClassDescriptors[i++] = "L" + s.replace('.', '/');
		}
	}

	public boolean isBuilt() {
		return built;
	}

	public void build() {
		if (!built)
			rebuild();
	}

	public void rebuild() {
		try {
			cg = buildDefaultCallGraph();
			built = true;
		} catch (CancelException e) {
			// TODO probably should just throw this.
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO what to do here and below?
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public CallGraph getCallGraph() {
		if (!built)
			rebuild();
		return cg;
	}

	@Override
	public PointerAnalysis getPointerAnalysis() {
		if (!built)
			rebuild();
		return super.getPointerAnalysis();
	}

	// ////////////////////////////////////////////////////////
	// /// ADD SOURCE & BINARY CODE TO THE ANALYSIS SCOPE /////
	// ////////////////////////////////////////////////////////

	/**
	 * Add the specified jar file to the analysis scope with addCompiledModule()
	 * 
	 * @param path
	 * @throws IOException
	 */
	public void addJar(String path) throws IOException {
		File f = new File(path);
		if (!f.exists())
			throw new FileNotFoundException("SvelteAnalysisEnginge.addJar: Couldn't find jar "
					+ path);
		addCompiledModule(new JarFileModule(new JarFile(f)));
	}

	/**
	 * Add the specified jar file to the analysis scope with addSystemModule()
	 * 
	 * @param path
	 * @throws IOException
	 */
	public void addSystemJar(String path) throws IOException {
		File f = new File(path);
		if (!f.exists())
			throw new FileNotFoundException("SvelteAnalysisEnginge.addJar: Couldn't find jar "
					+ path);
		addSystemModule(new JarFileModule(new JarFile(f)));
	}

	/**
	 * Add the specified directory of binary .class files to the analysis scope.
	 * 
	 * @param path
	 *            Directory name should be the root of a package hierarchy, i.e.
	 *            com.example.mypackage.MyClass should be found in <code>path</code>/com/example/mypackage/MyClass.class
	 * 
	 * @throws FileNotFoundException
	 */
	public void addClassDir(String path) throws IOException {
		File f = new File(path);
		if (!f.exists())
			throw new FileNotFoundException();
		addCompiledModule(new BinaryDirectoryTreeModule(f));
	}

	/**
	 * Add the specified directory full of .java files to the analysis scope. All .java files in the
	 * directory tree will be analyzed with WALA CAst (Polyglot)
	 * 
	 * @param path
	 * @throws FileNotFoundException
	 */
	public void addJavaDir(String path) throws IOException {
		File f = new File(path);
		if (!f.exists())
			throw new FileNotFoundException();
		addSourceModule(new SourceDirectoryTreeModule(f));
	}

	public void addJavaDir(IPath path) throws IOException {
		addJavaDir(path.toOSString());
	}

	/**
	 * Add the Java directory referenced by <code>path</code> to the analysis scope, but do not
	 * include any Java files referenced in <code>exceptions</code>, referenced relatively
	 * 
	 * @param path
	 *            The path of the directory containing *.java source files.
	 * 
	 * @param exceptions
	 *            Java files to specifically not add in the analysis scope. They should by relative
	 *            paths, relative to the directory path, e.g. if I want to add
	 *            /home/evan/myproj/src, but not /home/evan/myproj/src/foo/Foo.java, an exception
	 *            string would be "foo/Foo.java"
	 * 
	 * @throws IOException
	 */
	public void addJavaDirExcept(final String path, final String exceptions[]) throws IOException {
		File f = new File(path);
		if (!f.exists())
			throw new FileNotFoundException();
		SourceDirectoryTreeModule sdtm = new SourceDirectoryTreeModule(f) {
			@Override
			protected boolean includeFile(File file) {
				if (!super.includeFile(file))
					return false;
				for (int i = 0; i < exceptions.length; i++) {
					if (file.compareTo(new File(path + exceptions[i])) == 0)
						return false;
				}
				return true;
			}
		};
		addSourceModule(sdtm);
	}

	/**
	 * Add the specified .java file to the analysis scope.
	 * 
	 * @param path
	 * @throws FileNotFoundException
	 */
	public void addJavaFile(String path) throws IOException {
		File f = new File(path);
		if (!f.exists())
			throw new FileNotFoundException();

		addSourceModule(new SourceFileModule(f, f.getName()));
	}

	/**
	 * Add the specified .jar files to the analysis scope.
	 * 
	 * @param path
	 * @throws FileNotFoundException
	 * @see addJar()
	 */
	public void addJars(String[] jarfiles) throws IOException {
		String failedjars = "";
		for (String jf : jarfiles)
			try {
				addJar(jf);
			} catch (IOException e) {
				failedjars += " " + jf;
			}
		if (!failedjars.equals(""))
			throw new FileNotFoundException("Couldn't find jars: " + failedjars);
	}

	/**
	 * Add the specified .jar files to the analysis scope.
	 * 
	 * @param path
	 * @throws FileNotFoundException
	 * @see addJar()
	 */
	public void addJars(Iterable<String> jarfiles) throws IOException {
		String failedjars = "";
		for (String jf : jarfiles)
			try {
				addJar(jf);
			} catch (IOException e) {
				failedjars += " " + jf;
			}
		if (!failedjars.equals(""))
			throw new FileNotFoundException("Couldn't find jars: " + failedjars);
	}

	// ////////////////////////////////////////////////////////////
	// /// ECLIPSE-PROJECT-BASED ADDITION OF SOURCES/BINARIES /////
	// ////////////////////////////////////////////////////////////


	protected IPath makeAbsolute(IPath p) {
		if (p.toFile().exists()) {
			return p;
		}
		return EclipseJdtUtils.getAbsolutePath(p); // workspace path
	}
	
	public void addEclipseProjectSource(IJavaProject javaProject) {
		ArrayList<IFile> files;
		try {
			files = EclipseJdtUtils.getProjectCompilationUnits(javaProject);
			for (IFile file: files)
				addSourceModule(new EclipseSourceFileModule(file));
		} catch (JavaModelException e) {
			e.printStackTrace();
			Assertions.UNREACHABLE("SvelteAnalysisEngine.addProjectSource");
		}
	}

	
	public void addClasspathEntry(IClasspathEntry cpe, boolean usePolyglot) throws IOException {
		boolean pathAbsolute = cpe.getPath().toFile().exists();

		if (cpe.getEntryKind() == IClasspathEntry.CPE_SOURCE) {
			// System.out.println("DEBUG: adding CPE_SOURCE " +
			// makeAbsolute(cpe.getPath()).toOSString());

			if ( usePolyglot )
				addJavaDir(EclipseJdtUtils.getAbsolutePath(cpe)); // done in add EclipseClassPth for jdt
		} else if (cpe.getEntryKind() == IClasspathEntry.CPE_LIBRARY) {
			// System.out.println("DEBUG: adding CPE_LIBRARY " +
			// makeAbsolute(cpe.getPath()).toOSString());
			
			File file = makeAbsolute(cpe.getPath()).toFile();
			Module m = null;
			if (file.isDirectory())
				m = new BinaryDirectoryTreeModule(file);
			else {
				m = new JarFileModule(new JarFile(file));
			}

			// libraries with absolute paths are assumed to be "system" jars
			if (pathAbsolute) {
				// System.out.println("(as system)");
				addSystemModule(m);
			} else {
				// System.out.println("(as compiled)");
				addCompiledModule(m);
			}
		} else if (cpe.getEntryKind() == IClasspathEntry.CPE_PROJECT) {
			// TODO -- add as source if not using shrike, maybe.
			// addClassDir(makeAbsolute(cpe.getPath()).toOSString()); // could
			// add as source

			IProject project = (IProject) ResourcesPlugin.getWorkspace().getRoot().findMember(
					cpe.getPath());
			addEclipseClasspaths(JavaCore.create(project), true, usePolyglot, true);
		}

	}

	public void addEclipseClasspaths(IJavaProject project, boolean useShrike) {
		addEclipseClasspaths(project, useShrike, false, false);
	}
	public void addEclipseClasspaths(IJavaProject project, boolean useShrike, boolean usePolyglot) {
		addEclipseClasspaths(project, useShrike, usePolyglot, false);
	}

	public void addEclipseClasspaths(IJavaProject project, boolean useShrike, boolean usePolyglot, boolean onlyExported) {
		if ( !useShrike && !usePolyglot )
			addEclipseProjectSource(project);
		try {
			IClasspathEntry[] classpaths = project.getResolvedClasspath(true);
			for (IClasspathEntry cpe : classpaths) {
				try {
					if (!(cpe.getEntryKind() == IClasspathEntry.CPE_SOURCE && useShrike))
						if (cpe.isExported() || !onlyExported)
							addClasspathEntry(cpe, usePolyglot);
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (useShrike)
				try {
					// System.out.println("adding binary directory " +
					// makeAbsolute(project.getOutputLocation()).toOSString());
					addClassDir(makeAbsolute(project.getOutputLocation()).toOSString());
				} catch (IOException e) {
					e.printStackTrace();
				}
		} catch (JavaModelException e) {
			e.printStackTrace();
		}

	}

	@Override
	protected CallGraphBuilder getCallGraphBuilder(IClassHierarchy cha, AnalysisOptions options,
			AnalysisCache cache) {
		return new ZeroOneContainerCFABuilderFactory().make(options, cache, cha, scope, false);
	}
}
