package edu.berkeley.cs.bodik.svelte.slicers;

import org.eclipse.core.resources.IFile;

/**
 * This interface acts as a registry entry declaring what types of files a slicer can slice as well
 * as acting as a factory to generate a <code>ISliceEnvironment</code> for a given file.
 * 
 * @author DarkWulf
 */
public interface ILanguage {
	/**
	 * Declares whether a slicer is suitable for slicing a particular file
	 * 
	 * @param file
	 * @return true if the slicer object can generate slices for the given file, false otherwise
	 */
	public boolean canSlice(IFile file);

	/**
	 * Generates a SliceEnvironment for the file
	 * 
	 * @param file
	 * @return slice environment associated with a file
	 */
	public AbstractSliceEnvironment buildEnvironment(IFile file, long modTimestamp);
}
