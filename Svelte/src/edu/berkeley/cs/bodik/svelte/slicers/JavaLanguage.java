package edu.berkeley.cs.bodik.svelte.slicers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.debug.core.DebugPlugin;
import org.eclipse.debug.core.ILaunchConfiguration;
import org.eclipse.debug.core.ILaunchManager;
import org.eclipse.jdt.core.ICompilationUnit;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jdt.core.IMethod;
import org.eclipse.jdt.core.IType;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.ASTNode;
import org.eclipse.jdt.core.dom.ArrayAccess;
import org.eclipse.jdt.core.dom.ArrayCreation;
import org.eclipse.jdt.core.dom.ArrayInitializer;
import org.eclipse.jdt.core.dom.Assignment;
import org.eclipse.jdt.core.dom.EnhancedForStatement;
import org.eclipse.jdt.core.dom.Expression;
import org.eclipse.jdt.core.dom.FieldAccess;
import org.eclipse.jdt.core.dom.IMethodBinding;
import org.eclipse.jdt.core.dom.ITypeBinding;
import org.eclipse.jdt.core.dom.InfixExpression;
import org.eclipse.jdt.core.dom.MethodDeclaration;
import org.eclipse.jdt.core.dom.MethodInvocation;
import org.eclipse.jdt.core.dom.ParenthesizedExpression;
import org.eclipse.jdt.core.dom.QualifiedName;
import org.eclipse.jdt.core.dom.SingleVariableDeclaration;
import org.eclipse.jdt.core.dom.VariableDeclarationFragment;
import org.eclipse.jface.text.BadLocationException;
import org.eclipse.jface.text.IDocument;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.progress.UIJob;
import org.eclipse.ui.texteditor.AbstractTextEditor;
import org.eclipse.ui.texteditor.IDocumentProvider;
import org.eclipse.ui.texteditor.ITextEditor;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.slicer.NormalReturnCaller;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.ParamCallee;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSAArrayStoreInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.types.TypeReference;

import edu.berkeley.cs.bodik.svelte.CGNodeUtils;
import edu.berkeley.cs.bodik.svelte.CallGraphUtils;
import edu.berkeley.cs.bodik.svelte.Slice;
import edu.berkeley.cs.bodik.svelte.Slicing;
import edu.berkeley.cs.bodik.svelte.SourcePosition;
import edu.berkeley.cs.bodik.svelte.SvelteAnalysisEngine;
import edu.berkeley.cs.bodik.svelte.Slicing.SliceType;
import edu.berkeley.cs.bodik.svelte.plugin.EclipseJdtUtils;
import edu.berkeley.cs.bodik.svelte.plugin.EclipseUtils;
import edu.berkeley.cs.bodik.svelte.plugin.SveltePlugin;

/**
 * A slicer that handles Java code by using CAST's Java support
 * 
 * @author DarkWulf
 */
public class JavaLanguage implements ILanguage {
	public static boolean useShrike = false;

	/**
	 * 
	 * @author DarkWulf
	 */
	private class JavaSliceEnvironment extends AbstractSliceEnvironment {
		private IJavaProject javaProject;
		private SvelteAnalysisEngine se = new SvelteAnalysisEngine();

		private JavaSliceEnvironment(IFile file, long modTimeStamp) {
			super(file, modTimeStamp);
			this.javaProject = ((ICompilationUnit) JavaCore.create(file)).getJavaProject();

			// initialize the SvelteAnalysisEngine with appropriate entry points
			this.se.addEclipseClasspaths(this.javaProject, useShrike);

			List<String> mainClasses = findLauncherMainClasses(javaProject);

			// if no launchers for project, try open editor's className anyway.
			// the user may get the
			// idea...
			String fileClassname = this.getClassName(file);
			if ((!mainClasses.contains(fileClassname)) && EclipseJdtUtils.fileHasMain(file))
				mainClasses.add(fileClassname);

			se.findEntrypointFromClassnamesOfMain(mainClasses);
		}

		/**
		 * Retrieves the class name of a given file.
		 * 
		 * Since this ignores position we will never get an inner class from this, so the code
		 * you're looking for may be in another class file.
		 * 
		 * @param file
		 * @return the fully qualified class name for the file
		 */
		private String getClassName(IFile file) {
			ICompilationUnit compilationUnit = (ICompilationUnit) JavaCore.create(file);
			IType type = compilationUnit.findPrimaryType();
			return type.getFullyQualifiedName();
		}

		/**
		 * If the SourcePosition selects a formal parameter in a method declaration, return the
		 * parameter index, starting at 0. If it finds a parameter, also (should but can't since we
		 * can't get the document in this thread) sets the line numbers of the beginning of the
		 * function to make sure we can find the method in WALA's CGNode. Also uses selectAndReveal
		 * to select the whole parameter (hack) if necessary
		 * 
		 * @param position
		 * @param editor
		 * @return Parameter index of -1 if position does not correspond to a selected or
		 *         partially-selected parameter
		 */
		private int getParameterIndex(SourcePosition position, IEditorPart editor) {
			// get AST
			ICompilationUnit cu = (ICompilationUnit) JavaCore.create(position.getFile());
			ASTNode root = EclipseUtils.getRootNode(cu);

			// get covering node
			ASTNode covering = EclipseUtils.getCoveringNode(root, position.offsetStart,
					position.offsetEnd - position.offsetStart);

			// ascend the tree until we find a method.
			ASTNode iter = covering;
			while (iter != null) {
				if (iter.getNodeType() == ASTNode.SINGLE_VARIABLE_DECLARATION) {
					SingleVariableDeclaration svd = (SingleVariableDeclaration) iter;
					if ( svd.getParent().getNodeType() != ASTNode.METHOD_DECLARATION )
						return -1; // could be an enhanced for loop or something else
					
					MethodDeclaration md = (MethodDeclaration) svd.getParent();

					// set position's line number
					IDocumentProvider prov = ((ITextEditor) editor).getDocumentProvider();
					IEditorInput input = editor.getEditorInput();
					IDocument document = prov.getDocument(input);
					try {
						position.lineStart = position.lineEnd = document.getLineOfOffset(md
								.getBody().getStartPosition()) + 1;
					} catch (BadLocationException e) {
						e.printStackTrace();
					}

					selectAndRevealInUIThread(iter.getStartPosition(), iter.getLength());
					return md.parameters().indexOf(svd);
				}
				iter = iter.getParent();
			}

			return -1;
		}

		@Override
		public CallGraph getCallGraph() {
			return se.getCallGraph();
		}

		@Override
		public PointerAnalysis getPointerAnalysis() {
			return se.getPointerAnalysis();
		}

		/**
		 * Look at the Java application launchers for a given project and determine all main
		 * classes.
		 * 
		 * @param project
		 * @return List<String> where each element is the fully qualified name of each entrypoint
		 *         listed in the project's launchers
		 */
		public List<String> findLauncherMainClasses(IJavaProject project) {
			ILaunchManager manager = DebugPlugin.getDefault().getLaunchManager();
			List<String> classes = new LinkedList<String>();
			try {
				ILaunchConfiguration configs[] = manager.getLaunchConfigurations();
				for (ILaunchConfiguration cfg : configs) {
					if (cfg.getType().getIdentifier().equals(
							"org.eclipse.jdt.launching.localJavaApplication")
							&& cfg.getAttribute("org.eclipse.jdt.launching.PROJECT_ATTR", "")
									.equals(project.getElementName())) {
						String classWithMain = cfg.getAttribute("org.eclipse.jdt.launching.MAIN_TYPE",(String) null);
						IType classWithMainType = project.findType(classWithMain);
						// check if it really has a main method. TODO: this only works for source. maybe take this out.
						if ( classWithMainType != null ) {
							for ( IMethod met: classWithMainType.getMethods() )
								if ( met.getElementName().equals("main") && met.getParameterTypes().length == 1
										&& (met.getParameterTypes()[0].equals("[QString;")
												|| met.getParameterTypes()[0].equals("[Qjava.lang.String;"))) {
									classes.add(classWithMain);
									break;
								}
						}
					}
				}
			} catch (CoreException e) {
				e.printStackTrace();
			}
			return classes;
		}

		@Override
		public Slice getSlice(SliceType slicerType, int depth, Collection<Statement> seed) {

			if (seed.size() == 0)
				System.err.println("ERROR: slice is size 0");

			Slice s = Slicing.doSlice(slicerType, this, new Slice(seed, this), depth);

			s.dump();

			s.prune(true);
			return s;
		}

		@Override
		public Collection<Statement> generateSeed(SourcePosition position, IEditorPart editor) {
			CallGraph cg = getCallGraph();
			IFile file = position.getFile();

			if ( cg == null ) {
				EclipseUtils.errorMsgInUIThread("Could not build call graph",
					"Could not build call graph, probably due to an internal error. Check console output for more info.");
				return null;
			}
			
			CGNode cgn = null;

			// check if we have selected a parameter.
			// do this first, in changes position so we can find the method (if the declaration
			// spans multiple lines)
			int paramIndex = getParameterIndex(position, editor);
			int linenum = position.lineStart; // selection.getStartLine() + 1;

			Collection<Statement> result;

			if (useShrike) {
				// Shrike-loaded class don't have source file information. Compare based on
				// fully-qualified class name.
				// TODO: this doesn't find correct class name for an inner class.
				// We have to use the JDT AST to see what the innermost class/interface is.
				String mainClassName = this.getClassName(file);

				cgn = CallGraphUtils.findMethodIncludingLineNumInClass(cg, linenum, mainClassName);
			} else {
				cgn = CallGraphUtils.findMethodIncludingLineNum(cg, linenum, file.getLocation()
						.toOSString());
			}
			
			if ( cgn == null ) {
				EclipseUtils.errorMsgInUIThread("Could not find method",
				"Could not find a method containing the line number of code you have selected. It may be that:\n1) You have selected something outside of a method \n2) The source file is out-of-date and needs to be saved\n3) The method is not reachable from the program entrypoints");
				return null;
			}

			if (paramIndex != -1) { // slice from formal parameter
				int valueNumber = paramIndex + 1; // paramIndex starts at zero
				if (!cgn.getMethod().isStatic())
					valueNumber++; // v1 = this
				result = Collections.singleton((Statement) new ParamCallee(cgn, valueNumber));
			} else if (useShrike) // all statements from line number
				return CGNodeUtils.getStatementsFromLineNumber(cgn, linenum);
			else { // slice from selection
				result = reduceSubstatement(position,cgn);
				if ( result.size() > 0 ) {
					Object first = result.iterator().next();
					if ( first instanceof NormalStatement && ((NormalStatement)first).getInstruction() == null ) {
						EclipseUtils.errorMsgInUIThread("Could not find seed",
						"The statement you have selected as a seed is null in the intermediate representation. This probably means that the statement has been optimized away, as in the case of simple assignment statements (\"x=y\"). Please choose another statement where the value in the assignement statement originated from.");
						return null;
					}
				}
			}
			

			// swap out invoke instructions for their return values
			ArrayList<Statement> newSeed = new ArrayList<Statement>();
			for (Statement s : result)
				if (s instanceof NormalStatement
						&& ((NormalStatement) s).getInstruction() instanceof SSAAbstractInvokeInstruction
						&& ((SSAAbstractInvokeInstruction) ((NormalStatement) s).getInstruction())
								.getDeclaredResultType() != TypeReference.Void)
					newSeed.add(new NormalReturnCaller(s.getNode(), ((NormalStatement) s)
							.getInstructionIndex()));
				else
					newSeed.add(s);

			if (result == null || newSeed.size() == 0 )
					EclipseUtils.errorMsgInUIThread("Could not find seed",
										"Could not find a suitable seed to slice from. It may be that:\n1) You have selected something that evaluates to a constant\n2) The statement selected has been optimized out (i.e., simple assignment statements)\n3) The source file is out-of-date and needs to be saved");

			return newSeed;
		}

		/**
		 * Some variables may not have statements at that line, e.g. assume foo, a, b, c, and d are
		 * locals: Then they will not have defining statements in 'foo.bar(a[b],c+d)', but their
		 * value numbers are used directly.
		 * 
		 * Given a source position, if a simple name is selected, this function changes position
		 * to the the statement that uses the simple name and returns the index of the use.
		 * 
		 * For example, let's say the user selects 'b' above: then position will be changed to the position
		 * of 'a[b]' and the return value will be 1 (i.e. we should look at use 1 in the WALA arrayload instruction).
		 * 
		 * To accomplish this, we look at the context of the node referenced by position - i.e., the type of its parent node
		 * and its location in the parent node.
		 * 
		 * @param position
		 * This variable is changed to the match parent node --the instruction that we should find in the IR,
		 * using the return value as the index into the uses of this instruction
		 * @param cgn 
		 * @return
		 */
		private Collection<Statement> reduceSubstatement(SourcePosition position, CGNode cgn) {
			int substatementIndex = -1;
			Class<? extends SSAInstruction> parentInstructionClass = null;

			// get AST
			ICompilationUnit cu = (ICompilationUnit) JavaCore.create(position.getFile());
			ASTNode root = EclipseUtils.getRootNodeWithBindings(cu);

			// get covering node
			ASTNode covering = EclipseUtils.getCoveringNode(root, position.offsetStart,
					position.offsetEnd - position.offsetStart);

			// walk down any parentheses to get to the real expression to check if it is a
			// simplename
			while (covering.getNodeType() == ASTNode.PARENTHESIZED_EXPRESSION)
				covering = ((ParenthesizedExpression) covering).getExpression();

			if (covering.getNodeType() == ASTNode.SIMPLE_NAME) {
				final ASTNode toSelect = covering;

				// get rid of any parentheses above, so the next parent will be the actual parent.
				// e.g. "(((foo))).bar" -> get to the outermost paren so parent will be
				// qualifedexpression
				while (covering.getParent().getNodeType() == ASTNode.PARENTHESIZED_EXPRESSION)
					covering = covering.getParent();
		
				ASTNode parent = covering.getParent();
				if (parent.getNodeType() == ASTNode.ARRAY_INITIALIZER) {
					ArrayInitializer ai = (ArrayInitializer) covering.getParent();
					substatementIndex = 0;
					for ( Object o: ai.expressions() ) {
						if ( o.equals(covering) ) {
							break;
						}
						substatementIndex++;
					}
					
					return CGNodeUtils.getNthArrayStoreStatementFromOffset(cgn,
							parent.getStartPosition(), parent.getStartPosition()+parent.getLength(),
							substatementIndex, SSAArrayStoreInstruction.class);
				} else if (parent.getNodeType() == ASTNode.ARRAY_CREATION) {
					ArrayCreation ac = (ArrayCreation) covering.getParent();
					substatementIndex = ac.dimensions().indexOf(covering);
					if (substatementIndex != -1) {
						position.offsetStart = ac.getStartPosition();
						position.offsetEnd = ac.getStartPosition() + ac.getLength();
					}
				} else if (parent.getNodeType() == ASTNode.INFIX_EXPRESSION) {
					InfixExpression ie = (InfixExpression) covering.getParent();
					if ( covering == ie.getLeftOperand() ) {
						substatementIndex = 0;
						// don't look at extended operands, if there are any.
						// also, JDT conversion uses start of left operand, not start of infix expression (difference?)
						position.offsetStart = ie.getLeftOperand().getStartPosition();
						position.offsetEnd = ie.getRightOperand().getStartPosition() + ie.getRightOperand().getLength();
					} else if ( covering == ie.getRightOperand() ) {
						substatementIndex = 1;
						position.offsetStart = ie.getLeftOperand().getStartPosition();
						position.offsetEnd = ie.getRightOperand().getStartPosition() + ie.getRightOperand().getLength();
					} else {
						// extended operand
						substatementIndex = 1; // it will always be the second operand, since we add operands to the right hand side
						// the trick is determining the position: from the start of the first expression to the end of this expression
						position.offsetStart = ie.getLeftOperand().getStartPosition();
						// position.offsetEnd same
					}
				} else if (parent.getNodeType() == ASTNode.METHOD_INVOCATION) {
					MethodInvocation mi = (MethodInvocation) covering.getParent();
					position.offsetStart = mi.getStartPosition();
					position.offsetEnd = mi.getStartPosition() + mi.getLength();
					
					// if varargs, args are boxed first, so we have to deal with that.
					IMethodBinding binding = mi.resolveMethodBinding().getMethodDeclaration();
					if ( binding.isVarargs() && mi.getExpression() != covering ) {
						int nActuals = mi.arguments().size();
						int nFormals = binding.getParameterTypes().length;
						
						// if the nActuals != nFormals, it's definitely a varargs invocation. If
						// nActuals == nFormals, however, we have to check the type of the last argument.
						// if the last argument is an array type of the correct type, it will be passed in and not boxed.
					    ITypeBinding lastArgType = null;
					    if ( nActuals > 0 && mi.arguments().get(nActuals-1) instanceof Expression ) // if nActuals=0 then it's definitely varargs and nFormals != 0
					      lastArgType = ((Expression)mi.arguments().get(nActuals-1)).resolveTypeBinding();

						if ( nActuals != nFormals || lastArgType.isSubTypeCompatible(binding.getParameterTypes()[nFormals-1]) ) {
							int argIndex = mi.arguments().indexOf(covering);
							
							if ( argIndex >= (nFormals-1) ) {
								int indexWithinVarargsArray = argIndex - (nFormals-1);
								return CGNodeUtils.getNthArrayStoreStatementFromOffset(cgn,
										parent.getStartPosition(), parent.getStartPosition() + parent.getLength(),
										indexWithinVarargsArray, SSAArrayStoreInstruction.class);
								// argIndex - nFormals is 
							}
							// ELSE:
							// one of the previous, regular args in a varargs invocation.
							// be very sure to look at invocation instructions only (don't look at the new/array store instructions before it) 
						}
					}
					parentInstructionClass = SSAAbstractInvokeInstruction.class;
					
					if (mi.getExpression() == covering)
						substatementIndex = 0;
					else {
						int argIndex = mi.arguments().indexOf(covering);
						if (argIndex != -1) {
							substatementIndex = argIndex;
							if (mi.getExpression() != null)
								substatementIndex++; // result = argIndex + 1 "foo.bar(a,b,c)" -> a is index 1;
							// 	"bar(a,b,c)" -> a is index 0
						}
					}
				} else if (parent.getNodeType() == ASTNode.ARRAY_ACCESS) {
					ArrayAccess aa = (ArrayAccess) covering.getParent();
					if (parent.getParent().getNodeType() == ASTNode.ASSIGNMENT) {
						// case 1: "a[b] = c" -> WALA statement covers whole thing (putfield) EXCEPT
						// PARENTHESES (due to WALA/polyglot bug)!
						position.offsetStart = parent.getParent().getStartPosition();
						position.offsetEnd = parent.getParent().getStartPosition()
								+ parent.getParent().getLength();
					} else {
						// case 2: "a[b].c = d" (select a) or "a[b]"
						// covers just a.b
						position.offsetStart = parent.getStartPosition();
						position.offsetEnd = parent.getStartPosition() + parent.getLength();
					}

					if (aa.getArray() == covering)
						substatementIndex = 0;
					if (aa.getIndex() == covering)
						substatementIndex = 1;
				} else if (parent.getNodeType() == ASTNode.QUALIFIED_NAME
						|| parent.getNodeType() == ASTNode.FIELD_ACCESS) {
					// it will be a FIELD_ACCESS in the case if parentheses: (foo).bar = 5
					// it will be a QUALIFIED_NAME otherwise: foo.bar = 5
					ASTNode qualifier = (parent.getNodeType() == ASTNode.QUALIFIED_NAME) ? ((QualifiedName) parent)
							.getQualifier()
							: ((FieldAccess) parent).getExpression();
					if (qualifier == covering) {
						if (parent.getParent().getNodeType() == ASTNode.ASSIGNMENT &&
								((Assignment)parent.getParent()).getLeftHandSide().equals(parent) ) {
							// case 1: "a.b = c" -> WALA statement covers whole thing (putfield)
							// EXCEPT PARENTHESES (due to WALA/polyglot bug)!
							position.offsetStart = parent.getParent().getStartPosition();
							position.offsetEnd = parent.getParent().getStartPosition()
									+ parent.getParent().getLength();
						} else {
							// case 2: "a.b.c = d" (select a) or "a.b" -- getfield, WALA statement
							// or foo = a.b
							// covers just a.b
							position.offsetStart = parent.getStartPosition();
							position.offsetEnd = parent.getStartPosition() + parent.getLength();
						}
						substatementIndex = 0;
					}
				} else if ( parent.getNodeType() == ASTNode.ASSIGNMENT || parent.getNodeType() == ASTNode.VARIABLE_DECLARATION_FRAGMENT ) {
					Expression lhs, rhs;
					if ( parent.getNodeType() == ASTNode.ASSIGNMENT) {
						Assignment assign = (Assignment) parent;
						lhs = assign.getLeftHandSide();
						rhs = assign.getRightHandSide();
					} else {
						VariableDeclarationFragment vdf = (VariableDeclarationFragment) parent;
						lhs = vdf.getName();
						rhs = vdf.getInitializer();
					}

					if ( lhs == covering && rhs != null) {
						// slice on value of right hand side 
						// go past parentheses to get to the real value
						while ( rhs.getNodeType() == ASTNode.PARENTHESIZED_EXPRESSION )
							rhs = ((ParenthesizedExpression)rhs).getExpression();
						position.offsetStart = rhs.getStartPosition();
						position.offsetEnd = rhs.getStartPosition() + rhs.getLength();
					}
					// else
					// right hand side, give warning
					
				} else if ( parent.getNodeType() == ASTNode.ENHANCED_FOR_STATEMENT ) {
					EnhancedForStatement efs = (EnhancedForStatement) parent;
					if ( covering == efs.getExpression() ) {
						substatementIndex = 0;
						position.offsetStart = efs.getStartPosition();
						position.offsetEnd = efs.getStartPosition() + efs.getLength();
					}
				}
				// e.g. for ( Object f: foo ) 
				// the first instruction (in the case of a simple name in the expression) is arraylength on foo for array-type, invoke "foo.iterator()" for iterable.   

				if (substatementIndex != -1) {
					// selectAndReveal the actual simple name token to clarify what we are slicing
					// on. this is kind of a hack
					selectAndRevealInUIThread(toSelect.getStartPosition(), toSelect.getLength());
					return CGNodeUtils.getStatementFromOffsetAndSubstatementIndex(cgn,
							position.offsetStart, position.offsetEnd, substatementIndex,
							parentInstructionClass);

				}


			}

			return CGNodeUtils.getInnermostStatementFromOffset(cgn, position.offsetStart,
					position.offsetEnd);

			// System.out.println(covering.getParent().getNodeType());
			
			// if ( covering.getNodeType() == ASTNode.)
			// if covering node is a simple identifier
			// check parent type:
			// if the parent is a binop, getfield, putfield, invokevirtual, invokestatic, array
			// load/store, new Bla[x][y],...
			// get the "index".
			// it would be nice to add more safety here in case JDT AST & WALA statements don't
			// match up exactly
			// return 0;
		}

		private void selectAndRevealInUIThread(final int startPosition, final int length) {
			new UIJob("hello") {
				@Override
				public IStatus runInUIThread(IProgressMonitor monitor) {
					((AbstractTextEditor) (PlatformUI.getWorkbench().getActiveWorkbenchWindow()
							.getActivePage().getActiveEditor())).selectAndReveal(startPosition,
							length);
					return new Status(IStatus.OK, SveltePlugin.PLUGIN_ID, null);
				}
			}.schedule();
		}

		@Override
		public SourcePosition statementToPosition(Statement statement) {
			return CGNodeUtils.getSourcePositionOfInstructionIndex(statement.getNode(),
					((NormalStatement) statement).getInstructionIndex(), this.javaProject);
		}
	}

	@Override
	public AbstractSliceEnvironment buildEnvironment(IFile file, long modTimestamp) {
		return new JavaSliceEnvironment(file, modTimestamp);
	}

	/**
	 * JavaSlicer can handle files with the .java extension
	 */
	@Override
	public boolean canSlice(IFile file) {
		return "java".equalsIgnoreCase(file.getFileExtension());
	}
}
