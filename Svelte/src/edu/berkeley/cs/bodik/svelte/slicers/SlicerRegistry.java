package edu.berkeley.cs.bodik.svelte.slicers;

import java.util.LinkedList;
import java.util.List;

import org.eclipse.core.resources.IFile;

/**
 * A registry of slicers available, can query the registry to retrieve a slicer, from which most
 * important data structures can then be built
 * 
 * @author DarkWulf
 */
public class SlicerRegistry {
	private static SlicerRegistry instance = new SlicerRegistry();

	public static SlicerRegistry getInstance() {
		return instance;
	}

	private List<ILanguage> slicers = new LinkedList<ILanguage>();

	private SlicerRegistry() {
		// hardcode available slicers for now
		registerType(new JavaLanguage());
	}

	/**
	 * 
	 * @param slicer
	 */
	public void registerType(ILanguage slicer) {
		slicers.add(slicer);
	}

	/**
	 * Retrieves the slicer associated with a particular file
	 * 
	 * @param file
	 *            the file to check against slicer registry
	 * @return ISlicer capable of slicing the file, or null if no matching slicer was found
	 */
	public ILanguage getSlicer(IFile file) {
		for (ILanguage slicer : slicers) {
			if (slicer.canSlice(file))
				return slicer;
		}
		return null;
	}

	/**
	 * Retrieves the slice environment associated with a particular file
	 * 
	 * @param file
	 *            the file to check against slicer registry
	 * @return ISliceEnvironment encapsulating all that is needed to generate slices from a file
	 *         given a statement of interest
	 */
	public AbstractSliceEnvironment getEnvironment(IFile file, long modTimestamp) {
		ILanguage slicer = getSlicer(file);
		if (slicer == null)
			return null;
		return slicer.buildEnvironment(file, modTimestamp);
	}
}
