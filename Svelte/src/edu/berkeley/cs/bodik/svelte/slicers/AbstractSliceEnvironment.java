package edu.berkeley.cs.bodik.svelte.slicers;

import java.util.Collection;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.ui.IEditorPart;

import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.slicer.Statement;

import edu.berkeley.cs.bodik.svelte.Slice;
import edu.berkeley.cs.bodik.svelte.SourcePosition;
import edu.berkeley.cs.bodik.svelte.Slicing.SliceType;

/**
 * Builds a slice environment auto-magically for a given file, encapsulates the information needed
 * to generate slices and all that fun stuff. (Basically needs to know about any language-specific
 * build dependencies or "classpath" equivalents)
 * 
 * @author DarkWulf
 */
public abstract class AbstractSliceEnvironment {

	private long modTimestamp;
	private IProject project;
	
	public AbstractSliceEnvironment(IFile file, long modTimestamp) {
		this.modTimestamp = modTimestamp;
		project = file.getProject();
	}
	
	public IProject getProject() {
		return project;
	}
	
	public long getModTimestamp() {
		return modTimestamp;
	}
	
	public abstract CallGraph getCallGraph();

	public abstract PointerAnalysis getPointerAnalysis();

	public abstract Slice getSlice(SliceType slicerType, int depth, Collection<Statement> seed);

	/**
	 * Generates a seed based on a source range in an editor
	 * 
	 * @param position
	 * @param editor 
	 * @return a collection of seed statements from which slices can be run
	 */
	public abstract Collection<Statement> generateSeed(SourcePosition position, IEditorPart editor);

	/**
	 * 
	 * @param statement
	 * @return
	 */
	public abstract SourcePosition statementToPosition(Statement statement);
}
