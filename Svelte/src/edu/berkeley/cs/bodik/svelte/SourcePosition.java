/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte;

import org.eclipse.core.resources.IFile;
import org.eclipse.jdt.core.IJavaProject;
import org.eclipse.jface.text.ITextSelection;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IFileEditorInput;

import edu.berkeley.cs.bodik.svelte.plugin.EclipseJdtUtils;
import edu.berkeley.cs.bodik.svelte.plugin.EclipseUtils;

/**
 * A SourcePosition represents a position in a file -- the start line number, start column number,
 * end line number, and end column number.
 * 
 * If the filename cannot be determined, we can pass the buck and pass in a fully qualified class
 * name, like Lcom/example/mypackage/MyClass
 * 
 * @author evan
 * 
 */
public class SourcePosition {

	// line numbers are WALA line numbers, starting from one.
	public int lineStart, lineEnd, offsetStart, offsetEnd;
	public String filename;

	public SourcePosition(int lineStart, int lineEnd, int offsetStart, int offsetEnd,
			String filename) {
		this.lineStart = lineStart;
		this.lineEnd = lineEnd;
		this.offsetStart = offsetStart;
		this.offsetEnd = offsetEnd;
		this.filename = filename;
	}

	public static SourcePosition makeSourcePosition(int lineStart, int lineEnd, int offsetStart,
			int offsetEnd, String classname, IJavaProject project) {
		return new SourcePosition(lineStart, lineEnd, offsetStart, offsetEnd,
				project == null ? null : EclipseJdtUtils.findSourceFileForClassStripL(classname, project));
	}

	public static SourcePosition makeSourcePosition(IEditorPart editor, ITextSelection selection) {
		int offsetEnd = selection.getOffset() + selection.getLength();

		IEditorInput editorInput = editor.getEditorInput();
		IFile file = ((IFileEditorInput) editorInput).getFile();
		// eclipse line numbers start from zero, add one.
		return new SourcePosition(selection.getStartLine() + 1, selection.getEndLine() + 1,
				selection.getOffset(), offsetEnd, file.getLocation().toOSString());
	}

	@Override
	public String toString() {
		return "Line " + lineStart + ", offset " + offsetStart + " - line " + lineEnd + ", offset "
				+ offsetEnd + " (file: " + filename + ")";
	}

	public IFile getFile() {
		return EclipseUtils.findIFileForFilename(filename);
	}

}
