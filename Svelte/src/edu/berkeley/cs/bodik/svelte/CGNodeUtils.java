/*******************************************************************************
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html.
 * 
 * This file is a derivative of code released by the University of
 * California under the terms listed below.  
 *
 * Refinement Analysis Tools is Copyright ©2007 The Regents of the
 * University of California (Regents). Provided that this notice and
 * the following two paragraphs are included in any distribution of
 * Refinement Analysis Tools or its derivative work, Regents agrees
 * not to assert any of Regents' copyright rights in Refinement
 * Analysis Tools against recipient for recipients reproduction,
 * preparation of derivative works, public display, public
 * performance, distribution or sublicensing of Refinement Analysis
 * Tools and derivative works, in source code and object code form.
 * This agreement not to assert does not confer, by implication,
 * estoppel, or otherwise any license or rights in any intellectual
 * property of Regents, including, but not limited to, any patents
 * of Regents or Regents employees.
 * 
 * IN NO EVENT SHALL REGENTS BE LIABLE TO ANY PARTY FOR DIRECT,
 * INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES,
 * INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE
 * AND ITS DOCUMENTATION, EVEN IF REGENTS HAS BEEN ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *   
 * REGENTS SPECIFICALLY DISCLAIMS ANY WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE AND FURTHER DISCLAIMS ANY STATUTORY
 * WARRANTY OF NON-INFRINGEMENT. THE SOFTWARE AND ACCOMPANYING
 * DOCUMENTATION, IF ANY, PROVIDED HEREUNDER IS PROVIDED "AS
 * IS". REGENTS HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT,
 * UPDATES, ENHANCEMENTS, OR MODIFICATIONS.
 */
package edu.berkeley.cs.bodik.svelte;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import org.eclipse.jdt.core.IJavaProject;

import com.ibm.wala.cast.java.loader.JavaSourceLoaderImpl.ConcreteJavaMethod;
import com.ibm.wala.cast.tree.CAstSourcePositionMap.Position;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.ShrikeBTMethod;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.slicer.NormalStatement;
import com.ibm.wala.ipa.slicer.ParamCallee;
import com.ibm.wala.ipa.slicer.PhiStatement;
import com.ibm.wala.ipa.slicer.Statement;
import com.ibm.wala.shrikeCT.InvalidClassFileException;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.ssa.SSAInvokeInstruction;
import com.ibm.wala.ssa.SSAPhiInstruction;
import com.ibm.wala.util.debug.Assertions;
import com.ibm.wala.util.intset.IntSet;

/**
 * Utilities dealing with Call Graph Nodes (which are essentially methods) These would be handy in
 * the CGNode itself. Most of these are only useful for methods coming from a real .java or .class
 * file we are analyzing.
 * 
 * @author evan
 * 
 */
public class CGNodeUtils {

	/**
	 * Do a brute-force search thru the statements of the Call Graph and find the lexically first
	 * statement which calls a method called <code>methodName</code>
	 * 
	 * @param n
	 *            The method with code to search through.
	 * @param methodName
	 *            The method name, irrespective of class it is defined in, whether it is static,
	 *            etc.
	 * @return The statement found (i.e. an invokestatic or invokevirtual statement), or null if
	 *         none found.
	 */
	public static Statement findCallTo(CGNode n, String methodName) {
		IR ir = n.getIR();
		for (Iterator<SSAInstruction> it = ir.iterateAllInstructions(); it.hasNext();) {
			SSAInstruction s = it.next();
			if (s instanceof SSAInvokeInstruction) {
				SSAInvokeInstruction call = (SSAInvokeInstruction) s;
				if (call.getCallSite().getDeclaredTarget().getName().toString().equals(methodName)) {
					IntSet indices = ir.getCallInstructionIndices(((SSAInvokeInstruction) s)
							.getCallSite());
					Assertions.productionAssertion(indices.size() == 1, "expected 1 but got "
							+ indices.size());
					return new NormalStatement(n, indices.intIterator().next());
				}
			}
		}
		Assertions.UNREACHABLE("failed to find call to " + methodName + " in " + n);
		return null;
	}

	/**
	 * Find a particular SSA instruction in the method's instructions (by a simple brute force
	 * search), and create a NormalStatement from this. A NormalStatement contains the CGNode and
	 * the index of the instruction in the method's instructions.
	 * 
	 * @param node
	 * @param instr
	 * @return A NormalStatement or PhiStatement representing the instruction, or <code>null</code> if the
	 *         instruction was not found in the method.
	 */
	public static Statement findStatementFromSSAInstruction(CGNode node, SSAInstruction instr) {
	  if ( instr instanceof SSAPhiInstruction ) {
	    return new PhiStatement(node, (SSAPhiInstruction)instr);
	  }
	  SSAInstruction instrs[] = node.getIR().getInstructions();
		for (int i = 0; i < instrs.length; i++) {
			if (instrs[i] == instr)
				return new NormalStatement(node, i);
		}
		return null;
	}

	/**
	 * Given a CGNode and a line number, does a brute force search for statements in the CGNode
	 * which have that line number in the given source code. There's probably a better way to do
	 * this. If not, this should at least be a binary search. Note that you have to have the CGNode
	 * for the method to do this first off. Returns the FIRST statement only (until I can figure out
	 * how to use Set)
	 * 
	 * @param node
	 *            The method containing statements to look thru.
	 * 
	 * @param linenumber
	 *            The original source line number to look for.
	 */
	public static Statement getStatementFromLineNumber(CGNode node, int linenumber) {
		SSAInstruction[] instrs = node.getIR().getInstructions();
		for (int i = 0; i < instrs.length; i++) {
			if (((ConcreteJavaMethod) node.getMethod()).getLineNumber(i) == linenumber)
				return new NormalStatement(node, i);
		}
		return null;
	}

	/**
	 * Given a CGNode and a line number, does a brute force search for statements in the CGNode
	 * which have that line number in the given source code. There's probably a better way to do
	 * this. If not, this should at least be a binary search. Note that you have to have the CGNode
	 * for the method to do this first off. Returns all statements.
	 * 
	 * @param node
	 *            The method containing statements to look thru.
	 * 
	 * @param linenumber
	 *            The original source line number to look for.
	 */
	public static List<Statement> getStatementsFromLineNumber(CGNode node, int linenumber) {
		ArrayList<Statement> result = new ArrayList<Statement>();
		SSAInstruction[] instrs = node.getIR().getInstructions();

		for (int i = 0; i < instrs.length; i++) {
			// FIXME: may fail for Shrike instrs on multiple lines
			// if (instrs[i] != null && ((pos = getSourcePositionOfInstructionIndex(node,i)) !=
			// null))
			// System.out.println("looking for "+linenumber+" it's not " +pos.lineStart);
			// else if (instrs[i] != null)
			// System.out.println("couldn't get pos!");

			if (instrs[i] != null && linenumber == getLineNumberOfInstructionIndex(node, i)) {
				// FIXME: instructions like conditional branches, switches, and gotos seem to
				// encompass many lines, ... adding seeds we don't really want.
				// for a conditional, we probably want the first line. for other things, this may
				// not work as well.... analyze elements, not statements?
				// linenumber >= pos.lineStart && linenumber <= pos.lineEnd
				result.add(new NormalStatement(node, i));

				// NOTE: instrs[i] could be null if the instruction
				// has been optimized out. this could be useful later on.

			}
		}
		return result;
	}

	/**
	 * Finds the line number in the original source of the specified instruction. TODO: add code for
	 * Javascript methods
	 * 
	 * @param node
	 * @param instructionIndex
	 * @return The line number (starting from 1) or -1 if not available.
	 */
	private static int getLineNumberOfInstructionIndex(CGNode node, int instructionIndex) {
		IMethod met = node.getMethod();
		if (met instanceof ConcreteJavaMethod) {
			try {
				return met.getLineNumber(instructionIndex);
			} catch (InvalidClassFileException e) {
				e.printStackTrace();
			}
		} else if (met instanceof ShrikeBTMethod) {
			int bcIndex;
			try {
				bcIndex = ((ShrikeBTMethod) met).getBytecodeIndex(instructionIndex);
				return met.getLineNumber(bcIndex);
			} catch (InvalidClassFileException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	// used only to represent start and end lines for a Shrike method.
	private static class ShrikeBTMethodPosition implements Position {
		private int firstline;
		private int lastline;

		public ShrikeBTMethodPosition(int firstline, int lastline) {
			this.firstline = firstline;
			this.lastline = lastline;
		}

		public int getFirstCol() {
			return -1;
		}

		public int getFirstLine() {
			return firstline;
		}

		public InputStream getInputStream() throws IOException {
			return null;
		}

		public int getLastCol() {
			return -1;
		}

		public int getLastLine() {
			return lastline;
		}

		public URL getURL() {
			return null;
		}

		public int compareTo(Object o) {
			return 0;
		}

		public int getFirstOffset() {
			return -1;
		}

		public int getLastOffset() {
			return -1;
		}
	}

	/**
	 * Find lexically first and last instruction in the method and return a Position with their line
	 * numbers.
	 */
	public static Position getSourcePosition(ShrikeBTMethod met) {
		try {
			int firstline = met.getLineNumber(met.getBytecodeIndex(0));
			int lastinstr = met.getInstructions().length - 1;
			int lastline = met.getLineNumber(met.getBytecodeIndex(lastinstr));
			return new ShrikeBTMethodPosition(firstline, lastline);
		} catch (InvalidClassFileException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Return a Position representing the position of the method in the original source file. In the
	 * case of Shrike methods, these will be the line numbers of the first and last instruction that
	 * show up in the IR of the method.
	 * 
	 * @param cgn
	 *            The CGNode of the method.
	 * @return The position, or <code>null</code> it the method is neither a ConcreteJavaMethod or
	 *         ShrikeBTMethod.
	 */
	public static Position getSourcePosition(CGNode cgn) {
		IMethod met = cgn.getMethod();
		if (met instanceof ConcreteJavaMethod) {
			return ((ConcreteJavaMethod) met).getSourcePosition();
		} else if (met instanceof ShrikeBTMethod) {
			return getSourcePosition((ShrikeBTMethod) met);
		} else
			return null;
	}

	/**
	 * Gets as much information as possible about where in the original source a particular IR
	 * instruction is in the method. For a method originating from CAst, this will be start line and
	 * column information, as well as an absolute path. For a method originating from Shrike, this
	 * will be a line number (first line and last line are equal), and the name of the class (the
	 * filename is looked up from the class using the project parameter)
	 * 
	 * @param cgn
	 * @param instructionIndex
	 * @param project
	 *            An IJavaProject to use to lookup the source filename for a given class, in case
	 *            the class comes from compiled Shrike.
	 * @return
	 */
	public static SourcePosition getSourcePositionOfInstructionIndex(CGNode cgn,
			int instructionIndex, IJavaProject project) {
		IMethod met = cgn.getMethod();
		if (met instanceof ConcreteJavaMethod) {
			Position pos = ((ConcreteJavaMethod) met).getSourcePosition(instructionIndex);
			if (pos == null) {
				// WARNING
				System.err.println("couldn't get source position of instruction "
						+ instructionIndex + " in " + cgn);
				return null;
			}
			return new SourcePosition(pos.getFirstLine(), pos.getLastLine(), pos.getFirstOffset(),
					pos.getLastOffset(), met.getDeclaringClass().getSourceFileName());

		} else if (met instanceof ShrikeBTMethod) {
			try {
				int bcIndex = ((ShrikeBTMethod) met).getBytecodeIndex(instructionIndex);
				int linenum = met.getLineNumber(bcIndex);
				String classname = met.getDeclaringClass().getName().toString();
				return SourcePosition.makeSourcePosition(linenum, linenum, -1, -1, classname,
						project);
			} catch (InvalidClassFileException e) {
				e.printStackTrace();
			}
		}
		return null;
	}

	public static int getIndexOfSSAInstruction(CGNode node, SSAInstruction instr) {
		SSAInstruction instrs[] = node.getIR().getInstructions();
		for (int i = 0; i < instrs.length; i++) {
			if (instrs[i] == instr)
				return i;
		}
		return -1;
	}

	public static Collection<Statement> getFormalParameterStatement(CGNode cgn, int formalParameter) {
		int vn = formalParameter + 1;
		if (!cgn.getMethod().isStatic())
			vn++; // 'this' is v1; formalParameter 0 -> value number 2
		return Collections.singleton((Statement) new ParamCallee(cgn, vn));
	}

	/**
	 * Could return a NormalStatement that has a null instruction. In that case, the
	 * instruction has probably been optimized away.
	 * 
	 * Tries to get a non-null instruction, if possible.
	 * 
	 * @param cgn
	 * @param offsetStart
	 * @param offsetEnd
	 * @param parentInstructionClass 
	 * @param klass
	 * @return
	 */
	public static Collection<Statement> getInnermostStatementFromOffset(CGNode cgn,
			int offsetStart, int offsetEnd) {
		int minCoveringLength = -1;
		int minCoveringInst = -1;
		for (int i = 0; i < cgn.getIR().getInstructions().length; i++) {
			SourcePosition si = CGNodeUtils.getSourcePositionOfInstructionIndex(cgn, i, null);
			if (si != null) {
				if (si.offsetStart <= offsetStart && si.offsetEnd >= offsetEnd ) {
					if (si.offsetEnd - si.offsetStart < minCoveringLength
							|| minCoveringLength == -1 || cgn.getIR().getInstructions()[minCoveringInst] == null) {
						minCoveringLength = si.offsetEnd - si.offsetStart;
						minCoveringInst = i;
					}
				}
			}
		}

		if (minCoveringInst != -1) {
			return Collections.singleton((Statement) new NormalStatement(cgn, minCoveringInst));
		}
		return Collections.emptySet();
	}

	/**
	 * When a local is used in some statements such as binary operations, array access, and invoke
	 * instructions, the local does not have a defining statement on that line. If we know that the
	 * user has selected a simple identifier and that the parent AST node is one of these
	 * statements, we should look for the statement in WALA and find where the substatement (simple
	 * identifier used in the statement) is defined.
	 * 
	 * Rather than look for a WALA statement exactly matching the offset values, this function finds
	 * the largest WALA statement completely contained within the offset values. This is to work
	 * around the WALA bug in which for "a = (x)+(y)" WALA takes the statement offsets as "x)+y("\
	 * 
	 * TODO: fid the bug in WALA/Polyglot (likely Polyglot). The above may be dangerous in some
	 * cases, since if if the coordinates of off we could get a wrong statement (consider
	 * "somefield.foo" where somefield is a field in this; as a getfield statement it is a WALA
	 * statement in itself, but treated as a simplename in the JDT. Thus we run the danger of
	 * looking inside the "somefield" statement)
	 * 
	 * If two IR instructions have the exact same SourcePosition, it will use the first non-null instruction.
	 * 
	 * @param cgn
	 * @param offsetStart
	 *            offsetStart of the parent node (such as a binary operation, array acesss, invoke
	 *            instruction) in AST
	 * @param offsetEnd
	 *            offsetEnd of the parent node in AST
	 * @param subStatementIndex
	 *            the "index" within the statement -- for instance, a binary operation's left
	 *            operand is zero and it's right is one.
	 * @param parentInstructionClass
	 * 			  an optional filter to only consider instructions of a certain class. If null,
	 * 			  ignoreed and all instructions are considered.
	 * @return A collection of statements with one statement -- the one that defines the local -- or
	 *         an empty set if it cannot be found.
	 */
	public static Collection<Statement> getStatementFromOffsetAndSubstatementIndex(CGNode cgn,
			int offsetStart, int offsetEnd, int subStatementIndex,
			Class<? extends SSAInstruction> parentInstructionClass) {

		// find longest WALA statement contained within the bounds (or exactly fitting)
		// in the case of "((mya_x3)+(3+2))" WALA's statement will be "mya_x3)+(3+2"
		int maxLen = 0;
		int statement = -1;
		SSAInstruction instrs[] = cgn.getIR().getInstructions();
		for (int i = 0; i < instrs.length; i++) {
			SourcePosition si = CGNodeUtils.getSourcePositionOfInstructionIndex(cgn, i, null);
			if (si != null) {
				if (si.offsetStart >= offsetStart && si.offsetEnd <= offsetEnd
						&& (parentInstructionClass == null || parentInstructionClass.isInstance(instrs[i]))) {
					int len = si.offsetEnd - si.offsetStart;
					if (len > maxLen || (len == maxLen && cgn.getIR().getInstructions()[statement] == null)) {
						statement = i;
						maxLen = len;
					}
				}
			}
		}

		// if that doesn't work, we might have to find the smallest WALA statement covering the
		// bounds. in the case of "new int[((mya_x3)+(3))" the WALA statement for the binop is,
		// incredibly, "[((mya_x3)+(2))]"
		if (statement == -1) {
			int minLen = Integer.MAX_VALUE;
			for (int i = 0; i < cgn.getIR().getInstructions().length; i++) {
				SourcePosition si = CGNodeUtils.getSourcePositionOfInstructionIndex(cgn, i, null);
				if (si != null) {
					if (si.offsetStart <= offsetStart && si.offsetEnd >= offsetEnd
							&& (parentInstructionClass == null || parentInstructionClass.isInstance(instrs[i]))) {
						int len = si.offsetEnd - si.offsetStart;
						if (len < minLen || (len == minLen && cgn.getIR().getInstructions()[statement] == null)) {
							statement = i;
							minLen = len;
						}
					}
				}
			}
		}

		// TODO: warning if null?
		if (statement != -1) {
			int valUsed = cgn.getIR().getInstructions()[statement].getUse(subStatementIndex);
			SSAInstruction def = cgn.getDU().getDef(valUsed);
			if (def == null) {
				// several possibilities here:
				// 1) the value is a parameter, including v1. this is trivial to check
				if (valUsed <= cgn.getMethod().getNumberOfParameters()) {
					return Collections.singleton((Statement) new ParamCallee(cgn, valUsed));
				}
				// 2) the value is defined to be a constant.
				// 3) valUsed == -1, invalid. something went wrong.
				return Collections.singleton((Statement) new NormalStatement(cgn, statement));
			}
			return Collections.singleton((Statement) CGNodeUtils.findStatementFromSSAInstruction(
					cgn, def));
		}
		
		return Collections.emptySet();
	}

	/**
	 * Find and return the nth statement in the IR which has exactly the matching offsetStart and offsetEnd values
	 * Optionally, if a class is passed as the last argument, only instructions of this type
	 * will be considered, and the result will be the nth statement of that type with the matching offset. 
	 * @param cgn
	 * @param offsetStart
	 * @param offsetEnd
	 * @param n
	 * @param klass
	 * an optional filter to only consider instructions of this type 
	 * @return
	 */
	public static Collection<Statement> getNthArrayStoreStatementFromOffset(
			CGNode cgn, int offsetStart, int offsetEnd, int n, Class<? extends SSAInstruction> klass) {
		int statement = -1;
		SSAInstruction[] instrs = cgn.getIR().getInstructions();
		for (int i = 0; i < instrs.length; i++) {
			SourcePosition si = CGNodeUtils.getSourcePositionOfInstructionIndex(cgn, i, null);
			if (si != null && si.offsetStart == offsetStart && si.offsetEnd == offsetEnd &
					instrs[i] != null && (klass == null || klass.isInstance(instrs[i])) ) {
				if (n == 0) {
					statement = i;
					break;
				}
				n--;
			}
		}
		
		if (statement == -1)
			return Collections.emptySet();
		else
			return Collections.singleton((Statement) new NormalStatement(cgn,statement));
	}
}
