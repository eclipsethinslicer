package svelteselftests;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.jar.JarFile;

import com.ibm.wala.cast.java.client.JavaSourceAnalysisEngine;
import com.ibm.wala.cast.java.client.impl.ZeroOneContainerCFABuilderFactory;
import com.ibm.wala.cast.java.ssa.EnclosingObjectReference;
import com.ibm.wala.classLoader.JarFileModule;
import com.ibm.wala.classLoader.SourceFileModule;
import com.ibm.wala.core.tests.callGraph.CallGraphTestUtil;
import com.ibm.wala.eclipse.util.CancelException;
import com.ibm.wala.eclipse.util.EclipseProjectPath;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilder;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ssa.SSAInstruction;

/**
 * This test shows the existence of a bug/oversight which causes pointer analysis to be incorrect
 * when using inner classes. The problem comes when we use a "new" expression with an explicit
 * enclosing object, such as "Inner i = o.new Inner()" Subsequent "enclosing" instructions in the
 * pointer analysis will return the wrong points to set -- they will return the points to set for
 * value number 1 (v1) in the scope of the new expression, rather than the points to set for "o".
 * 
 * This test prints out the points to set for the object retrieved by the "enclosing" instruction in
 * an inner class.
 * 
 * ON SUCCESS (with bug fix to WALA): The test prints out an instance key for an object of type
 * Outer (variable "outer", the explicit enclosing object of the inner object).
 * 
 * ON FAILURE (without bug fix to WALA): The test prints out an instance key for an object of type
 * Integer (v1 in the temp() function in the test)
 * 
 * @author evan
 * 
 */
public class EnclosingExplicitNewBugTestDriver {
	public static void main(String args[]) {
		final String pathToRtJar = "/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/rt.jar";

		final String badFilePath = "/home/evan/t/bugtestsforwalawala/enclosingexplicitnew/InnerClassA.java";

		String[] mainClassDescriptors = new String[] { "LInnerClassA" };
		JavaSourceAnalysisEngine engine = staticGetAnalysisEngine(mainClassDescriptors);
		try {
			engine.addSystemModule(new JarFileModule(new JarFile(pathToRtJar)));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		engine.addSourceModule(new SourceFileModule(new File(badFilePath), "InnerClassA.java"));

		try {
			CallGraph cg = engine.buildDefaultCallGraph();
			PointerAnalysis pa = engine.getPointerAnalysis();

			Iterator<CGNode> iter = cg.iterator();
			while (iter.hasNext()) {
				CGNode n = iter.next();

				// assume in the test we have one enclosing instruction for each of the methods
				// here.
				String methodSigs[] = { "InnerClassA$AB.getA_X_from_AB()I",
						"InnerClassA$AB.getA_X_thru_AB()I", "InnerClassA$AB$ABSubA.getA_X()I",
						"InnerClassA$AB$ABA$ABAA.getABA_X()I", "InnerClassA$AB$ABA$ABAA.getA_X()I",
						"InnerClassA$AB$ABA$ABAB.getABA_X()I",
						"InnerClassA$AB$ABSubA$ABSubAA.getABA_X()I",
						"InnerClassA$AB$ABSubA$ABSubAA.getA_X()I", };

				// each type suffixed by ","
				String ikConcreteTypeStrings[] = { "LInnerClassA,", "LInnerClassA,",
						"LInnerClassA,", "LInnerClassA$AB$ABSubA,LInnerClassA$AB$ABA,",
						"LInnerClassA,", "LInnerClassA$AB$ABA,", "LInnerClassA$AB$ABSubA,",
						"LInnerClassA,", };

				// System.out.println(" found " + n.getMethod().getSignature());

				assert (methodSigs.length == ikConcreteTypeStrings.length);
				for (int i = 0; i < methodSigs.length; i++) {
					if (n.getMethod().getSignature().equals(methodSigs[i])) {
						// find enclosing instruction
						for (SSAInstruction instr : n.getIR().getInstructions()) {
							if (instr instanceof EnclosingObjectReference) {
								String allIks = "";
								for (InstanceKey ik : pa.getPointsToSet(new LocalPointerKey(n,
										instr.getDef())))
									allIks += ik.getConcreteType().getName() + ",";
								System.out.printf("in method %s, got ik %s\n", methodSigs[i],
										allIks);

								if (!allIks.equals(ikConcreteTypeStrings[i]))
									System.out
											.printf(
													"assertion failed: expecting ik %s in method %s, got %s\n",
													ikConcreteTypeStrings[i], methodSigs[i], allIks);
								break;
							}
						}
					}
				}
			}

		} catch (CancelException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	protected static JavaSourceAnalysisEngine staticGetAnalysisEngine(
			final String[] mainClassDescriptors) {
		JavaSourceAnalysisEngine engine = new JavaSourceAnalysisEngine() {
			@Override
			protected Iterable<Entrypoint> makeDefaultEntrypoints(AnalysisScope scope,
					IClassHierarchy cha) {
				return Util.makeMainEntrypoints(EclipseProjectPath.SOURCE_REF, cha,
						mainClassDescriptors);
			}

			@Override
			protected CallGraphBuilder getCallGraphBuilder(IClassHierarchy cha,
					AnalysisOptions options, AnalysisCache cache) {
				return new ZeroOneContainerCFABuilderFactory().make(options, cache, cha, scope,
						false);
			}
		};
		engine.setExclusionsFile(CallGraphTestUtil.REGRESSION_EXCLUSIONS);
		return engine;
	}
}