package svelteselftests;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.jar.JarFile;

import com.ibm.wala.cast.java.client.JavaSourceAnalysisEngine;
import com.ibm.wala.cast.java.client.impl.ZeroOneContainerCFABuilderFactory;
import com.ibm.wala.cast.java.ssa.EnclosingObjectReference;
import com.ibm.wala.classLoader.JarFileModule;
import com.ibm.wala.classLoader.SourceFileModule;
import com.ibm.wala.core.tests.callGraph.CallGraphTestUtil;
import com.ibm.wala.eclipse.util.CancelException;
import com.ibm.wala.eclipse.util.EclipseProjectPath;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilder;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ssa.SSAInstruction;

/**
 * This test shows the existence of a bug/oversight which causes pointer analysis to be incorrect
 * when using inner classes. The problem comes when we use a "new" expression with an explicit
 * enclosing object, such as "Inner i = o.new Inner()" Subsequent "enclosing" instructions in the
 * pointer analysis will return the wrong points to set -- they will return the points to set for
 * value number 1 (v1) in the scope of the new expression, rather than the points to set for "o".
 * 
 * This test prints out the points to set for the object retrieved by the "enclosing" instruction in
 * an inner class.
 * 
 * ON SUCCESS (with bug fix to WALA): The test prints out an instance key for an object of type
 * Outer (variable "outer", the explicit enclosing object of the inner object).
 * 
 * ON FAILURE (without bug fix to WALA): The test prints out an instance key for an object of type
 * Integer (v1 in the temp() function in the test)
 * 
 * @author evan
 * 
 */
public class OldEnclosingExplicitNewBugTestDriver {
	public static void main(String args[]) {
		final String pathToRtJar = "/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/rt.jar";

		final String badFilePath = "/home/evan/t/bugtestsforwalawala/enclosingexplicitnew/Outer.java";

		String[] mainClassDescriptors = new String[] { "LOuter" };
		JavaSourceAnalysisEngine engine = staticGetAnalysisEngine(mainClassDescriptors);
		try {
			engine.addSystemModule(new JarFileModule(new JarFile(pathToRtJar)));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		engine.addSourceModule(new SourceFileModule(new File(badFilePath), "Outer.java"));

		try {
			CallGraph cg = engine.buildDefaultCallGraph();
			PointerAnalysis pa = engine.getPointerAnalysis();

			Iterator<CGNode> iter = cg.iterator();
			while (iter.hasNext()) {
				CGNode n = iter.next();
				// find correct method
				if (n.getMethod().getSignature().equals("Outer$Inner.getOuterVar()I")) {

					// find enclosing instruction
					for (SSAInstruction i : n.getIR().getInstructions()) {
						if (i instanceof EnclosingObjectReference) {
							for (InstanceKey ik : pa.getPointsToSet(new LocalPointerKey(n, i
									.getDef())))
								System.out.println(ik); // This will probably only be run once.
							// prints the instance key associated
							// with the enclosing object
							break;
						}
					}
				}
			}

		} catch (CancelException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	protected static JavaSourceAnalysisEngine staticGetAnalysisEngine(
			final String[] mainClassDescriptors) {
		JavaSourceAnalysisEngine engine = new JavaSourceAnalysisEngine() {
			@Override
			protected Iterable<Entrypoint> makeDefaultEntrypoints(AnalysisScope scope,
					IClassHierarchy cha) {
				return Util.makeMainEntrypoints(EclipseProjectPath.SOURCE_REF, cha,
						mainClassDescriptors);
			}

			@Override
			protected CallGraphBuilder getCallGraphBuilder(IClassHierarchy cha,
					AnalysisOptions options, AnalysisCache cache) {
				return new ZeroOneContainerCFABuilderFactory().make(options, cache, cha, scope,
						false);
			}
		};
		engine.setExclusionsFile(CallGraphTestUtil.REGRESSION_EXCLUSIONS);
		return engine;
	}
}