package svelteselftests;

import java.io.IOException;
import java.util.Iterator;

import com.ibm.wala.ipa.callgraph.CGNode;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.propagation.InstanceKey;
import com.ibm.wala.ipa.callgraph.propagation.LocalPointerKey;
import com.ibm.wala.ipa.callgraph.propagation.PointerAnalysis;
import com.ibm.wala.ipa.callgraph.propagation.PointerKey;

import edu.berkeley.cs.bodik.svelte.SvelteAnalysisEngine;

public class AnonInnerClassThisZero {
	public static void main(String args[]) {
		SvelteAnalysisEngine se = new SvelteAnalysisEngine();

		try {
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/resources.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/rt.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/jsse.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/jce.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/charsets.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/sunjce_provider.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/dnsns.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/localedata.jar");
			se.addSystemJar("/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/ext/sunpkcs11.jar");
			se.addJavaFile("/home/evan/t/runwork/palante/dmn-innerclass-anon/src/AnonOuter.java");
			se.findEntrypointFromClassnameOfMain("AnonOuter");

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		CallGraph cg = se.getCallGraph();
		PointerAnalysis pa = se.getPointerAnalysis();

		for (Iterator<? extends CGNode> it = cg.iterator(); it.hasNext();) {
			CGNode n = it.next();
			if (n.getMethod().getDeclaringClass().toString().equals(
					"<src-class: LOuter$Inner(within LOuter)>")
					&& n.getMethod().getName().toString().equals("getOuterVar")) {
				System.out.println("declaringclass='" + n.getMethod().getDeclaringClass()
						+ "' name='" + n.getMethod().getName() + "' descriptor='"
						+ n.getMethod().getDescriptor() + "'");
				// System.out.println(n.getIR());
				System.out
						.println("-----------------------------------------------------------------------------");
				// EnclosingObjectReference encInst = (EnclosingObjectReference)
				// n.getIR().getInstructions()[1];
				PointerKey pk = new LocalPointerKey(n, 3);

				for (InstanceKey ik : pa.getPointsToSet(pk)) {
					System.out.println(ik);
					System.out.println(ik.getClass());
					// NormalAllocationSiteKey nask = (NormalAllocationSiteKey)ik;
					// System.out.println(nask.getNode().getIR());
				}
				/*
				 * Iterator x = pa.getPointsToSet(pk).iterator(); Instancekey ik; while (
				 * x.hasNext() ) { ik = x.next(); // etc }
				 */
			}
		}
	}
}