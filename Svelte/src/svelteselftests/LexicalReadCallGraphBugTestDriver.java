package svelteselftests;

import java.io.File;
import java.io.IOException;
import java.util.jar.JarFile;

import com.ibm.wala.cast.java.client.JavaSourceAnalysisEngine;
import com.ibm.wala.cast.java.client.impl.ZeroOneContainerCFABuilderFactory;
import com.ibm.wala.classLoader.JarFileModule;
import com.ibm.wala.classLoader.SourceFileModule;
import com.ibm.wala.core.tests.callGraph.CallGraphTestUtil;
import com.ibm.wala.eclipse.util.CancelException;
import com.ibm.wala.eclipse.util.EclipseProjectPath;
import com.ibm.wala.ipa.callgraph.AnalysisCache;
import com.ibm.wala.ipa.callgraph.AnalysisOptions;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.ipa.callgraph.CallGraph;
import com.ibm.wala.ipa.callgraph.CallGraphBuilder;
import com.ibm.wala.ipa.callgraph.Entrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.cha.IClassHierarchy;

/**
 * This test shows the existence of a bug/oversight which causes an exception when building a
 * ZeroOneContainer call graph.
 * 
 * ON SUCCESS (with bug fix to WALA): The test produces no output.
 * 
 * ON FAILURE (without bug fix to WALA): The test throws an UnimplementedError (from
 * Assertions.UNREACHABLE <- AstSSAPropagationCallGraphBuilder$AstConstraintVisitor$5.act).
 * 
 * @author evan
 * 
 */
public class LexicalReadCallGraphBugTestDriver {
	public static void main(String args[]) {
		final String pathToRtJar = "/usr/lib/jvm/java-6-sun-1.6.0.03/jre/lib/rt.jar";

		final String badFilePath = "/home/evan/t/bugtestsforwalawala/lexicalreadcallgraph/AnonOuter.java";

		String[] mainClassDescriptors = new String[] { "LAnonOuter" };
		JavaSourceAnalysisEngine engine = staticGetAnalysisEngine(mainClassDescriptors);
		try {
			engine.addSystemModule(new JarFileModule(new JarFile(pathToRtJar)));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		engine.addSourceModule(new SourceFileModule(new File(badFilePath), "AnonOuter.java"));

		try {
			@SuppressWarnings("unused")
			CallGraph cg = engine.buildDefaultCallGraph(); // UnimplementedError EXCEPTION THROWN
			// HERE ON TEST FAILURE
		} catch (CancelException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	protected static JavaSourceAnalysisEngine staticGetAnalysisEngine(
			final String[] mainClassDescriptors) {
		JavaSourceAnalysisEngine engine = new JavaSourceAnalysisEngine() {
			@Override
			protected Iterable<Entrypoint> makeDefaultEntrypoints(AnalysisScope scope,
					IClassHierarchy cha) {
				return Util.makeMainEntrypoints(EclipseProjectPath.SOURCE_REF, cha,
						mainClassDescriptors);
			}

			@Override
			protected CallGraphBuilder getCallGraphBuilder(IClassHierarchy cha,
					AnalysisOptions options, AnalysisCache cache) {
				return new ZeroOneContainerCFABuilderFactory().make(options, cache, cha, scope,
						false);
			}
		};
		engine.setExclusionsFile(CallGraphTestUtil.REGRESSION_EXCLUSIONS);
		return engine;
	}
}